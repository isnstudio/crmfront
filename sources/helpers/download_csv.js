import Errors from "./errors.js";

export default class Download_csv {

	download_csv(url) {
		webix.ajax().get(master_url + url).then(function (res) {
			webix.message("Descargando...");
			var csv = (res.text());

			var downloadLink = document.createElement("a");
			var blob = new Blob(["\ufeff", csv]);
			var url = URL.createObjectURL(blob);
			downloadLink.href = url;
			downloadLink.download = "data.csv";

			document.body.appendChild(downloadLink);
			downloadLink.click();
			document.body.removeChild(downloadLink);
		}).fail(function (res) {
			const error = new Errors();
			error.show_error(res);
		});
	}
}