export function getLabel(status){
	if (typeof status == "string")
		status = parseInt(status);
	switch (status){
		case 1:
			return "Taken";
		case 0:
			return "Available";
		case 90:
			return "Broken";
		case 99:
			return "Deprecated";
	}
}