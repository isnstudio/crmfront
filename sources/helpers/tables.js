import {JetView} from "webix-jet";

export default class Tables extends JetView {

	dataLoading(table, url, view, filters, filter_default) {
		let start = view.getParam("start");
		let count = view.getParam("count");
		let page = view.getParam("page");
		let filter = view.getParam("filter");
		filter_default = filter_default ? filter_default : "";

		// Data loading
		const data_url = new URL(url);

		if (start) {
			data_url.searchParams.append("start", start);
		} else {
			data_url.searchParams.append("start", 0);
		}
		if (count) {
			data_url.searchParams.append("count", count);
		} else {
			data_url.searchParams.append("count", 20);
		}
		if (filter) {
			data_url.searchParams.append("filter", filter);
			if(filters)
				filters.$$("cleanfilter").show();
		} else {
			if(filters)
				filters.$$("cleanfilter").hide();
			data_url.searchParams.append("filter", filter_default);
		}

		webix.ajax(data_url.href).then(function (data) {
			let json_data = data.json();
			table.parse(json_data);
			let pager = table.getPager();
			if (pager) {
				pager.disabled();
				if (page) {
					pager.select(page);
				} else {
					pager.select(0);
				}
			}
		}).finally(function (){
			let pager = table.getPager();
			if (pager) pager.enable();
		});
	}

	hideContents(view, datatable, details) {
		setTimeout(function (){
			let id = view.getParam("id");
			if (id) {
				if (id.constructor === Object) {
					if ("id" in id) {
						id = id["id"];
					}
				} else {
					if (datatable.exists(id)) {
						datatable.select(id);
						datatable.showItem(id);
					}
				}
			}
			if (id) {
				if (datatable.exists(id)) {
					datatable.select(id);
				} else {
					details.forEach((detail) => {
						detail.hideContents();
					});
				}
			} else {
				datatable.unselect();
				details.forEach((detail) => {
					detail.hideContents();
				});
			}
		}, 500);
	}

	pagerParams(view, datatable) {
		setTimeout(function () {
			let pager = datatable.getPager();
			pager.attachEvent("onBeforePageChange", function (new_page) {
				let start = new_page * pager.data.size;
				let count = pager.data.size;
				view.$scope.setParam("start", start, false);
				view.$scope.setParam("count", count, false);
				view.$scope.setParam("page", new_page, true);
			});
		}, 500);
	}

	afterRender(view, message_filters) {
		const message = message_filters ? "<p>No hay información para mostrar, verifique los filtros</p>"
			: "<p>No hay información para mostrar<p/>";
		const that = view;
		if (!that.count()) {
			that.showOverlay(message);
		} else {
			that.hideOverlay();
		}
		setTimeout(function () {
			that.enable();
		}, 1000);
	}

	afterSelect(details, header,headers_collapse, id) {
		details.forEach((detail) => {
			detail.showDetail(id);
		});
		if(!this.app.Mobile) {
			header.expand();
			headers_collapse.forEach((header) => {
				header.collapse();
			});
		}
	}
}