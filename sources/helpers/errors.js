export default class Errors {

	show_error(error, context) {
		if (error.status === 400) {
			var data = JSON.parse(error.response);
			if ("non_field_errors" in data) {
				for (const error of data["non_field_errors"]) {
					webix.message({
						text: error,
						type: "error",
						expire: -1,
					});
				}
			} else {
				var table = {};
				var actual_data = {};
				Object.keys(data).forEach(element => {
					if(element.substr(0, 7) !== "dtable-") {
						data[element].forEach(error => {
							const message = element.replaceAll("_", " ") + ": " + error;
							webix.message({
								text: message,
								type: "error",
								expire: -1,
							});
						});
					} else if(context) {
						table = context.$$("data");
						actual_data = table.serialize();
						actual_data.forEach(row => {
							row.$cellCss = {};
						});
						const key_error =  element.substr(7);
						const split_error = key_error.split("-");
						const id_row = split_error[0];
						const id_column = split_error[1];
						if(typeof actual_data[parseInt(id_row) - 1].$cellCss !== "object")
							actual_data[parseInt(id_row) - 1].$cellCss = {};
						actual_data[parseInt(id_row) - 1].$cellCss[id_column] = "highlight";
						actual_data[parseInt(id_row) - 1]["tooltip_" + parseInt(id_column)] = data[element][0];
						table.refresh();
					}
				});
			}
		}

		if (error.status === 405) {
			const response = JSON.parse(error.response);
			webix.message({
				text: response.detail,
				type: "error",
				expire: -1,
			});
		}

		if (error.status === 404) {
			webix.message({
				text: "Uno de los datos no se encuentra en la base de datos.",
				type: "error",
				expire: -1,
			});
		}
		if (error.status === 403) {
			webix.message({
				text: "No estas autorizado a realizar esta acción.",
				type: "error",
				expire: -1,
			});
		}
		if (error.status === 500) {
			webix.message({
				text: "ha ocurrido un error interno del servidor.",
				type: "error",
				expire: -1,
			});
		}
	}
}