import {JetView} from "webix-jet";

export default class QuotationCodeFormView extends JetView {
	config() {
		return {
			rows: [
				{
					id: "quotation-code",
					rows: [
						{},
						{
							cols: [
								{},
								{
									view: "form",
									localId: "form",
									css: {
										"background-color": "white !important",
									},
									borderless: true,
									width: 400,
									rows: [
										{
											template: "<b>Ingresa tu código de verificación</b>",
											height: 50,
											width: 300
										},
										{
											align: "left",
											view: "text",
											label: "Codigo",
											name: "pin",
											id: 1623280353828,
											height: 38,
											pattern: {mask: "######", allow: /[0-9]/g}
										},
										{
											view: "button",
											css: "webix_primary",
											label: "Verificar",
											value: "submit",
											height: 50,
											hotkey: "enter",
											click: (context) => {
												context = this;
												if (this.$$("form").validate()) {
													const get_values = this.$$("form").getValues();
													const uuid = context.getParam("uuid");
													get_values.uuid = uuid;

													var xhr = webix.ajax().sync().get(master_url + "api/pdc/cotizacion-venta/imprimir/", get_values);

													if (xhr.status == 200) {
														context.$$("pdf").url_setter("binary->" + xhr.responseURL);
														context.$$("pdf-view").show();
														context.$$("quotation-code").hide();
													}
													if (xhr.status == 206) {
														webix.message({
															text: xhr.response,
															type: "error",
															expire: -1,
														});
													}
												} else {
													webix.message({
														text: "Faltan campos por llenar",
														type: "error",
														expire: -1,
														id: "messageLead"
													});
												}
											},
										},
									],
									rules: {
										pin: webix.rules.isNotEmpty,
									}
								},
								{}
							]
						},
						{},
					]
				},
				{
					id: "pdf-view",
					hidden: true,
					rows: [
						{view: "pdfbar", id: "toolbar"},
						{view: "pdfviewer", id: "pdf", toolbar: "toolbar"}
					]
				}
			]
		};
	}
}