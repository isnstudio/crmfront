import { JetView } from "webix-jet";

import QuotationCodeFormView from "./form"

export default class QuotationCodeView extends JetView {
  config() {
		return {
			type: "space",
			css:{
				"background-image": 'url(/data/images/grad.png)',
				"transition": 'background .2s ease',
				"position": 'fixed',
				"top": '0',
				"left": '0',
				"right": '0',
				"bottom": '0',
				"background-size": 'cover',
				"background-repeat": 'no-repeat',
				"background-position": 'center'
			},
			cols: [
				QuotationCodeFormView
			]
		};
  }
}