import {JetView} from "webix-jet";
import Errors from "../../../helpers/errors";

export default class ChartView extends JetView {
	config() {
		return {
			rows: [
				{
					id: "chart",
					hidden: false,
					template: "<div id='my_chart'><div id=\"chart_div\" class='flex-center'></div></div>"
				},
				{
					id: "empty_chart",
					hidden: true,
					template: "<p style='text-align: center; color: #9F6000; background-color: #FEEFB3;'>No hay información para mostrar, verifique los filtros</p>"
				}
			]
				
			
		}
	}

	urlChange() {
		google.charts.load("current", {packages: ["corechart"]});
		google.charts.setOnLoadCallback(drawChart);
		const that = this;

		function drawChart() {
			var options = {
				title: "Pronóstico",
				height: 650,
				width: 1200,
				colors: ["#4179d0", "#76ae49"],
				legend: { position: "top" },
				fontSize: 14,
				trendlines: {
					0: {
						type: "linear",
						showR2: true,
						visibleInLegend: true,
						title: "Pronóstico"
					}
				},
				hAxis: {
					format: "yyyy-MM-d"
				}
			};
			if(that.app.Mobile) {
				options.width = screen.width;
				options.fontSize = 10;
			}
			var filters = that._parent.getParam("filter");

			printChart(filters, options);
		}

		function printChart(filters, options) {
			const data_url = new URL(master_url + "api/crm/sale/forecast_bi/");
			data_url.searchParams.append("filter", filters);
			const chart = that.$$("chart");
			if(filters) {
				webix.extend(chart, webix.ProgressBar);
				chart.showProgress();
				webix.ajax(data_url.href).then(function (data) {
					var data_response = null;
					var chart_data = false;
					data_response = data.json();
					data_response.shift();
					data_response.forEach(element => {
						element[0] = new Date(element[0]);
					});
					data_response.unshift(["Periodo", "Demanda"]);
	
					for (let index = 1; index < data_response.length; index++) {
						if(data_response[index][1] !== null) {
							chart_data = true; 
							index = data_response.length + 1;
						}
					}
					if(chart_data) {
						that.$$("chart").show();
						that.$$("empty_chart").hide();
						var data_chart = google.visualization.arrayToDataTable(data_response);
						var chart = new google.visualization.ScatterChart(document.getElementById("chart_div"));
						chart.draw(data_chart, options);	
					} else {
						chart.hide();
						that.$$("empty_chart").show();
					}
				
				}).fail(function (res) {
					const error = new Errors();
					error.show_error(res);
					chart.hide();
					that.$$("empty_chart").show();
				}).finally(function () {
					chart.hideProgress();
				});
			}
		}
	}
}	
