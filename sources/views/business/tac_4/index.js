import {JetView} from "webix-jet";
import Tables from "../../../helpers/tables";
import FilterTac4View from "jet-views/business/tac_4/filter";

export default class tacView extends JetView {
	config() {

		this.selected_item = null;

		this.filter_form_values = null;

		this.mark_color = function(value, row){
			const actual = Number(row.actual.replace(/[^0-9.-]+/g,""));
			const sugerido = Number(row.sugerido.replace(/[^0-9.-]+/g,""));
			if (actual < sugerido){
				return { "background-color": "#ffbaba" };
			}else{
				return { "background-color": "#e4ffdf" };
			}
		};

		this.Tables = new Tables(this.app);

		this.grouplistTAC = {
			view: "grouplist",
			id: "taclist",
			// data: dataTac,
			select: true,
			autowidth: true,
			on: {
				onItemClick: (id) => {
					this.selected_item = this.$$("taclist").getItem(id);
					this.itemClick(id);
				}
			}
		};

		this.dataAgente = {
			view: "property",
			id: "dataAgente",
			scroll: "y",
			nameWidth: 165,
			elements: [
				{label: "Promedio", type: "text", id: "promedio", css: "right", format: webix.i18n.priceFormat},
				{
					label: "Promedio diario",
					type: "text",
					id: "promedio_diario",
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					label: "Promedio actual",
					type: "text",
					id: "venta_actual",
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					label: "Promedio sugerido",
					type: "text",
					id: "sugerido",
					css: "right",
					format: webix.i18n.priceFormat
				},
			],
			editable: false,
			height: 120,
			width: 350,
		};

		this.excelDownload = {
			view: "button",
			label: "Exportar vista a excel",
			type: "icon",
			icon: "mdi mdi-file-excel",
			height: 40,
			click:function(){
				webix.toExcel($$("gridClienteToExcelDownload"));
			}
		};

		this.dataTableCliente = {
			view: "datatable",
			id: "gridCliente",
			scroll: "xy",
			rowHeight: 40,
			select: true,
			columns: [
				{id: "cliente", header: "Cliente", fillspace: true, minWidth: 160},
				{id: "sales_team", header: "Sales team", minWidth: 140, adjust: true},
				{id: "articulo", header: "Clave articulo", fillspace: true, minWidth: 160},
				{id: "cantidad", header: "Cantidad", fillspace: true, minWidth: 80},
				{id: "unidad_medida", header: "UM",fillspace: true, minWidth: 40},
				{id: "fecha", header: "Fecha", fillspace: true, minWidth: 120},
				{fillspace: true},
				{
					id: "promedio", header: {text: "Promedio", css: "right"}, fillspace: true, minWidth: 80,
					css: "right", format: webix.i18n.priceFormat
				},
				{
					id: "promedio_diario", header: {text: "Diario", css: "right"}, fillspace: true, minWidth: 80,
					css: "right", format: webix.i18n.priceFormat
				},
				{
					id: "actual", header: {text: "Actual", css: "right"}, fillspace: true, minWidth: 80,
					css: "right", format: webix.i18n.priceFormat
				},
				{
					id: "sugerido", header: {text: "Sugerido", css: "right"}, fillspace: true, minWidth: 80,
					css: "right", format: webix.i18n.priceFormat, cssFormat:this.mark_color
				},
			],
			on: {
				onAfterRender() {
					this.$scope.Tables.afterRender(this, false);
				},
			}
		};

		this.dataTableClienteToExcelDownload = {
			view: "datatable",
			id: "gridClienteToExcelDownload",
			scroll: "xy",
			rowHeight: 40,
			select: true,
			hidden: true,
			columns: [
				{ id: "cliente", header: "Cliente", fillspace: true, minWidth: 160 },
				{ id: "sales_team", header: "Sales team", minWidth: 140, adjust: true },
				{ id: "articulo", header: "Clave articulo", fillspace: true, minWidth: 160 },
				{ id: "cantidad", header: "Cantidad", fillspace: true, minWidth: 80 },
				{ id: "unidad_medida", header: "UM", fillspace: true, minWidth: 40 },
				{ id: "fecha", header: "Fecha", fillspace: true, minWidth: 120 },
				{ fillspace: true },
				{ id: "promedio", header: { text: "Promedio", css: "right" }, fillspace: true, minWidth: 80, css: "right" },
				{ id: "promedio_diario", header: { text: "Diario", css: "right" }, fillspace: true, minWidth: 80, css: "right" },
				{ id: "actual", header: { text: "Actual", css: "right" }, fillspace: true, minWidth: 80, css: "right" },
				{ id: "sugerido", header: { text: "Sugerido", css: "right" }, fillspace: true, minWidth: 80, css: "right" },
			]
		};

		this.Filter = new FilterTac4View(this.app);

		const listTableCliente = {
			view: "list",
			localId: "gridCliente",
			css: "multi-line-box",
			select: true,
			type: {
				height: "auto",
				template: obj =>
					`
					<span style="font-weight: 500;"><b>Cliente:  ${obj.cliente}</b></span>
					<br>
					<span style="font-weight: 500;"><b>Articulo</b>: ${obj.articulo}</span>
					<br>
					<span style="font-weight: 500;"><b>Cantidad</b>: ${obj.cantidad}</span>
					<br>
					<span style="font-weight: 500;"><b>Unidad de medida</b>: ${obj.unidad_medida}</span>
					<br>
					<span style="font-weight: 500;"><b>Fecha</b>: ${obj.fecha}</span>
					<br>
					<span style="font-weight: 500;"><b>Promedio</b>: ${obj.promedio}</span>
					<br>
					<span style="font-weight: 500;"><b>Promedio diario</b>: ${obj.promedio_diario}</span>
					<br>
					<span style="font-weight: 500;"><b>Promedio actual</b>: ${obj.actual}</span>
					<br>
					<span style="font-weight: 500;"><b>Promedio sugerido</b>: ${obj.sugerido}</span>
					<hr>
					`
			}
		};

		const searchTAC = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 24,
			height: 44,
			visibleBatch: "default",
			cols: [
				{
					view: "label",
					label: "Mantenimiento clientes",
					batch: "default",
					localId: "label",
				}
			],
		};

		if (this.app.Mobile) {
			return {
				view: "accordion",
				rows: [
					{
						view: "accordionitem",
						template: "Análisis",
						header: "Análisis",
						type: "header",
						collapsed: false,
						body: this.grouplistTAC,
					},
					{
						view: "accordionitem",
						header: "Promedios",
						type: "header",
						collapsed: true,
						body: this.dataAgente,
					},
					{
						view: "accordionitem",
						header: "Mantenimiento clientes",
						type: "header",
						collapsed: true,
						body: listTableCliente,
					},
				]
			};
		}

		return {
			type: "space",
			view: "accordion",
			cols: [
				{
					rows: [
						searchTAC,
						this.dataTableCliente,
						this.dataTableClienteToExcelDownload
					]
				},
				{
					view: "accordionitem",
					template: "Análisis",
					header: "Análisis",
					type: "header",
					collapsed: false,
					body: {
						rows: [
							this.grouplistTAC,
							this.Filter,
							this.dataAgente,
							this.excelDownload,
							{}
						]
					}
				},
			]
		};
	}

	itemClick(id) {
		const that = this;
		const list = this.$$("taclist");

		webix.extend(list, webix.ProgressBar);
		list.showProgress();
		list.disable();
		const grid = this.$$("gridCliente");
		webix.extend(grid, webix.ProgressBar);
		grid.showProgress();
		grid.disable();

		let cliente_id = "";
		if(this.filter_form_values){
			cliente_id = this.filter_form_values.cliente_id;
		}

		if (this.selected_item.sales_team_id) {
			const sales_team_id = this.selected_item.sales_team_id;
			const url = master_url + "api/crm/sale/average_sales/?sales_team_id=" + sales_team_id + "&cliente_id=" + cliente_id
			webix.ajax().get(url)
				.then(function (res) {
					const data = res.json();
					that.$$("dataAgente").data_setter(data.analisis);
					that.$$("gridCliente").clearAll();
					that.$$("gridCliente").data_setter(data.clientes);
					that.$$("gridClienteToExcelDownload").clearAll();
					that.$$("gridClienteToExcelDownload").data_setter(data.clientes);
					this.dataTableCliente.yCount = data.clientes.lenght;
				}).finally(function () {
					list.hideProgress();
					list.enable();
					grid.hideProgress();
					grid.enable();
				});
		} else if (this.selected_item.adviser_id) {
			const adviser_id = this.selected_item.adviser_id;
			const url = master_url + "api/crm/sale/average_sales/?adviser_id=" + adviser_id + "&cliente_id=" + cliente_id
			webix.ajax().get(url)
				.then(function (res) {
					const data = res.json();
					that.$$("dataAgente").data_setter(data.analisis);
					that.$$("gridCliente").clearAll();
					that.$$("gridCliente").data_setter(data.clientes);
					that.$$("gridClienteToExcelDownload").clearAll();
					that.$$("gridClienteToExcelDownload").data_setter(data.clientes);
				}).finally(function () {
					list.hideProgress();
					list.enable();
					grid.hideProgress();
					grid.enable();
				});
		}
	}

	ready() {
		var xhr = webix.ajax().sync().get(master_url + "api/crm/sales_team/tree_sales_team_for_customer_maintenance/");
		const dataTac = JSON.parse(xhr.response);
		this.$$("taclist").data_setter(dataTac);
	}
}