import {JetView} from "webix-jet";
import "webix/server_autocomplete";

export default class FilterTac4View extends JetView {
	config() {
		const form = {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			scroll: "y",
			rows: [
				{
					view: "server_autocomplete",
					id: "id_cliente",
					name: "cliente_id",
					label: "Cliente",
					placeholder: "Cliente",
					url: master_url + "api/cxc/cliente/select_list/?status_str=['Activo']",
					required: true
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon",
					icon: "mdi mdi-filter",
					height: 40,
					click: () => {
                        this._parent.filter_form_values = this.$$("filterform").getValues();
						this._parent.itemClick();
					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						this.$$("filterform").clear();
                        this._parent.filter_form_values = this.$$("filterform").getValues();
                        this._parent.itemClick();
					}
				}
			]
		};
		return {
			rows:[
				{
					view: "template",
					id: "form_header",
					template: "Filtros",
					header: "Filtros",
					type: "header"
				},
				form
			]
		};

	}

	init() {

	}
}