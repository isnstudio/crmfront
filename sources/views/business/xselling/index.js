import {JetView} from "webix-jet";
import FilterListView from "jet-views/business/xselling/listFilters";
import Errors from "../../../helpers/errors";

export default class xsellingView extends JetView {
	config() {
		this.Filters = new FilterListView(this.app);

		const headerToolbar = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 24,
			height: 44,
			visibleBatch: "default",
			cols: [
				{
					view: "label",
					label: "Cross selling",
					batch: "default",
					localId: "label",
				}
			],
		};

		this.empty_chart = {
			id: "empty_chart",
			hidden: true,
			template: "<p style=\"text-align: center; color: #9F6000; background-color: #FEEFB3;\">No hay información para mostrar, verifique los filtros</p>"
		};

		if (this.app.Mobile) {
			return {
				type: "space",
				view: "scrollview",
				scroll: "y",
				body: {
					view: "accordion",
					id: "view",
					rows: [
						{
							view: "accordionitem",
							template: "Filtros",
							header: "Filtros",
							type: "header",
							collapsed: true,
							body: this.Filters,
						},
						headerToolbar,
						this.empty_chart
					]
				}
			};
		}

		return {
			rows: [
				{
					type: "space",
					view: "accordion",
					cols: [
						{
							id: "view",
							rows: [
								headerToolbar,
								this.empty_chart,
							]
						},
						{
							view: "accordionitem",
							template: "Filtros",
							header: "Filtros",
							type: "header",
							collapsed: false,
							body: {
								rows: [
									this.Filters,
									{}
								]
							},
						}
					]
				}
			]
		};
	}

	init() {
		this.setParam("filter", "", "");
		this.$$("empty_chart").show();
	}

	urlChange() {
		const that = this;
		let filter = this.getParam("filter");
		const data_url = new URL(master_url + "api/crm/sale/x_selling/");
		let pivot_view = {
            view: "pivot",
			mode: "table",
            chart: {
              type: "line",
              scale: "logarithmic",
            },
            datatable: {
              columnWidth: 150,
              css: "pivot",
            },
            url: ""
        };

		if (filter) {
			data_url.searchParams.append("filter", filter);
			this.Filters.$$("cleanfilter").show();

			let childs = that.$$("view").getChildViews();
			if(childs[2])
				that.$$("view").removeView(childs[2].config.id);

			pivot_view.url = data_url.href;
			that.$$("empty_chart").hide();
			that.$$("view").addView(pivot_view);
		} else {
			let childs = that.$$("view").getChildViews();
			if(childs[2])
				that.$$("view").removeView(childs[2].config.id);
			that.$$("empty_chart").show();
			that.Filters.$$("cleanfilter").hide();
		}
	}


	ready() {
		const that = this;
		webix.attachEvent("onBeforeAjax", function(mode, url, data, request, headers, files, promise){
		var urlObj = new URL(url);
		var path = urlObj.pathname;
		if (path.includes("api/crm/sale/x_selling/")) {
			promise.then(function(response){
				var responseData = response.json();
				if (!responseData || responseData.length === 0) {
					webix.message("No hay datos para estos filtros");
					let childs = that.$$("view").getChildViews();
					if(childs[2])
						that.$$("view").removeView(childs[2].config.id);
					that.$$("empty_chart").show();
				}
			}).catch(function(res){
				let childs = that.$$("view").getChildViews();
				if(childs[2])
					that.$$("view").removeView(childs[2].config.id);
				that.$$("empty_chart").show();
				const error = new Errors();
				error.show_error(res);
			});
		}
		return true;
		});
	}
}