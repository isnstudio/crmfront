import {JetView} from "webix-jet";
import "webix/server_autocomplete";
import "webix/multi_server_autocomplete";
import "webix/datepicker_iso";

export default class FilterListView extends JetView {
	config() {
		const select_text = "Clic para seleccionar";
		return {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			width: 384,
			height: 600,
			scroll: "y",
			rows: [
				{
					id: "fecha_inicial",
					view: "datepicker_iso",
					label: "Fecha inicial",
					name: "fecha_inicial",
					labelPosition: "top",
					required: true,
					onlyDay: 1
				},
				{
					id: "fecha_final",
					view: "datepicker_iso",
					label: "Fecha final",
					name: "fecha_final",
					labelPosition: "top",
					required: true,
					onlyDay: 0
				},
				{
					id: "cliente_ids",
					view: "multi_server_autocomplete",
					label: "Cliente",
					labelPosition: "top",
					name: "cliente_ids",
					url: master_url + "api/cxc/cliente/select_list_name/",
					select_text: select_text,
					required: true
				},
				{
					id: "cruce",
					view: "combo",
					label: "Tipo de cruce",
					labelPosition: "top",
					name: "cruce",
					required: true,
					options: [{id: 1, value: "Por artículo"}, {id: 2, value: "Por categorización"}]
				},
				{
					id: "articulo_ids",
					view: "multi_server_autocomplete",
					label: "Artículo",
					labelPosition: "top",
					name: "articulo_ids",
					disabled: false,
					url: master_url + "api/inv/articulo/select_list_clave/",
					select_text: select_text,
					required: true,
					hidden: true,
				},
				{
					id: "categorizacion",
					view: "multi_server_autocomplete",
					label: "Categorización",
					labelPosition: "top",
					name: "articulo_categorizacion_ids",
					url: master_url + "api/inv/categorizacion-articulo/select_list/",
					select_text: select_text,
					required: true,
					hidden: true,
				},
				{
					view: "button",
					value: "submit",
					label: "Configurar datos",
					type: "icon", icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						if (this.$$("filterform").validate()) {
							let querystring = "";
							const values = this.$$("filterform").getValues();
							if (values.is_project == 3)
								delete values.is_project;
							if (Object.keys(values).length !== 0) {
								for (let k in values) {
									if (values[k]) {
										querystring += k + "==" + values[k] + ";";
									}
								}
							}
							querystring += "freq==W;";
							this._parent.setParam("filter", querystring, true);
						} else {
							webix.message({
								text: "Faltan campos por llenar",
								type: "error",
								expire: -1,
								id: "messageLead"
							});
						}
					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						this._parent.setParam("filter", "", true);
						this.$$("filter_form").clear();
						let childs = this._parent.$$("view").getChildViews();
						if(childs[2])
							this._parent.$$("view").removeView(childs[2].config.id);
					}
				},
			],
		};
	}

	ready() {
		const that = this;
		this.$$("cruce").attachEvent("onChange", function (value) {
			if(value === "1") {
				that.$$("articulo_ids").show();
				that.$$("categorizacion").hide();
			}
			else if (value === "2") {
				that.$$("articulo_ids").hide();
				that.$$("categorizacion").show();
			}
		});
	}
}