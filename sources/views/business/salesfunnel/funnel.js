import {JetView} from "webix-jet";
import Errors from "../../../helpers/errors";

export default class funnelView extends JetView {

    config() {
        return  {
            rows: [
                {

                    id: "funnel",
                    template: '<div class="flex" id="graph"><div class="funnel" id="funnel"></div></div>',
                    borderless: true,
                },
                {
					id: "empty_chart",
					hidden: true,
					template: '<p style="text-align: center; color: #9F6000; background-color: #FEEFB3;">No hay información para mostrar, verifique los filtros</p>'
				},
            ]
            
        }
    
    }

    init() {
    }

    urlChange() {
        const filter = this._parent.getParam("filter");

        var xhr = webix.ajax().sync().get(master_url + "api/crm/log/sales_funnel/", {filter: filter});
        if(xhr.status !== 200) {
            const error = new Errors();
			error.show_error(xhr);
            $$("funnel").hide();
            $$("empty_chart").show();
        }
        var data = JSON.parse(xhr.response);

        if(data.values[0] == 0) {
            $$("funnel").hide();
			$$("empty_chart").show();
            webix.message({ type:"error", text:"No hay datos que mostrar de: " + data.subLabels[0] });
        } else {
            this.$$("funnel").show();
			this.$$("empty_chart").hide();

            var funnel = document.getElementById("funnel");
            funnel.parentNode.removeChild(funnel);

            var graph_view = document.getElementById("graph");
            var new_funnel = document.createElement('div'); 
            new_funnel.className += " funnel";
            new_funnel.id = "funnel"
            graph_view.appendChild(new_funnel);

            this.graph = new FunnelGraph({
                container: '.funnel',
                gradientDirection: 'horizontal',
                data: data,
                displayPercent: true,
                direction: 'vertical',
                width: window.outerWidth - 950,
                height: 500,
                subLabelValue: 'raw'
            });

            this.graph.draw();
    
            if(this.app.Mobile)
                this.graph.updateWidth(window.outerWidth - 200);
        }
    }
}