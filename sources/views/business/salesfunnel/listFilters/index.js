import {JetView} from "webix-jet";
import "webix/server_autocomplete";
import "webix/datepicker_iso";

export default class FilterListView extends JetView {
	config() {
		const select_text = "Clic para seleccionar";

		return {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			width: 384,
			autoheight: true,
			rows: [
				{
					id: "start_date",
					view: "datepicker_iso",
					label: "Fecha inicial",
					name: "start_date",
					labelPosition: "top",
					required: true
				},
				{
					id: "end_date",
					view: "datepicker_iso",
					label: "Fecha final",
					name: "end_date",
					labelPosition: "top",
					required: true
				},
				{
					id: "sales_team_id",
					view: "server_autocomplete",
					label: "Equipo",
					labelPosition: "top",
					name: "sales_team_id",
					url: master_url + "api/crm/sales_team/select_list/",
					select_text: select_text
				},
				{
					id: "adviser_id",
					view: "server_autocomplete",
					label: "Vendedor",
					labelPosition: "top",
					name: "adviser_id",
					url: master_url + "api/cat/user/select_list/?por_jerarquia=1&activo=1",
					select_text: select_text
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon", icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						let querystring = "";
						const that = this;
						if (this.$$("filter_form").validate()) {
							const values = that.$$("filterform").getValues();
							if (Object.keys(values).length !== 0) {
								for (let k in values) {
									if (values[k]) {
										querystring += k + "==" + values[k] + ";";
									}
								}
							}
							that._parent.setParam("filter", querystring, true);
						} else {
							webix.message({
								text: "Faltan campos por llenar",
								type: "error",
								expire: -1,
								id: "messageLead"
							});
						}
					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						this._parent.setParam("filter", "", true);
						this.$$("filter_form").clear();
					}
				},
			],
			rules: {
				start_date: webix.rules.isNotEmpty,
				end_date: webix.rules.isNotEmpty,
			}
		};
	}
}