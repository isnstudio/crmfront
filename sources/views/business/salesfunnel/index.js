import {JetView} from "webix-jet";
import FilterListView from "jet-views/business/salesfunnel/listFilters";


export default class salesfunnelView extends JetView {
    config() {
        this.Filters = new FilterListView(this.app);

        const search = {
            view: "toolbar",
            localId: "toolbar",
            paddingX: 24,
            height: 44,
            visibleBatch: "default",
            cols: [
                {
                    view: "label",
                    label: "Embudo de ventas",
                    batch: "default",
                    localId: "label",
                }
            ],
        };

        if(this.app.Mobile) {
            return {
				type: "space",
				view: "scrollview",
				scroll: "auto",
				body: {
					view: "accordion",
					rows: [
						{ 
							view:"accordionitem",
							template: "Filtros",
							header: "Filtros",
							type: "header",
							collapsed: true,
							body: this.Filters,
						},
						{
							view: "template",
							template: "Gráfica de Pronostico",
							type: "header"
						},
						{
							type: "wide",
							height: 600,
							cols: [
								{
									type: "space",
									height: 100,
									$subview: "business.salesfunnel.funnel",
								}
							],
						},
					],
				}
			};
        } 

		return {
			rows: [
				{
					type: "space",
					view: "accordion",
					cols: [
						{
							rows: [
								search,
								{
									type: "space",
									height: 100,
									$subview: "business.salesfunnel.funnel",
								}
							]
						},
						{
							view:"accordionitem",
							template: "Filtros",
							header: "Filtros",
							type: "header",
							collapsed: false,
							body: {
								rows: [
									this.Filters,
									{}
								]
							},
						}
					]
				}
			]
		};
    }
}
