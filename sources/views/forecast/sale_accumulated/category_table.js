import {JetView} from "webix-jet";

export default class CategoryTableView extends JetView {
	config() {
		function status_color(value, config) {
			if (value === "BAJO")
				return {"color": "#ff0000"};
			if (value === "QUE NO PARE")
				return {"color": "#2cbb10"};
		}

		const category_table = {
			view: "datatable",
			id: "category_data",
			rowHeight: 40,
			tooltip: true,
			select: true,
			columns: [
				{
					id: "status",
					header: "Estatus",
					adjust: true,
					template: function (obj) {
						if (obj.total_categoria_sales > obj.total_categoria_metas) {
							return "QUE NO PARE";
						}
						return "BAJO";
					},
					cssFormat: status_color
				},
				{
					id: "categoria",
					header: "Categoría",
					adjust: true,
					hidden: false
				},
				{fillspace: true},
				{
					id: "total_categoria_sales",
					header: {text: "Venta", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "total_categoria_metas",
					header: {text: "Meta", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
			],
			on: {
				onAfterSelect: (id) => {
					this.setParam("category_id", id, true);
					this._parent.UnitsDetailView.refreshDetails(id.id);
					this._parent.HeaderTable.$$("header_data").unselectAll();
				},
			}
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					category_table,
				]
			}
		};
	}

	init() {
		const datatable = this.$$("category_data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}