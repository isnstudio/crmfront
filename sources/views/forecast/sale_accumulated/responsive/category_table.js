import {JetView} from "webix-jet";

export default class ListCategoryView extends JetView {
	config() {
		const category_list = {
			view: "list",
			id: "category_data",
			css: "multi-line-box",
			select: true,
			type: {
				height: "auto",
				template: obj => {
					if (obj.total_categoria_sales > obj.total_categoria_metas) {
						obj.status = "QUE NO PARE";
						obj.color = "#2cbb10";
					} else {
						obj.status = "BAJO";
						obj.color = "#ff0000";
					}
					return `
                        <span style="font-weight: 500;"><b>Sales team: </b>${obj.sales_team}</span>
                        <br>
						<span style="font-weight: 500;"><b>Categoria: </b>${obj.category}</span>
						<br>
						<span style="font-weight: 500;"><b>Estatus: </b><span style="color: ${obj.color}">${obj.status}</span></span>
						<br>
						<span style="font-weight: 500;"><b>Venta: </b> ${obj.total_categoria_sales}</span>
						<br>
						<span style="font-weight: 500;"><b>Meta: </b> ${obj.total_categoria_metas}</span>
                        `
				}
			},
			on: {
				onAfterSelect: (id) => {
					this.setParam("category_id", id, true);
					const team_id = this.getParam("id");
					this.show("/home/forecast.sale_accumulated.category_view?id=" + team_id + "/forecast.sale_accumulated.popups.details?category_id=" + id + "?id=" + team_id);
					this._parent.HeaderTable.$$("header_data").unselectAll();
				},
			}
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					category_list,
				]
			}
		};
	}
}