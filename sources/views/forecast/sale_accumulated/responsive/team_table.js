import {JetView} from "webix-jet";

export default class ListTeamView extends JetView {
	config() {
		const team_list = {
			view: "list",
			id: "team_data",
			css: "multi-line-box",
			select: true,
			type: {
				height: "auto",
				template: obj => {
					if (obj.total_team_sales > obj.total_team_metas) {
						obj.status = "QUE NO PARE";
						obj.color = "#2cbb10";
					} else {
						obj.status = "BAJO";
						obj.color = "#ff0000";
					}

					return `
						<span style="float:right; text-transform: uppercase"><b>${obj.id}</b></span>
						<span style="font-weight: 500;"><b>Equipo: </b>${obj.team}</span>
						<br>
						<span style="font-weight: 500;"><b>Estatus: </b><span style="color: ${obj.color}">${obj.status}</span></span>
						<br>
						<span style="font-weight: 500;"><b>Venta: </b> ${obj.total_team_sales}</span>
						<br>
						<span style="font-weight: 500;"><b>Meta: </b> ${obj.total_team_metas}</span>
						<br>
						<span style="font-weight: 500;"><b>Forecast: </b> ${obj.total_team_forecasts}</span>
						<br>
						<span style="font-weight: 500;"><b>Tibios: </b> ${obj.total_team_warms}</span>`
				}
			},
			on: {
				onAfterSelect: (id) => {
					this._parent.setParam("id", id, true);
					this.show("/home/forecast.sale/forecast.sale_accumulated.popups.details?id=" + id);
					this._parent.HeaderTable.$$("header_data").unselectAll();
				},
			}
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					team_list,
				]
			}
		};
	}
}