import {JetView} from "webix-jet";

export default class AdviserTableView extends JetView {
	config() {
		function status_color(value, config) {
			if (value === "BAJO")
				return {"color": "#ff0000"};
			if (value === "QUE NO PARE")
				return {"color": "#2cbb10"};
		}

		const adviser_table = {
			view: "datatable",
			id: "adviser_data",
			rowHeight: 40,
			tooltip: true,
			select: true,
			columns: [
				{
					id: "status",
					header: "Estatus",
					adjust: true,
					template: function (obj) {
						if (obj.total_adviser_sales > obj.total_adviser_metas) {
							return "QUE NO PARE";
						}
						return "BAJO";
					},
					cssFormat: status_color
				},
				{
					id: "adviser",
					header: "Vendedor",
					adjust: true,
					hidden: false
				},
				{fillspace: true},
				{
					id: "total_adviser_sales",
					header: {text: "Venta", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "total_adviser_metas",
					header: {text: "Meta", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "total_adviser_forecasts",
					header: {text: "Forecast", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "total_adviser_warms",
					header: {text: "Tibios", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
			],
			on: {
				onAfterSelect: (id) => {
					this.setParam("adviser_id", id, true);
					this._parent.UnitsDetailView.refreshDetails(id.id);
					this._parent.HeaderTable.$$("header_data").unselectAll();
				},
			}
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					adviser_table,
				]
			}
		};
	}

	init() {
		const datatable = this.$$("adviser_data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}