import {JetView} from "webix-jet";

export default class HeaderTableView extends JetView {
	config() {
		function status_color(value, config) {
			if (value === "BAJO")
				return {"color": "#ff0000"};
			if (value === "QUE NO PARE")
				return {"color": "#2cbb10"};
		}

		return {
			view: "datatable",
			id: "header_data",
			tooltip: true,
			select: true,
			height: 84,
			rowHeight: 40,
			columns: [
				{fillspace: true},
				{
					id: "header_sales",
					header: {text: "Venta", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "header_metas",
					header: {text: "Meta", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "header_forecasts",
					header: {text: "Forecast", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "header_warms",
					header: {text: "Tibios", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
			],
			on: {
				onAfterSelect: () => {
					this.setParam("id", "0", true);
					this._parent.UnitsDetailView.refreshDetails(0);
					if(this._parent.TeamTable)
						this._parent.TeamTable.$$("team_data").unselectAll();
					else if (this._parent.AdviserTable)
						this._parent.AdviserTable.$$("adviser_data").unselectAll();
				},
				onAfterRender() {
					if (!this.count()) {
						this.showOverlay("<p>No hay información para mostrar</p>");
					} else {
						this.hideOverlay();
					}
				}
			}
		};
	}
}