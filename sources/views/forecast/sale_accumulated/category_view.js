import {JetView} from "webix-jet";

import CategoryTableView from "./category_table";
import HeaderTableView from "jet-views/forecast/sale/header_category_table";
import ListCategoryView from "./responsive/category_table";
import ListHeaderView from "./responsive/header_table";
import UnitsDetailView from "jet-views/forecast/sale/details";
import "webix/breadcrumb";

export default class CategoryView extends JetView {
	config() {
		this.CategoryTable = new (this.app.Mobile ? ListCategoryView : CategoryTableView)(this.app);
		this.HeaderTable = new (this.app.Mobile ? ListHeaderView : HeaderTableView)(this.app);
		if(!this.app.Mobile)
			this.UnitsDetailView = new UnitsDetailView(this.app);

		this.team_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Reporte de venta diaria acumulada", link: "/home/forecast.sale_accumulated"},
				{value: "Categoria de venta"},
			],
		};

		const search = {
			view: "toolbar", localId: "toolbar",
			paddingX: 10, height: 44, visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		if (this.app.Mobile)
			return {
				view: "accordion",
				rows: [
					search,
					{
						view: "accordionitem",
						header: "Totales",
						type: "header",
						height: 175,
						body: this.HeaderTable,
					},
					{
						view: "accordionitem",
						header: "Categorias",
						type: "header",
						body: this.CategoryTable
					},
					{$subview: true, popup: true}
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.HeaderTable,
						this.CategoryTable,
					]
				},
				{
					width: 384,
					rows: [
						this.UnitsDetailView,
					]
				},
			]
		};
	}

	ready(_$view, _$url) {
		super.ready(_$view, _$url);
		setTimeout(function () {
			_$view.$scope.getData()
		}, 50)
	}

	getData() {
		let that = this;
		let team_header = this.HeaderTable.$$("header_data");
		let category_table = this.CategoryTable.$$("category_data");
		webix.ajax().get(master_url + "api/crm/sale/period_sale_categorizacion/?team_id=" + this.team_id + "&detail=1").then(function (response) {
			let data = response.json();
			team_header.clearAll();
			category_table.clearAll();
			that.header = data.header;
			team_header.data_setter(that.header);
			category_table.data_setter(data.lines);
		});

		let warning_sales_budget_estatus = $$("warning_sales_budget_estatus")
		webix.ajax().get(master_url + "api/crm/sale/warning_sales_budget_estatus/").then(function (response) {
			warning_sales_budget_estatus.show();
			warning_sales_budget_estatus.define("template", response.json().mensaje)
			warning_sales_budget_estatus.refresh()
		});
	}

}