import {JetView} from "webix-jet";

export default class UnitsDetailView extends JetView {
	config() {
		const details = {
			rows: [
				{
					view: "property",
					id: "detail_info",
					disabled: true,
					autoheight: true,
					nameWidth: 120,
					elements: [
						{label: "Folio", type: "text", id: "id"},
						{label: "Vendedor", type: "text", id: "adviser__display_webix"},
						{label: "Fecha", type: "text", id: "created_on"},
						{label: "Monto", type: "text", id: "amount", format: webix.i18n.priceFormat},
						{label: "Responsable", type: "text", id: "responsable__display_webix" },
					]
				},
				{
					localId: "detail_components",
					autoheight: true,
					padding: 20,
					css: "wbackground",
					rows: []
				},
				{
					view: "datatable",
					localId: "data_forecast",
					scroll: true,
					columns: [
						{id: "customer", header: "Cliente", fillspace: true},
						{
							id: "amount",
							header: {text: "Monto", css: "right"},
							css: "right",
							format: webix.i18n.priceFormat
						},
					]
				},
				{
					localId: "spacer",
					css: "template-scroll-y",
				}
			]
		};

		return {
			rows: [
				details
			]
		};
	}

	init() {
		this.hideContents();
	}

	hideContents() {
		this.$$("detail_components").hide();
		this.$$("detail_info").hide();
		this.$$("data_forecast").hide();
		this.$$("spacer").show();
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		this.$$("detail_components").show();
		this.$$("detail_info").show();
		this.$$("data_forecast").show();
		this.$$("spacer").hide();
		this.RefreshDetails(id);
	}

	showDetail(id, hide_buttons) {
		if (id && id.constructor === Object && "id" in id) {
			id = id["id"];
		}
		if (hide_buttons)
			this.hide_buttons = hide_buttons;
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				child_ids.push(child_id);
			}
			for (let childIdsKey in child_ids) {
				content.removeView(child_ids[childIdsKey]);
			}
		}
		this.forecast_id = id;
		if (!this.forecast_id)
			this.forecast_id = this.getParam("id");
		if (!isNaN(id) && id !== "") {
			if (this.getRoot()) {
				this.showContents(id);
			}
		}
		if (!isNaN(this.forecast_id) && this.forecast_id !== "" && this.app.Mobile) {
			if (this.getRoot()) {
				this.showContents(this.forecast_id);
			}
		}
	}

	urlChange() {
		this.showDetail();
	}

	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(master_url + "api/crm/forecast/" + id + "/wdetails/");
			const json_response = JSON.parse(xhr.response);
			if ("actions" in json_response && "id" in json_response) {
				const content = this.$$("detail_components");
				if (content) {
					for (const action_key in json_response["actions"]) {
						const action = json_response["actions"][action_key];
						content.addView({
							view: "button",
							label: action["label"],
							height: 40,
							click: () => {
								this.show("/home/forecast.tibios.actions." + action["action"] + "?id=" + json_response["id"]);
							}
						}, 0);
					}
				}
			}
			if ("id" in json_response) {
				const sets = this.$$("detail_info");
				const table = this.$$("data_forecast");
				if (sets) {
					sets.setValues(json_response);
				}
				if (table) {
					table.clearAll();
					table.load(function () {
						return webix.ajax().get(master_url + "api/crm/forecast/" + id + "/wdetails_breakdown/", {});
					});
				}
			}
			if ("traces" in json_response) {
				const traces = this.$$("traces_timeline");
				if (traces) {
					const trace_data = json_response["traces"];
					if (trace_data.length > 0) {
						let data_collection = new webix.DataCollection({data: trace_data});
						traces.sync(data_collection);
						traces.show();
					}
				}
			}
		}
	}
}

