import {JetView} from "webix-jet";
import Errors from "../../../../../helpers/errors";
import "webix/breadcrumb";
import SaveService from "../../../../../services/save";
import "webix/server_autocomplete";

export default class WarmChangeToLostView extends JetView {
	config() {
		this.id_warm = this.getParam("id_warm");
		this.id_forecast = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Forecast", link: "/home/forecast.tibios"},
				{
					value: "#" + this.id_forecast,
					link: "/home/forecast.tibios.actions.details?id=" + this.id_forecast
				},
				{
					value: "Tibio #" + this.id_warm + " - Cambiar a perdido"
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const required_text = "Requerido";


		const comment = {
			view: "textarea",
			name: "comment",
			label: "Comentarios",
			labelPosition: "top",
			placeholder: "Comentarios",
			invalidMessage: required_text,
			height: 75
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Confirmar",
					height: 40,
					css: "webix_primary",
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.edit(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/forecast.tibios.actions.details?id=" + this.id_forecast);
					},
				},
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			scroll: "y",
			rows: [
				comment
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							],
						},
					],
				},
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
						],
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384,
							},
							buttons,
						],
					},
				],
			},
		};
	}

	edit(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/warm/" + that.id_warm + "/change_status_to_lost/",
			get_values: that.$$("form").getValues(),
			validate: this.$$("form").validate(),
			message: "Tibio guardado",
			path: "/home/forecast.tibios.actions.details?id=" + this.id_forecast + "&warm_id=",
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}