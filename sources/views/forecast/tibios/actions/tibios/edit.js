import {JetView} from "webix-jet";
import Errors from "../../../../../helpers/errors";
import "webix/breadcrumb";
import SaveService from "../../../../../services/save";
import "webix/server_autocomplete";

export default class PersonView extends JetView {
	config() {
		this.id_warm = this.getParam("id_warm");
		this.id_forecast = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Forecast", link: "/home/forecast.tibios"},
				{
					value: "#" + this.id_forecast,
					link: "/home/forecast.tibios.actions.details?id=" + this.id_forecast
				},
				{
					value: "Tibio #" + this.id_warm + " - Editar",
					link: "/home/forecast.tibios.actions.tibios.edit?id=" + this.id_forecast + "&id_warm=" + this.id_warm
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const required_text = "Requerido";

		const cliente = {
			view: "server_autocomplete",
			id: "id_cliente",
			name: "cliente",
			label: "Cliente",
			placeholder: "Cliente",
			url: master_url + "api/cxc/cliente/select_list/?status_str=['Activo']",
			required: true
		};

		const lead = {
			id: "lead_id",
			view: "server_autocomplete",
			label: "Lead",
			labelPosition: "top",
			name: "lead",
			placeholder: "Lead",
			url: master_url + "api/crm/lead/select_list/",
			required: true
		};

		const amount = {
			view: "text",
			name: "amount",
			label: "Monto USD",
			labelPosition: "top",
			placeholder: "Monto USD",
			invalidMessage: required_text,
			format: "$1,111.00",
			required: true
		};
		const comment = {
			view: "textarea",
			name: "comment",
			label: "Comentarios",
			labelPosition: "top",
			placeholder: "Comentarios",
			invalidMessage: required_text,
			height: 75
		};

		const left_side = {
			margin: 10,
			rows: [
				amount,
				cliente
			]
		};
		const right_side = {
			margin: 10,
			rows: [
				lead
			]
		};

		const standard_view = {
			margin: 48,
			cols: [
				left_side,
				right_side,
			]
		};

		const responsive_view = {
			margin: 10,
			rows: [
				amount,
				cliente,
				lead
			]
		};

		const main_section = this.app.Mobile ? responsive_view : standard_view;

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Editar",
					height: 40,
					css: "webix_primary",
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.edit(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/forecast.tibios.actions.details?id=" + this.id_forecast);
					},
				},
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			scroll: "y",
			rows: [
				main_section,
				comment
			],
			rules: {
				amount: webix.rules.isNotEmpty,
			}
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							],
						},
					],
				},
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
						],
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384,
							},
							buttons,
						],
					},
				],
			},
		};
	}

	ready() {
		fillForm(this.id_warm);
		const that = this;

		function fillForm(id) {
			if (!isNaN(id) && id !== "") {
				webix.ajax().get(master_url + "api/crm/warm/" + id + "/").then(function (response) {
					that.warm = response.json();
					that.$$("form").setValues(that.warm);
				});
			}
		}

		that.$$("id_cliente").attachEvent("onChange", function (value) {
			if (value !== "") {
				that.$$("lead_id").setValue("")
				that.$$("lead_id").define({ required: false })
				that.$$("lead_id").refresh()
			}
			that.$$("id_cliente").define({ required: true })
			that.$$("id_cliente").refresh()
		})

		that.$$("lead_id").attachEvent("onChange", function (value) {
			if (value !== "") {
				that.$$("id_cliente").setValue("")
				that.$$("id_cliente").define({ required: false })
				that.$$("id_cliente").refresh()
			}
			that.$$("lead_id").define({ required: true })
			that.$$("lead_id").refresh()
		})
	}

	edit(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/warm/" + that.id_warm + "/",
			get_values: that.$$("form").getValues(),
			validate: this.$$("form").validate(),
			message: "Tibio guardado",
			path: "/home/forecast.tibios.actions.details?id=" + this.id_forecast + "&warm_id=",
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}