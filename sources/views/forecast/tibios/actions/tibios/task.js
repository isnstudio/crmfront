import {JetView} from "webix-jet";
import SaveService from "../../../../../services/save";
import TaskWarmDetailView from "./task_detail";
import "webix/server_autocomplete";
import "webix/datepicker_iso";
import "webix/breadcrumb";


export default class TimeLineView extends JetView {
	config() {
		this.id_warm = this.getParam("id_warm");
		this.id_tibio = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Forecast", link: "/home/forecast.tibios"},
				{
					value: "#" + this.id_tibio,
					link: "/home/forecast.tibios.actions.details?id=" + this.id_tibio
				},
				{
					value: "Tibio #" + this.id_warm + " - Seguimiento",
					link: "/home/forecast.tibios.actions.tibios.task?id=" + this.id_tibio + "&id_warm=" + this.id_warm
				},
			],
		};

		const search = {
			view: "toolbar", localId: "toolbar",
			paddingX: 10, height: 44, visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const dateFormat = webix.Date.dateToStr("%Y-%m-%d %H:%i");
		const select_text = "Clic para seleccionar";
		const required_text = "Requerido";
		this.TaskDetailView = new TaskWarmDetailView(this.app);

		const activity = {
			id: "activity",
			name: "activity",
			view: "server_autocomplete",
			label: "Actividad",
			url: master_url + "api/crm/activity/select_list/",
			required: true
		};

		const start_date = {
			view: "datepicker_iso",
			name: "start_date",
			id: "start_date",
			label: "Fecha y hora (Inicio)",
			labelPosition: "top",
			placeholder: select_text,
			timepicker: true,
			required: true,
			format: dateFormat
		};

		const date = {
			view: "datepicker_iso",
			name: "date",
			id: "date",
			label: "Fecha y hora (Fin)",
			labelPosition: "top",
			placeholder: select_text,
			timepicker: true,
			required: true,
			format: dateFormat
		};

		const reminder_time = {
			view: "combo",
			name: "reminder_time",
			localId: "reminder_time:combo",
			id: "reminder_time",
			label: "Notificación",
			labelPosition: "top",
			placeholder: select_text,
			invalidMessage: required_text,
			suggest: {url: master_url + "api/crm/task/choice_field/reminder_time/"},
			required: true,
		};

		const responsable = {
			id: "responsable",
			name: "responsable",
			view: "server_autocomplete",
			label: "Responsable (En caso de que no sea el Encargado de la Actividad)",
			url: master_url + "api/crm/user/select_list/?activo=1",
		};
		const description = {
			view: "textarea",
			name: "description",
			id: "description",
			label: "Comentarios",
			labelPosition: "top",
			placeholder: "Comentarios",
			invalidMessage: required_text,
			height: 75
		};

		const left_side = {
			margin: 10,
			rows: [
				activity,
				start_date,
				reminder_time,
			]
		};
		const right_side = {
			margin: 10,
			rows: [
				{},
				date,
				responsable,
			]
		};

		const standard_view = {
			margin: 48,
			cols: [
				left_side,
				right_side
			]
		};

		const responsive_view = {
			margin: 10,
			rows: [
				activity,
				date,
				reminder_time,
				responsable,
			]
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);

		const buttons = {
			autoheight: true,
			css: "wbackground",
			id: "buttons",
			padding: 20,
			rows: [
				{
					view: "button",
					id: "save",
					value: "Agregar",
					height: 40,
					css: "webix_primary",
					click: () => {
						const button = this.$$("save");
						this.create(button);
					},
				},
				{
					view: "button",
					value: "Regresar",
					id: "cancel",
					height: 40,
					click: () => {
						this.show("/home/forecast.tibios.actions.details?id=" + this.id_tibio + "&id_warm=" + this.id_warm);
					}
				},
			]
		};

		const list = {
			view: "datatable",
			localId: "data",
			id: "data",
			scroll: "xy",
			select: true,
			rowHeight: 40,
			columns: [
				{id: "activity", header: "Actividad", adjust: true},
				{id: "description", header: "Comentarios", adjust: true},
				{id: "status", header: "Estatus", adjust: true},
				{fillspace: true},
				{ id: "date", header: { text: "Fecha límite", css: "right" }, css: "right", adjust: true, minWidth: 150 },
				{ id: "created_on", header: { text: "Fecha creación", css: "right" }, css: "right", adjust: true, minWidth: 150 },
			],
			on: {
				onAfterSelect: (id) => {
					if (this.app.Mobile) {
						this.show("/home/forecast.tibios.actions.tibios.task?id=" + this.id_tibio + "&id_warm=" + this.id_warm +
							"/forecast.tibios.actions.tibios.popup_activities?id=" + this.id_tibio + "&id_warm=" + this.id_warm + "&task_id=" + id);
					} else this.setParam("task_id", id, true);
				},
				onAfterRender() {
					const that = this;
					if (!that.count()) {
						that.showOverlay("<p>No hay información que mostrar</p>");
					} else {
						that.hideOverlay();
					}
				},
			}
		};


		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			scroll: "y",
			rows: [
				main_section,
				description
			]
		};

		if (this.app.Mobile)
			return {
				type: "space",
				rows: [
					search,
					buttons,
					form,
					list,
					{$subview: true, popup: true}
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
							list
						]
					},
					{
						localId: "right_space",
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons,
							this.TaskDetailView,
						]
					}
				]
			}
		};
	}

	ready(ui, url) {
		const that = this;
		const table = this.$$("data");
		let id = null;
		if (url.length === 1) {
			if ("task_id" in url[0].params) {
				id = url[0].params["task_id"];
				if (table.exists(id)) {
					table.select(id);
					table.showItem(id);
				}
			} else {
				table.unselect();
			}
		}
		table.clearAll();
		table.load(function () {
			return webix.ajax().get(master_url + "api/crm/warm/" + that.id_warm + "/tasks/");
		});

		$$("activity").attachEvent("onChange", function (value) {
			if (value && value.constructor !== Object) {
				var xhr = webix.ajax().sync().get(master_url + "api/crm/activity/" + value + "/has_responsable/");
				const json_response = JSON.parse(xhr.response);
				const date = $$("date");
				const reminder_time = $$("reminder_time");
				const description = $$("description");
				if (json_response.has_responsable) {
					date.define("required", true);
					reminder_time.define("required", true);
					description.define("required", false);
				} else {
					date.define("required", false);
					reminder_time.define("required", false);
					description.define("required", true);
				}
				date.refresh();
				reminder_time.refresh();
				description.refresh();
			}
		});
	}

	create(button) {
		const that = this;
		var get_values = this.$$("form").getValues();
		get_values["warm"] = this.id_warm;
		get_values["task_type"] = 2;

		const request_options = {
			url: master_url + "api/crm/task/",
			get_values: get_values,
			validate: this.$$("form").validate(),
			message: "Tarea guardada",
			button: button,
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}