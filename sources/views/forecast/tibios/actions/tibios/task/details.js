import {JetView} from "webix-jet";
import "webix/server_autocomplete";
import "webix/breadcrumb";


export default class TibioDetailView extends JetView {
	config() {
		const seguimiento = {
			view: "button",
			name: "seguimiento",
			css: "webix_secondary",
			id: "seguimiento",
			label: "Seguimiento",
			hidden: true,
			height: 40,
			click: () => {
				this.id_forecast = this.getParam("id");
				this.id_warm = this.getParam("id_warm");
				this.show("/home/forecast.tibios.actions.tibios.task?id=" + this.id_forecast + "&id_warm=" + this.id_warm);
			}
		};

		const editar = {
			view: "button",
			name: "editar",
			css: "webix_secondary",
			id: "editar",
			label: "Editar",
			height: 40,
			hidden: true,
			click: () => {
				this.id_forecast = this.getParam("id");
				this.id_warm = this.getParam("id_warm");
				this.show("/home/forecast.tibios.actions.tibios.edit?id=" + this.id_forecast + "&id_warm=" + this.id_warm);
			}
		};

		const change_status_to_lost = {
			view: "button",
			name: "change_status_to_lost",
			css: "webix_secondary",
			id: "change_status_to_lost",
			label: "Cambiar a perdido",
			height: 40,
			hidden: true,
			click: () => {
				this.id_forecast = this.getParam("id");
				this.id_warm = this.getParam("id_warm");
				this.show("/home/forecast.tibios.actions.tibios.change_to_lost?id=" + this.id_forecast + "&id_warm=" + this.id_warm);
			}
		};

		return {
			localId: "detalle",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				seguimiento,
				editar,
				change_status_to_lost
			]
		};
	}

	ready() {
		if (this.app.Mobile){
			this.$$("editar").show();
			this.$$("seguimiento").show();
			this.$$("change_status_to_lost").show();
		}
	}

}