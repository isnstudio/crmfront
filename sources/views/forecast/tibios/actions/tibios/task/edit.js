import {JetView, plugins} from "webix-jet";
import Errors from "../../../../../../helpers/errors.js";
import "webix/server_autocomplete";
import "webix/datepicker_iso";
import "webix/breadcrumb";
import SaveService from "../../../../../../services/save";

export default class TaskConcludeView extends JetView {
	config() {
		const dateFormat = webix.Date.dateToStr("%Y-%m-%d %H:%i");
		const select_text = "Clic para seleccionar";
		const required_text = "Requerido";
		this.forecast_id = this.getParam("id");
		this.warm_id = this.getParam("warm_id");
		this.task_id = this.getParam("task_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Forecast", link: "/home/forecast.tibios?id=" + this.forecast_id},
				{
					value: "#" + this.forecast_id,
					link: "/home/forecast.tibios.actions.details?id=" + this.forecast_id + "&id_warm=" + this.warm_id
				},
				{
					value: "Tibio #" + this.warm_id + " - Seguimiento",
					link: "/home/forecast.tibios.actions.tibios.task?id=" + this.forecast_id + "&id_warm=" + this.warm_id + "&task_id=" + this.task_id
				},
				{
					value: "Tarea #" + this.task_id + " - Editar",
					link: "/home/forecast.tibios.actions.tibios.task.editar?id=" + this.forecast_id + "&id_warm=" + this.warm_id + "&task_id=" + this.task_id
				},
			],
		};

		const search = {
			view: "toolbar", localId: "toolbar",
			paddingX: 10, height: 44, visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const activity = {
			id: "activity",
			name: "activity",
			view: "server_autocomplete",
			label: "Actividad",
			url: master_url + "api/crm/activity/select_list/",
			required: true
		};
		const date = {
			view: "datepicker_iso",
			name: "date",
			id: "date",
			label: "Fecha y hora",
			labelPosition: "top",
			placeholder: select_text,
			timepicker: true,
			required: true,
			format: dateFormat
		};
		const reminder_time = {
			view: "combo",
			name: "reminder_time",
			localId: "reminder_time:combo",
			id: "reminder_time",
			label: "Notificación",
			labelPosition: "top",
			placeholder: select_text,
			invalidMessage: required_text,
			suggest: {url: master_url + "api/crm/task/choice_field/reminder_time/"},
			required: true,
		};
		const responsable = {
			id: "responsable",
			name: "responsable",
			view: "server_autocomplete",
			label: "Responsable",
			url: master_url + "api/cat/user/select_list/?activo=1",
		};

		const description = {
			view: "textarea",
			name: "description",
			id: "description",
			label: "Comentarios",
			labelPosition: "top",
			placeholder: "Comentarios",
			invalidMessage: required_text,
			height: 75
		};

		const left_side = {
			margin: 10,
			rows: [
				activity,
				reminder_time,
			]
		};
		const right_side = {
			margin: 10,
			rows: [
				date,
				responsable,
			]
		};

		const standard_view = {
			margin: 48,
			cols: [
				left_side,
				right_side
			]
		};

		const responsive_view = {
			margin: 10,
			rows: [
				activity,
				date,
				reminder_time,
				responsable,
			]
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);


		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [
				main_section,
				description
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					css: "webix_primary",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.edit(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/forecast.tibios.actions.tibios.task?id=" + this.forecast_id + "&id_warm=" + this.warm_id + "&task_id=" + this.task_id);
					}
				},
			]
		};


		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons,
						]
					}
				]
			}
		};
	}

	ready() {
		const that = this;
		webix.ajax().get(master_url + "api/crm/task/" + this.task_id + "/").then(function (response) {
			setTimeout(function () {
				that.task = response.json();
				that.$$("form").setValues(that.task);
			}, 500);
		});

		$$("activity").attachEvent("onChange", function (value) {
			if (value && value.constructor !== Object) {
				var xhr = webix.ajax().sync().get(master_url + "api/crm/activity/" + value + "/has_responsable/");
				const json_response = JSON.parse(xhr.response);
				const date = $$("date");
				const reminder_time = $$("reminder_time");
				const description = $$("description");
				if (json_response.has_responsable) {
					date.define("required", true);
					reminder_time.define("required", true);
					description.define("required", false);
				} else {
					date.define("required", false);
					reminder_time.define("required", false);
					description.define("required", true);
				}
				date.refresh();
				reminder_time.refresh();
				description.refresh();
			}
		});
	}

	edit(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/task/" + that.task_id + "/",
			get_values: this.$$("form").getValues(),
			validate: this.$$("form").validate(),
			message: "Tarea editada",
			path: "/home/forecast.tibios.actions.tibios.task?id=" + that.forecast_id + "&id_warm=" + that.warm_id + "&task_id=" + that.task_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}