import {JetView} from "webix-jet";
import SaveService from "../../../../../../services/save";
import "webix/breadcrumb";

export default class TaskConcludeView extends JetView {
	config() {
		const required_text = "Requerido";

		this.forecast_id = this.getParam("id");
		this.warm_id = this.getParam("warm_id");
		this.task_id = this.getParam("task_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Forecast", link: "/home/forecast.tibios?id=" + this.forecast_id},
				{
					value: "#" + this.forecast_id,
					link: "/home/forecast.tibios.actions.details?id=" + this.forecast_id + "&id_warm=" + this.warm_id
				},
				{
					value: "Tibio #" + this.warm_id + " - Seguimiento",
					link: "/home/forecast.tibios.actions.tibios.task?id=" + this.forecast_id + "&id_warm=" + this.warm_id + "&task_id=" + this.task_id
				},
				{
					value: "Tarea #" + this.task_id + " - Cancelar",
					link: "/home/forecast.tibios.actions.tibios.task.cancel?id=" + this.forecast_id + "&id_warm=" + this.warm_id + "&task_id=" + this.task_id
				},
			],
		};

		const search = {
			view: "toolbar", localId: "toolbar",
			paddingX: 10, height: 44, visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const cancel_motive = {
			view: "textarea",
			name: "cancel_motive",
			label: "Motivo",
			labelPosition: "top",
			placeholder: "Motivo",
			invalidMessage: required_text,
			height: 150,
			required: true,
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [
				{
					template: "¿Estás seguro de marcar la tarea como no realizada?",
					type: "label",
					borderless: true,
					css: "question"
				},
				cancel_motive
			],
			rules: {
				cancel_motive: webix.rules.isNotEmpty,
			}
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					css: "webix_primary",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/forecast.tibios.actions.tibios.task?id=" + this.forecast_id + "&id_warm=" + this.warm_id + "&task_id=" + this.task_id);
					}
				},
			]
		};


		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons
						]
					}
				]
			}
		};
	}

	action(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/task/" + this.task_id + "/cancel/",
			get_values: this.$$("form").getValues(),
			validate: this.$$("form").validate(),
			message: "Tarea no realizada",
			path: "/home/forecast.tibios.actions.tibios.task?id=" + that.forecast_id + "&id_warm=" + that.warm_id + "&task_id=" + that.task_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}
