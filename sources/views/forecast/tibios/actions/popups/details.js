import ModalView from "views/sections/modal";
import TibioDetailView from "../tibios/task/details";

export default class UnitDetailsPopupView extends ModalView {
	constructor(app) {
		super(app, {
			header: {
				view: "toolbar",
				height: 56,
				padding: {left: 6, right: 14},
				cols: [
					{view: "icon", icon: "wxi-close", click: () => this.Hide()},
					{view: "label", id: "folio", css: "details_popup_header_label", label: "Detalle"},
				]
			},
			body: TibioDetailView
		});
	}

	ready() {
		this.$$("folio").setValue("Tibio #" + this.getParam("id_warm"));
	}
}