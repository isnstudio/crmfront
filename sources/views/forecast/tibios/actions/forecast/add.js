import {JetView} from "webix-jet";
import SaveService from "../../../../../services/save";
import "webix/server_autocomplete";
import "webix/breadcrumb";

export default class PersonView extends JetView {
	config() {
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Forecast", link: "/home/forecast.tibios"},
				{value: "Agregar", link: "/home/forecast.tibios.actions.forecast.add"},
			],
		};

		const search = {
			view: "toolbar", localId: "toolbar",
			paddingX: 10, height: 44, visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const required_text = "Requerido";
		const amount = {
			view: "text",
			name: "amount",
			label: "Forecast",
			labelPosition: "top",
			placeholder: "Forecast",
			invalidMessage: required_text,
			tooltip: "Forecast: " + "#value#",
			format: "$1,111.00",
			required: true,
		};
		const adviser = {
			view: "server_autocomplete",
			name: "adviser",
			id: "adviser",
			label: "Vendedor",
			labelPosition: "top",
			placeholder: "Clic para seleccionar",
			invalidMessage: required_text,
			url: master_url + "api/crm/user/select_list/?por_jerarquia=1&activo=1",
			required: true,
		};
		const responsable = {
			view: "server_autocomplete",
			name: "responsable",
			id: "responsable",
			label: "Responsable",
			labelPosition: "top",
			placeholder: "Clic para seleccionar",
			invalidMessage: required_text,
			url: master_url + "api/crm/user/select_list/",
			required: true,
		};

		const left_side = {
			margin: 10,
			rows: [amount, responsable],
		};

		const right_side = {
			margin: 10,
			rows: [adviser],
		};

		const responsive_view = {
			margin: 10,
			rows: [amount, adviser],
		};
		const standard_view = {
			margin: 48,
			cols: [left_side, right_side],
		};

		const main_section = this.app.Mobile ? responsive_view : standard_view;

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Agregar",
					height: 40,
					css: "webix_primary",
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.create(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/forecast.tibios?flush=true");
					},
				},
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [main_section],
			rules: {
				amount: webix.rules.isNotEmpty,
				adviser: webix.rules.isNotEmpty,
			},
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							],
						},
					],
				},
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
						],
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384,
							},
							buttons,
						],
					},
				],
			},
		};
	}

	create(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/forecast/",
			get_values: that.$$("form").getValues(),
			validate: this.$$("form").validate(),
			message: "Forecast guardado",
			path: "/home/forecast.tibios/",
			button: button,
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}

	ready() {
		const that = this;
		webix.ajax().get(master_url + "api/crm/forecast/init/").then(function (response) {
			const json_response = response.json();
			that.$$("form").setValues(json_response, true);
		});
	}
}
