import {JetView} from "webix-jet";
import SaveService from "../../../../services/save";
import "webix/breadcrumb";
import TibioDetailView from "./tibios/task/details";

export default class ForecastDetailView extends JetView {
	config() {
		this.id_forecast = this._data.id;
		this.id_warm = this.getParam("id_warm");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Forecast", link: "/home/forecast.tibios"},
				{
					value: "#" + this.id_forecast + " - Tibios",
					link: "/home/forecast.tibios.actions.details?id=" + this.id_forecast
				},
			],
		};

		const required_text = "Requerido";

		const cliente = {
			view: "server_autocomplete",
			id: "id_cliente",
			name: "cliente",
			label: "Cliente",
			placeholder: "Cliente",
			url: master_url + "api/cxc/cliente/select_list/?status_str=['Activo']",
			required: true
		};

		const lead = {
			id: "lead_id",
			view: "server_autocomplete",
			label: "Lead",
			labelPosition: "top",
			name: "lead",
			placeholder: "Lead",
			url: master_url + "api/crm/lead/select_list/?create_task_or_warm=1",
			required: true
		};

		const amount = {
			view: "text",
			name: "amount",
			label: "Monto USD",
			labelPosition: "top",
			placeholder: "Monto USD",
			invalidMessage: required_text,
			format: "$1,111.00",
			required: true
		};
		const comment = {
			view: "textarea",
			name: "comment",
			label: "Comentarios",
			labelPosition: "top",
			placeholder: "Comentarios",
			invalidMessage: required_text,
			height: 75,
			required: true
		};

		const left_side = {
			margin: 10,
			rows: [
				amount,
				cliente
			]
		};
		const right_side = {
			margin: 10,
			rows: [
				lead
			]
		};

		const standard_view = {
			margin: 48,
			cols: [
				left_side,
				right_side,
			]
		};

		const responsive_view = {
			margin: 10,
			rows: [
				amount,
				cliente,
				lead
			]
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					height: 40,
					css: "webix_primary",
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.create(button);
					},
				},
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/forecast.tibios");
					}
				},
			]
		};

		this.TibioDetailView = new TibioDetailView(this.app);

		function customCheckbox(obj, common, value) {
			if (value) {
				return "<span class='webix_table_checkbox is_closed'> SI </span>";
			} else {
				return "<span class='webix_table_checkbox is_not_closed'> NO </span>";
			}
		}

		const list = {
			view: "datatable",
			id: "warms",
			localId: "data",
			scroll: "y",
			select: true,
			autoheight: true,
			rowHeight: 40,
			url: master_url + "api/crm/forecast/" + this.id_forecast + "/wdetail_list/",
			columns: [
				{id: "cliente__display_webix", header: "Cliente", adjust: true},
				{ id: "lead__display_webix", header: "Lead", adjust: true },
				{id: "is_closed", header: "Cerrado", template: customCheckbox},
				{id: "comment", header: "Comentarios", fillspace: true},
				{ id: "get_status_display", header: "Status", adjust: true },
				{id: "date", header: {text: "Fecha", css: "right"}, css: "right"},
				{
					id: "amount",
					header: {text: "Monto", css: "right"},
					css: "right",
					format: webix.i18n.priceFormat,
					adjust: true
				},
			],
			on: {
				onAfterRender() {
					const that = this;
					if (!that.count()) {
						that.showOverlay("<p>Nothing here</p>");
					} else {
						that.hideOverlay();
					}
				},
				onAfterSelect: (id) => {
					if (!this.app.Mobile) {
						this.warm_selectd = id;
						this.$$("detalle_header").show();
						this.TibioDetailView.$$("detalle").show();
						this.TibioDetailView.$$("seguimiento").show();
						this.TibioDetailView.$$("editar").show();
						this.setParam("id_warm", id, true);
					} else {
						this.show("/home/forecast.tibios.actions.details?id=" + this.id_forecast + "&id_warm=" + id + "/forecast.tibios.actions.popups.details?id=" + this.id_forecast + "&id_warm=" + id);
					}
				},
			}
		};

		const form = {
			view: "form",
			localId: "form",
			rows: [
				main_section,
				comment,
			],
			rules: {
				amount: webix.rules.isNotEmpty,
			}
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					{
						view: "scrollview",
						scroll: "y",
						body: {
							rows:[
								form,
								list
							]
						},
					},
					{$subview: true, popup: true}
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						{
							view: "property",
							id: "information",
							disabled: true,
							autoheight: true,
							nameWidth: 130,
							elements: [
								{label: "Folio", type: "text", id: "id"},
								{label: "Vendedor", type: "text", id: "adviser__display_webix"},
								{label: "Monto", type: "text", id: "amount", format: webix.i18n.priceFormat},
								{label: "Responsable", type: "text", id: "responsable__display_webix"},
							]
						},
						{
							view: "scrollview",
							scroll: "y",
							body: {
								rows:[
									form,
									list
								]
							},
						},

					]
				},
				{
					rows: [
						{
							view: "template",
							template: "Acciones",
							type: "header",
							width: 384
						},
						buttons,
						{
							localId: "detalle_header",
							view: "template",
							template: "Detalle",
							type: "header",
							hidden: true
						},
						this.TibioDetailView,
						{
							localId: "spacer",
							css: "template-scroll-y",
							borderless: true,
							template: function (obj) {
								return "";
							}
						}
					]
				}
			]
		};
	}

	ready(_$view, _$url) {
		super.ready(_$view, _$url);
		const that = this;

		that.$$("id_cliente").attachEvent("onChange", function (value) {
			if (value !== ""){
				that.$$("lead_id").setValue("")
				that.$$("lead_id").define({ required: false })
				that.$$("lead_id").refresh()
			}
			that.$$("id_cliente").define({ required: true })
			that.$$("id_cliente").refresh()
		})

		that.$$("lead_id").attachEvent("onChange", function (value) {
			if (value !== "") {
				that.$$("id_cliente").setValue("")
				that.$$("id_cliente").define({ required: false })
				that.$$("id_cliente").refresh()
			}
			that.$$("lead_id").define({ required: true })
			that.$$("lead_id").refresh()
		})
	}

	urlChange() {
		const form = this.$$("form");
		let that = this;
		form.clear();
		that.TibioDetailView.$$("seguimiento").hide();
		that.TibioDetailView.$$("editar").hide();
		that.TibioDetailView.$$("change_status_to_lost").hide();
		if (!this.app.Mobile && this.warm_selectd) {
			const information = this.$$("information");
			var xhr = webix.ajax().sync().get(master_url + "api/crm/warm/" + this.warm_selectd + "/wdetails/");
			const json_response = JSON.parse(xhr.response);
			json_response.actions.forEach(function(button){
				that.TibioDetailView.$$(button.action).show();
			})
			information.setValues(json_response);
		}
	}

	create(button) {
		const that = this;
		var get_values = this.$$("form").getValues();
		get_values["forecast"] = this.id_forecast;

		const request_options = {
			url: master_url + "api/crm/warm/",
			get_values: get_values,
			validate: this.$$("form").validate(),
			message: "Tibio guardado",
			button: button,
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}
