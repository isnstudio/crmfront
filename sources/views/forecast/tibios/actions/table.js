import {JetView} from "webix-jet";
import PagerView from "../../../common/pager";

export default class TableView extends JetView {
	config() {
		return {
			rows: [
				{
					view: "datatable",
					localId: "data",
					css: "units_table",
					pager: "pager",
					url: master_url + "api/crm/warm/?",
					columns: [
						{
							id: "adviser",
							header: "Vendedor",
							fillspace: true,
						},
						{
							id: "date",
							header: {text: "Fecha", css: "right"},
							fillspace: true,
							css: "right"
						},
						{
							id: "amount",
							header: {text: "Monto", css: "right"},
							fillspace: true,
							css: "right",
							format: webix.i18n.priceFormat
						},
					],
					on: {
						onAfterRender() {
							const that = this;
							if (!that.count()) {
								that.showOverlay("<p>Nothing here</p>");
							} else {
								that.hideOverlay();
							}
							setTimeout(function () {
								that.enable();
							}, 1000);
						}
					}
				},
				PagerView
			]
		};

	}

	init() {
		this.webix.extend(this.$$("data"), webix.OverlayBox);
		this.webix.extend(this.$$("data"), webix.ProgressBar);
	}

	urlChange(ui, url) {
		const month = this.getParam("month");
		const table = this.$$("data");
		table.disable();
		if(!this.app.Mobile)
			table.showProgress({type: "icon"});

		const flush = this.getParam("flush");
		const _cache = `${month}`;
		if ((this._cache == _cache) && !flush) {
			setTimeout(function () {
				table.enable();
			}, 1000);
			return;
		}

		this._cache = _cache;
		if (flush) {
			this.setParam("flush", false);
			table.clearAll();
			table.load(function () {
				return webix.ajax().get(master_url + "api/crm/forecast/wlist/", {});
			});
		}
	}
}