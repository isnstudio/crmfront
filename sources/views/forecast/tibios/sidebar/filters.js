import {JetView} from "webix-jet";
import "webix/server_autocomplete";

export default class FilterListView extends JetView {
	config() {
		const select_text = "Clic para seleccionar";

		const form = {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			scroll: "y",
			paddingX: 20,
			paddingY: 10,
			rows: [
                {
					id: "adviser",
					view: "server_autocomplete",
					label: "Vendedor",
					labelPosition: "top",
					name: "adviser_id",
					placeholder: "Vendedor",
					url: master_url + "api/crm/user/select_list/?por_jerarquia=1&es_vendedor=true",
					select_text: select_text,
				},
				{
					id: "sales_team",
					view: "server_autocomplete",
					label: "Sales team",
					labelPosition: "top",
					name: "sales_team_id",
					placeholder: "Sales Team",
					url: master_url + "api/crm/sales_team/select_list/",
					select_text: select_text,
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon", icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						let querystring = "";
						const values = this.$$("filterform").getValues();
						if (Object.keys(values).length !== 0) {
							for (let k in values) {
								if (values[k]) {
									querystring += k + "==" + values[k] + ";";
								}
							}
						}

						let table = this._parent.Table.$$("data");
						let current_querystring = this._parent.getParam("filter");
						if (querystring != current_querystring) {
							table.clearAll();
						}
						this._parent.setParam("page", 0, false);
						this._parent.setParam("start", 0, false);
						this._parent.setParam("filter", querystring, true);
					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						let table = this._parent.Table.$$("data");
						this._parent.setParam("page", 0, false);
						this._parent.setParam("start", 0, false);
						this._parent.setParam("filter", "", true);
						this.$$("filter_form").clear();
						table.clearAll();
					}
				}
			]
		};

		return {
			view: "accordionitem",
			template: "Filtros",
			header: "Filtros",
			type: "header",
			id: "filters_header",
			collapsed: false,
			body: form,
		};
	}
}