import {JetView} from "webix-jet";
import PagerView from "../../common/pager";

export default class TableView extends JetView {
	config() {
		return {
			rows: [
				{
					view: "datatable",
					localId: "data",
					id: "data",
					select: true,
					pager: "pager",
					rowHeight: 40,
					url: master_url + "api/crm/forecast/wlist/",
					columns: [
						{
							id: "adviser__display_webix",
							header: "Vendedor",
							fillspace: true,
						},
						{
							id: "created_on",
							header: {text: "Fecha", css: "right"},
							fillspace: true,
							css: "right"
						},
						{
							id: "amount",
							header: {text: "Monto", css: "right"},
							fillspace: true,
							css: "right",
							format: webix.i18n.priceFormat
						},
					],
					on: {
						onAfterRender() {
							const that = this;
							if (!that.count()) {
								that.showOverlay("<p>No hay información para mostrar, verifique los filtros</p>");
							} else {
								that.hideOverlay();
							}
							setTimeout(function () {
								that.enable();
							}, 1000);
						},
						onAfterSelect: (id) => {
							this.setParam("id", id, true);
							this._parent.UnitsDetailView.showDetail(id);
						}
					}
				},
				PagerView
			]
		};

	}

	init() {
		this.webix.extend(this.$$("data"), webix.OverlayBox);
		this.webix.extend(this.$$("data"), webix.ProgressBar);
	}
}