import {JetView} from "webix-jet";
import TableView from "./table";
import ListView from "./responsive/table";
import UnitsDetailView from "./details";
import "webix/breadcrumb";
import Tables from "../../../helpers/tables";
import FilterListView from "./sidebar/filters";

export default class forecastView extends JetView {
	config() {
		this.Filters = new FilterListView(this.app);
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Forecast", link: "/home/forecast.sale"},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
				{
					view: "icon",
					icon: "mdi mdi-plus",
					localId: "addButton",
					css: "right_icon",
					click: () => {
						this.show("../forecast.tibios.actions.forecast.add");
					},
				}
			]
		};

		this.Table = new (this.app.Mobile ? ListView : TableView)(this.app);
		this.UnitsDetailView = new UnitsDetailView(this.app);

		if (this.app.Mobile)
			return {
				rows: [
					this.Filters,
					search,
					this.Table,
					{$subview: true, popup: true},
				],
			};

		return {
			type: "space",
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [search, this.Table],
					},
					{
						view: "accordion",
						rows: [
							this.Filters,
							{
								view: "template",
								template: "Detalles",
								type: "header",
								width: 384
							},
							this.UnitsDetailView,
						]
					},
				],
			},
		};
	}

	ready(view) {
		let that = this;
		setTimeout(function () {
			let datagrid = view.$scope.Table.$$("data");
			let pager = datagrid.getPager();
			pager.attachEvent("onBeforePageChange", function (new_page) {
				let start = new_page * pager.data.size;
				let count = pager.data.size;
				view.$scope.setParam("start", start, false);
				view.$scope.setParam("count", count, false);
				view.$scope.setParam("page", new_page, true);
			});
			let adviser_id = view.$scope.getParam("adviser_id");
			if (adviser_id) {
				let querystring = "adviser_id==" + adviser_id;
				that.Filters.$$("adviser").setValue(adviser_id)
				let table = that.Table.$$("data");
				webix.ajax().get(master_url + "api/crm/forecast/wlist/?filter=" + querystring).then(function (response) {
					let data = response.json().data;
					table.clearAll();
					table.define({ "data": data });
					table.refresh();
				});
			}
		}, 500);
	}

	urlChange() {
		const url = master_url + "api/crm/forecast/wlist/";
		const table = this.Table.$$("data");
		const tables = new Tables(this.app);
		tables.dataLoading(table, url, this);
	}
}
