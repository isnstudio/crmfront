import {JetView} from "webix-jet";
import PagerView from "../../../common/pager";

export default class ListView extends JetView {
	config() {
		return {
			type: "space",
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					{
						view: "list",
						id: "data",
						css: "multi-line-box",
						select: true,
						pager: "pager",
						type: {
							height: "auto",
							template: obj => `
                                    <span style="font-weight: 500;"><b>Vendedor: </b>${obj.adviser__display_webix}</span>
                                    <br>
                                    <span style="font-weight: 500;"><b>Fecha: </b> ${obj.created_on}</span>
                                    <br>
                                    <span style="font-weight: 500;"><b>Monto: </b> ${obj.amount}</span>`
						},
						on: {
							onItemClick: (id) => {
								this.show("/home/forecast.tibios/forecast.tibios.popups.details?id=" + id);
							}
						}
					},
					PagerView
				]
			}
		};
	}
}