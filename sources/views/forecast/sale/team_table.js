import {JetView} from "webix-jet";
import PagerView from "jet-views/common/pager";

export default class TeamTableView extends JetView {
	config() {
		function status_color(value, ) {
			if (value === "BAJO")
				return {"color": "#ff0000"};
			if (value === "QUE NO PARE")
				return {"color": "#2cbb10"};
		}
		var socket_url = SOCKET_URL;

		socket_url += "notifications/";
		const socket = new WebSocket(socket_url);

		socket.onmessage = (e) => {
			if (e.data === "notification_crm_sale_daily_sale") {
				const table = this.$$("data");
				const table_detail = this.$$("data_sale");

				table.clearAll();
				table_detail.clearAll();

				var xhr = webix.ajax().sync().get(master_url + "api/crm/sale/daily_sale/");
				const data = JSON.parse(xhr.response);

				table.data_setter(data.header);
				table_detail.data_setter(data.lines);

			}
		};

		const team_table = {
			view: "datatable",
			localId: "team_data",
			tooltip: true,
			select: true,
			rowHeight: 40,
			columns: [
				{
					id: "status",
					header: "Estatus",
					adjust: true,
					template: function (obj) {
						if (obj.total_team_sales > obj.total_team_metas) {
							return "QUE NO PARE";
						}
						return "BAJO";
					},
					cssFormat: status_color
				},
				{
					id: "team",
					header: "Equipo",
					adjust: true,
					hidden: false
				},
				{fillspace: true},
				{
					id: "total_team_sales",
					header: {text: "Venta", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "total_team_metas",
					header: {text: "Meta", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "total_team_forecasts",
					header: {text: "Forecast", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "total_team_warms",
					header: {text: "Tibios", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				}
			],
			on: {
				onAfterRender() {
					const that = this;
					if (!that.count()) {
						that.showOverlay("<p>No hay información para mostrar</p>");
					} else {
						that.hideOverlay();
					}
					setTimeout(function () {
						that.enable();
					}, 1000);
				},
				onAfterSelect: (id) => {
					let that = this;
					this._parent.setParam("id", id, true);
					this._parent.UnitsDetailView.refreshDetails(id.id);
					this._parent.HeaderTable.$$("header_data").unselectAll();
					let record = this.$$("team_data").getItem(id);
					if (record.no_es_gerente){
						setTimeout(function(){
							that._parent.UnitsDetailView.$$("category_button").hide()
						}, 50)
					}
				},
			}
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					team_table,
				]
			}
		};
	}

	init() {
		const datatable = this.$$("team_data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}