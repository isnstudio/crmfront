import {JetView} from "webix-jet";

import TeamTableView from "./team_table";
import HeaderTableView from "jet-views/forecast/sale/header_table";
import ListTeamView from "./responsive/team_table";
import ListHeaderView from "./responsive/header_table";
import UnitsDetailView from "jet-views/forecast/sale/details";
import "webix/breadcrumb";

export default class forecastView extends JetView {
	config() {
		this.TeamTable = new (this.app.Mobile ? ListTeamView : TeamTableView)(this.app);
		this.HeaderTable = new (this.app.Mobile ? ListHeaderView : HeaderTableView)(this.app);
		if(!this.app.Mobile)
			this.UnitsDetailView = new UnitsDetailView(this.app);

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Reporte de venta diaria", link: "/home/forecast.sale"},
			],
		};

		const search = {
			view: "toolbar", localId: "toolbar",
			paddingX: 10, height: 44, visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		if (this.app.Mobile)
			return {
				view: "accordion",
				rows: [
					search,
					{
						view: "accordionitem",
						header: "Totales",
						type: "header",
						height: 175,
						body: this.HeaderTable,
					},
					{
						view: "accordionitem",
						header: "Equipos",
						type: "header",
						body: this.TeamTable,
					},
					{$subview: true, popup: true}
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.HeaderTable,
						this.TeamTable,
					]
				},
				{
					width: 384,
					rows: [
						this.UnitsDetailView,
					]
				},
			]
		};
	}

	ready(_$view, _$url) {
		super.ready(_$view, _$url);
		setTimeout(function () {
			_$view.$scope.getData()
		}, 50)
	}

	getData() {
		let that = this;
		let team_header = this.HeaderTable.$$("header_data");
		let team_table = this.TeamTable.$$("team_data");
		webix.ajax().get(master_url + "api/crm/sale/daily_sale/").then(function (response) {
			const data = response.json();
			team_header.clearAll();
			team_table.clearAll();
			that.header = data.header;
			team_header.data_setter(that.header);
			team_table.data_setter(data.lines);
		});
	}
}
