import {JetView} from "webix-jet";

export default class ListAdviserView extends JetView {
	config() {
		const adviser_list = {
			view: "list",
			id: "adviser_data",
			css: "multi-line-box",
			select: true,
			type: {
				height: "auto",
				template: obj => {
					if (obj.total_adviser_sales > obj.total_adviser_metas) {
						obj.status = "QUE NO PARE";
						obj.color = "#2cbb10";
					} else {
						obj.status = "BAJO";
						obj.color = "#ff0000";
					}
					return `
						<span style="font-weight: 500;"><b>Vendedor: </b>${obj.adviser}</span>
						<br>
						<span style="font-weight: 500;"><b>Estatus: </b><span style="color: ${obj.color}">${obj.status}</span></span>
						<br>
						<span style="font-weight: 500;"><b>Venta: </b> ${obj.total_adviser_sales}</span>
						<br>
						<span style="font-weight: 500;"><b>Meta: </b> ${obj.total_adviser_metas}</span>
						<br>
						<span style="font-weight: 500;"><b>Forecast: </b> ${obj.total_adviser_forecasts}</span>
						<br>
						<span style="font-weight: 500;"><b>Tibios: </b> ${obj.total_adviser_warms}</span>`
				}
			},
			on: {
				onAfterSelect: (id) => {
					this.setParam("adviser_id", id, true);
					const team_id = this.getParam("id");
					this.show("/home/forecast.sale.adviser_view?id=" + team_id + "/forecast.sale.popups.details?adviser_id=" + id + "?id=" + team_id);
					this._parent.HeaderTable.$$("header_data").unselectAll();
				},
			}
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					adviser_list,
				]
			}
		};
	}
}