import {JetView} from "webix-jet";

export default class ListHeaderView extends JetView {
	config() {
		const header_list = {
			view: "list",
			id: "header_data",
			css: "multi-line-box",
			select: true,
			type: {
				height: "auto",
				template: obj => `
			<span style="font-weight: 500;"><b>Venta total: </b>${obj.header_forecasts}</span>
			<br>
			<span style="font-weight: 500;"><b>Meta total: </b> ${obj.header_metas}</span>
			<br>
			<span style="font-weight: 500;"><b>FC total: </b> ${obj.header_sales}</span>
			<br>
			<span style="font-weight: 500;"><b>Tibios: </b> ${obj.header_warms}</span>`
			},
			on: {
				onAfterSelect: () => {
					this.setParam("id", "0", true);
					if(this._parent.TeamTable) {
						this.show("/home/forecast.sale/forecast.sale.popups.details?id=" + 0);
						this._parent.TeamTable.$$("team_data").unselectAll();
					} else if (this._parent.AdviserTable) {
						this.show("/home/forecast.sale.adviser_view/forecast.sale.popups.details?id=" + 0);
						this._parent.AdviserTable.$$("adviser_data").unselectAll();
					}
				},
			}
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					header_list,
				]
			}
		};
	}
}