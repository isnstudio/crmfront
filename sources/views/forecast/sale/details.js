import {JetView} from "webix-jet";

export default class UnitsDetailView extends JetView {
	config() {
		const that = this;
		this.parent = !this.app.Mobile ? this._parent : this._parent._parent;
		this.adviser_id = this.getParam("adviser_id");
		const details = {
			type: "space",
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					{
						view: "template",
						localId: "detalle_header",
						template: "Detalle",
						type: "header",
					},
					{
						rows: [
							{
								localId: "chart",
								view: "chart",
								type: "donut",
								pieInnerText: "$#value#",
								label: "#stage#",
								color: "#color#",
								hidden: true,
								borderless: true,
								legend: {
									width: 100,
									align: "right",
									valign: "middle",
									template: "#text#"
								},
							},
							{
								view: "button",
								id: "adviser_button",
								label: "Vendedores",
								borderless: true,
								height: 40,
								hidden: true,
								click: function () {
									const id = that._parent.getParam("id");
									that.show("/home/forecast.sale.adviser_view?id=" + id);
								}
							},
							{
								view: "button",
								id: "category_button",
								label: "Categorias",
								borderless: true,
								height: 40,
								hidden: true,
								click: function () {
									const id = that._parent.getParam("id");
									that.show("/home/forecast.sale.category_view?id=" + id);
								}
							},
							{
								view: "button",
								id: "regresar",
								label: "Regresar",
								borderless: true,
								height: 40,
								hidden: true,
								click: function () {
									var url = window.location.href;
									if (url.includes('/forecast.sale_accumulated.adviser_view')) {
										that.show("/home/forecast.sale_accumulated");
									} else if(url.includes('/forecast.sale.adviser_view')) {
										that.show("/home/forecast.sale");
									} else if(url.includes('/forecast.sale_accumulated.category_view')){
										that.show("/home/forecast.sale_accumulated");
									} else if(that.show("/home/forecast.sale.category_view")) {
										that.show("/home/forecast.sale");
									}

								}
							},
							{
								view: "button",
								id: "refrescar",
								label: "Refrescar",
								borderless: true,
								height: 40,
								click: function () {
									that._parent.getData()
								}
							}
						]
					},
					{
						template: "Estatus presupuesto",
						align: "center",
						height: 40,
						id: "warning_sales_budget_estatus",
						borderlses: true,
						hidden: true,
					},
					{
						template: "No hay información para mostrar",
						height: 40,
						id: "empty_template",
						borderlses: true,
						hidden: true,
					},
					{
						view: "template",
						localId: "detalle_header_ventas",
						template: "Ventas",
						type: "header",
						hidden: true
					},
					{
						view: "gage",
						localId: "gage",
						value: 0,
						maxRange: 500000,
						minRange: 0,
						format: webix.i18n.priceFormat,
						hidden: true,
						color: "red"
					},
					{
						view: "template",
						id: "header_get_per_adviser_today",
						template: "Forecast tibios del día",
						type: "header",
						hidden: true
					},
					{
						view: "scrollview",
						scroll: "y",
						id: "warm_data_container",
						hidden: true,
						body: {
							rows: [
								{
									view: "datatable",
									id: "warm_data",
									localId: "warm_data",
									select: true,
									scroll: "xy",
									rowHeight: 40,
									columns: [
										{
											id: "cliente__display_webix",
											header: "Cliente",
											sort: "string",
											adjust: true,
											maxWidth: 125,
										},
										{
											id: "lead__display_webix",
											header: "Lead",
											sort: "string",
											adjust: true,
											maxWidth: 125,
										},
										{
											id: "amount",
											format: webix.i18n.priceFormat,
											fillspace: true,
											header: { text: "Monto.", css: "right" },
											sort: "int",
											width: 175,
											css: "right"
										}
									],
									on: {
										onItemClick: (id) => {
											let warm_item = $$("warm_data").getItem(id);
											this.forecast_id = warm_item.forecast
											$$("ver_detalle_forecast").show()
										}
									},
								},
								{
									view: "button",
									value: "Ver Detalle",
									id: "ver_detalle_forecast",
									height: 40,
									hidden: true,
									click: () => {
										this.show("/home/forecast.tibios.actions.details?id=" + that.forecast_id);
									}
								}
							]
						}
					}
				]
			}
		};

		return {
			rows: [
				details
			]
		};
	}

	urlChange() {
		this.id = this.getParam("id");
		if (!isNaN(this.id)) {
			if (this.getRoot()) {
				this.refreshDetails(this.id);
			}
		}

		const return_button = this.$$("regresar");
		if (this.parent.AdviserTable)
			return_button.show();
		if (this.parent.CategoryTable)
			return_button.show();
	}

	setCharts(ventas, forecast, tibios, metas) {
		const pie = this.$$("chart");
		const gage = this.$$("gage");
		const header_pie = this.$$("detalle_header_ventas");
		if (metas === 0) {
			if (ventas + forecast + tibios === 0) {
				pie.hide();
				this.$$("empty_template").show();
			}
			header_pie.hide();
			gage.hide();
		} else {
			this.$$("empty_template").hide();
			if (ventas + forecast + tibios !== 0)
				pie.show();
			gage.show();
			header_pie.show();
		}
		if (metas !== 0) {
			gage.setValue(ventas);
			gage._settings.maxRange = metas;
			let color = function (val) {
				let tercio = metas / 3;
				if (val < tercio) return "#F6AD7B";
				if (val < tercio * 2) return "#f8e894";
				return "#7FCD91";
			};
			gage._settings.color = color;
			gage.refresh();
			gage.show();
		} else {
			gage.hide();
		}
		if (ventas + forecast + tibios !== 0) {
			pie.clearAll();
			pie.add({"id": 1, "value": ventas, "color": "#7FCD91", "stage": "", text: "Ventas"});
			pie.add({"id": 2, "value": forecast, "color": "#76EAD7", "stage": "", text: "Forecast"});
			pie.add({"id": 3, "value": tibios, "color": "#F6AD7B", "stage": "", text: "Tibios"});
			header_pie.show();
			pie.show()
		} else {
			pie.hide();
			header_pie.hide();
		}
	}

	setCategoryCharts(ventas, metas) {
		const pie = this.$$("chart");
		const gage = this.$$("gage");
		const header_pie = this.$$("detalle_header_ventas");
		if (metas === 0) {
			if (ventas === 0) {
				pie.hide();
				this.$$("empty_template").show();
			}
			header_pie.hide();
			gage.hide();
		} else {
			this.$$("empty_template").hide();
			if (ventas !== 0)
				pie.show();
			gage.show();
			header_pie.show();
		}
		if (metas !== 0) {
			gage.setValue(ventas);
			gage._settings.maxRange = metas;
			let color = function (val) {
				let tercio = metas / 3;
				if (val < tercio) return "#F6AD7B";
				if (val < tercio * 2) return "#f8e894";
				return "#7FCD91";
			};
			gage._settings.color = color;
			gage.refresh();
			gage.show();
		} else {
			gage.hide();
		}
		if (ventas !== 0) {
			pie.clearAll();
			pie.add({ "id": 1, "value": ventas, "color": "#7FCD91", "stage": "", text: "Ventas" });
			header_pie.show();
			pie.show()
		} else {
			pie.hide();
			header_pie.hide();
		}
	}

	refreshDetails(id) {
		const adviser_button = this.$$("adviser_button");
		const category_button = this.$$("category_button");
		const return_button = this.$$("regresar");
		if (id === 0 || id === "0") {
			const detail = this.parent.header;
			if (detail) {
				if ("header_forecasts" in detail || "header_warms" in detail){
					this.setCharts(detail.header_sales, detail.header_forecasts, detail.header_warms, detail.header_metas);
				} else {
					this.setCategoryCharts(detail.header_sales, detail.header_metas);
				}
				adviser_button.hide();
				category_button.hide();
			}
		} else {
			if (this.parent.TeamTable) {
				const table_team = this.parent.TeamTable.$$("team_data");
				const detail = table_team.getItem(id);
				if (detail) {
					this.setCharts(detail.total_team_sales, detail.total_team_forecasts, detail.total_team_warms, detail.total_team_metas);
					adviser_button.show();
					category_button.show();
					return_button.hide();
				}
			} else if (this.parent.AdviserTable) {
				const table_adviser = this.parent.AdviserTable.$$("adviser_data");
				var detail = table_adviser.getItem(id);
				if(!detail)
					detail = table_adviser.getItem(this.getParam("adviser_id"));
				if (detail) {
					this.setCharts(detail.total_adviser_sales, detail.total_adviser_forecasts, detail.total_adviser_warms, detail.total_adviser_metas);
					adviser_button.hide();
					category_button.hide();
					return_button.show();
				}
			} else if (this.parent.CategoryTable) {
				const table_category = this.parent.CategoryTable.$$("category_data");
				var detail = table_category.getItem(id);
				if(!detail)
					detail = table_category.getItem(this.getParam("category_id"));
				if (detail) {
					this.setCategoryCharts(detail.total_categoria_sales, detail.total_categoria_metas);
					adviser_button.hide();
					category_button.hide();
					return_button.show();
				}
			}
		}
	}
}