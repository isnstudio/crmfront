import {JetView} from "webix-jet";

export default class AdviserTableView extends JetView {
	config() {
		function status_color(value, config) {
			if (value === "BAJO")
				return {"color": "#ff0000"};
			if (value === "QUE NO PARE")
				return {"color": "#2cbb10"};
		}

		const adviser_table = {
			view: "datatable",
			id: "adviser_data",
			rowHeight: 40,
			tooltip: true,
			select: true,
			columns: [
				{
					id: "status",
					header: "Estatus",
					adjust: true,
					template: function (obj) {
						if (obj.total_adviser_sales > obj.total_adviser_metas) {
							return "QUE NO PARE";
						}
						return "BAJO";
					},
					cssFormat: status_color
				},
				{
					id: "adviser",
					header: "Vendedor",
					adjust: true,
					hidden: false
				},
				{fillspace: true},
				{
					id: "total_adviser_sales",
					header: {text: "Venta", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "total_adviser_metas",
					header: {text: "Meta", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "total_adviser_forecasts",
					header: {text: "Forecast", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
				{
					id: "total_adviser_warms",
					header: {text: "Tibios", css: "right"},
					adjust: true,
					css: "right",
					format: webix.i18n.priceFormat
				},
			],
			on: {
				onAfterSelect: (id) => {
					let record = $$("adviser_data").getItem(id);
					this.setParam("adviser_id", record.adviser_id, true);
					this._parent.UnitsDetailView.refreshDetails(record.adviser_id);
					this._parent.HeaderTable.$$("header_data").unselectAll();
					let warm_data = $$("warm_data")

					var xhr = webix.ajax().sync().get(master_url + "api/crm/warm/get_per_adviser_today/?adviser_id=" + record.adviser_id);
					let adviser_today = JSON.parse(xhr.response);
					$$("ver_detalle_forecast").hide()
					warm_data.clearAll();
					warm_data.define({ "data": adviser_today })
					if (adviser_today.length){
						$$("warning_sales_budget_estatus").show()
						$$("header_get_per_adviser_today").show()
						$$("warm_data_container").show()
					}
					else {
						$$("warning_sales_budget_estatus").hide()
						$$("header_get_per_adviser_today").hide()
						$$("warm_data_container").hide()
					}
					warm_data.show()
					warm_data.refresh()
				},
			}
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					adviser_table,
				]
			}
		};
	}

	init() {
		const datatable = this.$$("adviser_data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}