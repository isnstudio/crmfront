import {JetView} from "webix-jet";

import AdviserTableView from "./adviser_table";
import HeaderTableView from "jet-views/forecast/sale/header_table";
import ListAdviserView from "./responsive/adviser_table";
import ListHeaderView from "./responsive/header_table";
import UnitsDetailView from "jet-views/forecast/sale/details";
import "webix/breadcrumb";

export default class forecastView extends JetView {
	config() {
		this.AdviserTable = new (this.app.Mobile ? ListAdviserView : AdviserTableView)(this.app);
		this.HeaderTable = new (this.app.Mobile ? ListHeaderView : HeaderTableView)(this.app);
		if(!this.app.Mobile)
			this.UnitsDetailView = new UnitsDetailView(this.app);

		this.team_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Reporte de venta diaria", link: "/home/forecast.sale"},
				{value: "Equipo de venta", link: "/home/forecast.sale.adviser_view?id=" + this.team_id},
			],
		};

		const search = {
			view: "toolbar", localId: "toolbar",
			paddingX: 10, height: 44, visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		if (this.app.Mobile)
			return {
				view: "accordion",
				rows: [
					search,
					{
						view: "accordionitem",
						header: "Totales",
						type: "header",
						height: 175,
						body: this.HeaderTable,
					},
					{
						view: "accordionitem",
						header: "Vendedores",
						type: "header",
						body: this.AdviserTable
					},
					{$subview: true, popup: true}
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.HeaderTable,
						this.AdviserTable,
					]
				},
				{
					width: 384,
					rows: [
						this.UnitsDetailView,
					]
				},
			]
		};
	}

	ready(_$view, _$url) {
		super.ready(_$view, _$url);
		setTimeout(function () {
			_$view.$scope.getData()
		}, 50)
	}

	getData() {
		let that = this;
		let team_header = this.HeaderTable.$$("header_data");
		let adviser_table = this.AdviserTable.$$("adviser_data");;
		webix.ajax().get(master_url + "api/crm/sale/daily_sale/?team_id=" + that.team_id + "&detail=1").then(function (response) {
			let data = response.json();
			team_header.clearAll();
			adviser_table.clearAll();
			that.header = data.header;
			team_header.data_setter(that.header);
			adviser_table.data_setter(data.lines);
		});

		let warning_sales_budget_estatus = $$("warning_sales_budget_estatus")
		webix.ajax().get(master_url + "api/crm/sale/warning_sales_budget_estatus/").then(function (response) {
			warning_sales_budget_estatus.show();
			warning_sales_budget_estatus.define("template", response.json().mensaje)
			warning_sales_budget_estatus.refresh()
		});
	}
}