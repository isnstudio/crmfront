import {JetView} from "webix-jet";
import "webix/breadcrumb";

export default class leadsView extends JetView {
	config() {
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Reporte de venta diaria v2"},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const list = [
			/*
			{
				label: "Ventas diarias",
				id: "1"
			},
			{
				label: "Ventas, acumulado del mes",
				id: "2"
			},
			{
				label: "Ventas diarias con metas",
				id: "3"
			},
			{
				label: "Ventas con metas, acumulado del mes",
				id: "4"
			},
			*/
			{
				label: "Forecast Tibios Semanal",
				id: "5"
			},
			{
				label: "Ventas por período",
				id: "6"
			}
		];

		this.pivot_config = {
			view: "pivot",
			chart: {
				type: "line",
				scale: "logarithmic",
			},
			structure: {
				rows: ["Sales team", "Adviser"],
				columns: ["Date"],
				values: [
					{ name: "Forecasts", operation: ["sum"], format: webix.i18n.priceFormat },
					{ name: "Warms", operation: ["sum"], format: webix.i18n.priceFormat },
				],
				filters: [
					{ name: "Date", type: "between", select: "true" },
					{ name: "Sales team" }
				]
			},
			datatable: {
				columnWidth: 200,
				css: "pivot",
			},
			url: master_url + "api/crm/forecast/sales_teams_current_week/",
		};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						{
							cols: [
								/*
								{
									view: "button",
									label: "Export to PDF",
									click: () =>{
										let childs = $$("view").getChildViews();
										webix.toPDF(childs[0].config.id, {autowidth: true, styles: true})
									}
								},
								*/
								{
									view: "button",
									label: "Export to Excel",
									click: () => {
										let childs = $$("view").getChildViews();
										webix.toExcel(childs[0].config.id, { styles: true, spans: true })
									}
								},
								{
									view: "button",
									label: "Export to CSV",
									click: () => {
										let childs = $$("view").getChildViews();
										webix.toCSV(childs[0].config.id)
									}
								},
								{
									view: "button",
									label: "Export to PNG",
									click: () => {
										let childs = $$("view").getChildViews();
										webix.toPNG(childs[0].config.id)
									}
								},
							],
						},
						{
							id: "view",
							cols:[
								this.pivot_config,
								{
									type: "clean",
									rows: [
									  { template: "Selecciona un reporte", type: "header" },
									  {
										view: "list",
										id: "structures",
										data: list,
										template: "#label#",
										width: 250,
										select: true,
										value: 1
									  },
									],
								  },
							]
						}
					]
				},
			]
		};
	}

	init() {
		this.$$("structures").select(5);
	}

	ready() {
		const that = this;
		$$("structures").attachEvent("onItemClick", function(id) {

			let url = "";
			let config = that.pivot_config;
			switch (id) {
				case "1":
					url = master_url + "test_api/";
					config.structure = {
						rows: ["Team", "Vendedor"],
						values: [
							{ name: "Venta", operation: ["sum"], format: webix.i18n.priceFormat},
						]
					};
					break;
				case "2":
					url = master_url + "test_api/?current_period_sale=1";
					config.structure = {
						rows: ["Team", "Vendedor"],
						values: [
							{ name: "Venta", operation: ["sum"], format: webix.i18n.priceFormat},
						]
					};
					break;
				case "3":
					url = master_url + "test_api_meta/";
					config.structure = {
						rows: ["Team", "Vendedor"],
						values: [
							{ name: "Venta", operation: ["sum"], format: webix.i18n.priceFormat},
							{ name: "Forecasts", operation: ["sum"], format: webix.i18n.priceFormat},
							{ name: "Meta", operation: ["sum"], format: webix.i18n.priceFormat},
							{ name: "Warms", operation: ["sum"], format: webix.i18n.priceFormat}
						]
					};
					break;
				case "4":
					url = master_url + "test_api_meta/?current_period_sale=1";
					config.structure = {
						rows: ["Team", "Vendedor"],
						values: [
							{ name: "Venta", operation: ["sum"], format: webix.i18n.priceFormat},
							{ name: "Forecasts", operation: ["sum"], format: webix.i18n.priceFormat},
							{ name: "Meta", operation: ["sum"], format: webix.i18n.priceFormat},
							{ name: "Warms", operation: ["sum"], format: webix.i18n.priceFormat}
						]
					};
					break;
				case "5":
					url = master_url + "api/crm/forecast/sales_teams_current_week/";
					config.structure = {
						rows: ["Sales team", "Adviser"],
						columns: ["Date"],
						values: [
							{ name: "Forecasts", operation: ["sum"], format: webix.i18n.priceFormat },
							{ name: "Warms", operation: ["sum"], format: webix.i18n.priceFormat },
						],
						filters: [
							{ name: "Date", type: "between", select: "true" },
							{ name: "Sales team" }
						]
					};
					break;
				case "6":
					url = master_url + "api/crm/sale/advisers_last_three_periods/";
					config.structure = {
						rows: ["Adviser", "Cliente"],
						columns: ["Periodo"],
						values: [
							{ name: "Amount", operation: ["sum"], format: webix.i18n.priceFormat },
						],
						filters: [
							{ name: "Adviser" },
							{ name: "Cliente" }
						]
					};
					break;
				default:
					break;
			}

			let childs = $$("view").getChildViews();
			$$("view").removeView(childs[0].config.id);
			config.url = url;
			$$("view").addView(config, 0);
		});
	}
}
