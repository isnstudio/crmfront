import {JetView, plugins} from "webix-jet";
import AuthService from "../services/auth";

export default class SidebarView extends JetView {
	config() {

		const user_service = this.app.getService("auth");

		const crm = {
			id: 1,
			value: "CRM",
			icon: "mdi mdi-account-circle",
			open: false,
			data: []
		};


		const forecast = {
			id: 2,
			value: "Forecast",
			icon: "mdi mdi-chart-pie",
			open: false,
			data: []
		};

		const business_intelligence = {
			id: 3,
			value: "Business Intelligence",
			icon: "mdi mdi-domain",
			open: false,
			data: []
		};

		const configuracion = {
			id: 4,
			value: "Configuración",
			icon: "mdi mdi-cogs",
			open: false,
			data: []
		};

		const menu_mapping = [
			{id: "crm.leads", value: "Leads"},
			{id: "crm.quotation", value: "Cotizaciones de venta"},
			{ id: "crm.tareas", value: "Panel de control de tareas" },
			{id: "forecast.sale", value: "Reporte de venta diaria"},
			{id: "forecast.sale_accumulated", value: "Reporte de venta diaria acumulado"},
			{id: "forecast.tibios", value: "Forecast Tibios"},
			{id: "business.salesfunnel", value: "Embudo de ventas"},
			{id: "business.pronostico", value: "Pronóstico"},
			{id: "business.tac_4", value: "Mantenimiento clientes"},
			{id: "business.xselling", value: "X-Selling"},
			{id: "configuration.holidays", value: "Días inhábiles"},
			{id: "configuration.budgets", value: "Presupuestos"},
			{id: "configuration.profile", value: "Perfil"},
			{id: "configuration.salesteam", value: "Salesteam"},
			{id: "inventarios.proxy_inventory", value: "Proxy Inventory"},
			{id: "login", value: "Cerrar sesión"}
		];

		const logout = {id: "login", value: "Cerrar sesión"};

		const sidebar_menu = {
			localId: "nav",
			view: "sidebar",
			css: "webix_dark",
			minWidth: 200,
			data: [],
			on: {
				onItemClick: function (id) {
					if (id === "login") {
						const auth = new AuthService();
						auth.logout();
					}
					if (isNaN(id) && this.$scope.app.Mobile) {
						this.hide();
					}
				}
			}
		};

		let menu_crm = false;
		let menu_forecast = false;
		let menu_business_inteligence = false;
		let menu_configuracion = false;

		for (let i = 0; i < menu_mapping.length; i++) {
			const menu_id = menu_mapping[i].id;
			const module = menu_id.split(".")[0];
			const submodule = menu_id.split(".")[1];
			if (module === "crm" && user_service.hasAccess(submodule)) {
				crm.data.push(menu_mapping[i]);
				menu_crm = true;
			}
			if (module === "forecast" && user_service.hasAccess(submodule)) {
				forecast.data.push(menu_mapping[i]);
				menu_forecast = true;
			}
			if (module === "business" && user_service.hasAccess(submodule)) {
				business_intelligence.data.push(menu_mapping[i]);
				menu_business_inteligence = true;
			}
			if (module === "configuration" && user_service.hasAccess(submodule)) {
				configuracion.data.push(menu_mapping[i]);
				menu_configuracion = true;
			}
		}
		if (menu_crm) {
			sidebar_menu.data.push(crm);
		}
		if (menu_forecast) {
			forecast.data.push({id: "forecast.sale_v2", value: "Reporte de venta diaria v2"});
			sidebar_menu.data.push(forecast);
		}
		if (menu_business_inteligence) {
			sidebar_menu.data.push(business_intelligence);
		}
		if (menu_configuracion) {
			sidebar_menu.data.push(configuracion);
		}
		sidebar_menu.data.push(logout);

		return sidebar_menu;
	}

	init() {
		this.use(plugins.Menu, {
			id: "nav",
			urls: {
				reports: "reports/reports.statistics"
			}
		});
	}
}
