import {JetView} from "webix-jet";
import TableView from "./table";

export default class manageSalesTeamView extends JetView {
	config() {
		this.Table = new TableView(this.app);

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				{
					view: "label",
					label: "Sales Teams",
					batch: "default",
					localId: "label"
				},
				{
					view: "icon",
					icon: "mdi mdi-plus",
					localId: "addButton",
					css: "right_icon",
					click: () => {
						this.Table.agregar_vendedor_form(this.Table)
					},
				}
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 10,
			rows: [
				{
					view: "button",
					value: "Mostrar todo",
					css: "webix_secondary",
					height: 40,
					id: "open_all",
					hidden: true,
					click: () => {
						this.Table.$$("treetable").openAll()
						$$("close_all").show()
						$$("open_all").hide()
					}
				},
				{
					view: "button",
					value: "Retraer todo",
					css: "webix_secondary",
					height: 40,
					id: "close_all",
					click: () => {
						this.Table.$$("treetable").closeAll()
						$$("open_all").show()
						$$("close_all").hide()
					}
				}
			]
		};

		if (this.app.Mobile) return {
			type: "space",
			cols: [
				{
					rows: [
						{
							view: "template",
							template: "Detalles",
							type: "header"
						},
						buttons,
						search,
						this.Table,
					]
				}
			]
		};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.Table,
					]
				},
				{
					padding: 0,
					width: 384,
					rows: [
						{
							view: "template",
							template: "Detalles",
							type: "header"
						},
						buttons,
						{}
					]
				},
			]
		};
	}

	create() {
		const data = { "data": $$("treetable").data.serialize() }
		webix.ajax().headers({ "Content-type": "application/json" }).put(
			master_url + `api/crm/sales_team/treetable_guardar/`, data).then(function (res) {
				webix.message("Salesteam actualizado");
			}).fail(function (res) {
				const error = new Errors();
				error.show_error(res);
			});
	}

	urlChange() {
		const table = this.Table.$$("treetable");
		webix.ajax().get(master_url + "api/crm/sales_team/treetable/").then(function (response) {
			table.parse(response.json())
			table.openAll()
		});

	}
}
