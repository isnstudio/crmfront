import { JetView } from "webix-jet";
import Tables from "../../../helpers/tables";
import "webix/server_autocomplete";
import "webix/choice_field";

export default class TableSalesTeamView extends JetView {
	config() {
		this.Tables = new Tables(this.app);
		let that = this;
		const datatable = {
			view: "treetable",
			id: "treetable",
			select: true,
			type: {
				folder: function (obj) {
					if (obj.$count)
						return "<span class='mdi mdi-account-multiple-outline'></span>";
					return "<span class='mdi mdi-account'></span>";
				}
			},
			columns: [
				{
					id: "id",
					header: "",
					minWidth: 10,
					template: "{common.treetable()}"
				},
				{
					id: "value",
					minWidth: 200,
					header: "Nombre",
					fillspace: true
				},
				{
					id: "acciones",
					header: { text: "Acciones", css: "right" },
					minWidth: 400,
					css: "right",
					adjust: true,
					template: function (obj) {
						let action_buttons = "";
						if (obj.content_type == "profile"){
							action_buttons = `
								<div class='webix_el_button'>
									<button class='webix_danger eliminar_vendedor'>Quitar</button>
								</div>
							`
						}
						return action_buttons
					}
				}
			],
			onClick: {
				editar_salesteam: function (ev, id, html) {
					setTimeout(function () {
						that.editar_salesteam_form(that)
					}, 100)
				},
				eliminar_salesteam: function (ev, id, html) {
					that.removeItemById(id.row)
				},
				agregar_vendedor: function (ev, id, html) {
					that.agregar_vendedor_form(that)
				},
				editar_vendedor: function (ev, id, html) {
					setTimeout(function () {
						that.editar_vendedor_form(that)
					}, 100)
				},
				eliminar_vendedor: function (ev, id, html) {
					that.removeItemById(id.row)
				}
			},
			on: {
				onAfterClose: function (context, native_event) {
					$$("open_all").show()
					$$("close_all").hide()
				},
				onAfterOpen: function (context, native_event) {
					$$("close_all").show()
					$$("open_all").hide()
				}
			}
		};

		return {
			rows: [
				{
					view: "scrollview",
					scroll: "y",
					body: {
						rows: [
							datatable,
						]
					}
				}
			],
		};
	}

	agregar_vendedor_form(that) {
		if ($$("modal_editar_salesteam"))
			$$("modal_editar_salesteam").close();
		if ($$("modal_agregar_vendedor"))
			$$("modal_agregar_vendedor").close();
		if ($$("modal_editar_vendedor"))
			$$("modal_editar_vendedor").close();

		let data = $$("treetable").data.serialize();
		let salesteam_options = []

		data.forEach(function (item) {
			salesteam_options.push({ "id": item.id, "value": item.value })
			item.data.forEach(function (item_salesteam) {
				if (item_salesteam.content_type == "salesteam"){
					salesteam_options.push({ "id": item_salesteam.id, "value": item_salesteam.value })
				}
			})
		})

		const salesteam = {
			view: "choice_field",
			label: "Sales Team",
			id: "salesteam",
			name: "salesteam",
			required: true,
			options: salesteam_options
		};

		const vendedor = {
			view: "server_autocomplete",
			name: "item_id",
			id: "vendedor_input",
			label: "Vendedor",
			labelPosition: "top",
			placeholder: "Vendedor",
			url: master_url + "api/cat/profile/treetable/",
			required: true,
			windowForm: true
		};

		const standard_view = {
			rows: [
				{
					cols: [
						salesteam,
						vendedor
					],
				},
				{
					cols: [
						{
							view: "button",
							value: "Guardar",
							css: "webix_primary",
							click: function () {
								if ($$("form_vendedor").validate()) {
									let get_values = $$("form_vendedor").getValues()
									let tree_data = $$("treetable").data.serialize();
									if (!that.findProfileById(tree_data, parseInt(get_values.item_id))){
										that.addItem(that, get_values)
									}
									else {
										that.validateMessage()
									}

								}
							}
						},
						{
							view: "button",
							value: "Guardar y agregar otra",
							css: "webix_primary",
							click: function () {
								if ($$("form_vendedor").validate()) {
									let get_values = $$("form_vendedor").getValues()
									let tree_data = $$("treetable").data.serialize();
									if (!that.findProfileById(tree_data, parseInt(get_values.item_id))) {
										that.addItem(that, get_values)
										that.agregar_vendedor_form(that)
										setTimeout(function () {
											$$("salesteam").setValue(get_values["salesteam"])
											get_values["salesteam"]
										}, 50)
									}
									else {
										that.validateMessage()
									}
								}
							}
						},
					]
				},
			]
		};

		var parentId = $$("treetable").getSelectedId();
		var parentSection = isParentSection(parentId);

		function isParentSection(parentId) {
			if (parentId && "nombre" in $$("treetable").getItem(parentId.id)) {
				return true;
			}
		}

		webix.ui({
			view: "window",
			id: "modal_agregar_vendedor",
			height: 400,
			width: 1200,
			position: "center",
			close: true,
			head: "Agregar vendedor",
			body: {
				borderless: true,
				rows: [
					{
						view: "scrollview",
						scroll: "y",
						body: {
							rows: [
								{ height: 30 },
								{
									borderless: true,
									view: "form",
									id: "form_vendedor",
									rows: [
										standard_view
									]
								}
							]
						}
					}
				]
			}
		}).show();
		that.attachEvents();
	}

	removeItemById(id) {
		let item = $$("treetable").getItem(id)
		let parent_id = $$("treetable").getParentId(item.id)
		let parent = $$("treetable").getItem(parent_id)
		webix.ajax().put(master_url + "api/crm/sales_team/" + parent.obj_id + "/eliminar_vendedor/", { profile: item.obj_id}).then(function (res) {
			$$("treetable").remove(id);
			webix.ajax().get(master_url + "api/cat/profile/treetable/").then(function (response) {
				that.vendedor_display = response.json();
			});
		});
	}

	addItem(that, values){
		let profile_id = values["item_id"]
		let table = $$("treetable");
		let parent = table.getItem(values.salesteam)
		values["obj_id"] = profile_id
		values["content_type"] = "profile"
		webix.ajax().put(master_url + "api/crm/sales_team/" + parent.obj_id + "/colocar_vendedor/", { profile: profile_id }).then(function (response) {
			webix.ajax().get(master_url + "api/crm/sales_team/treetable/").then(function (response) {
				table.parse(response.json())
				table.openAll()
			});
		});
		$$("modal_agregar_vendedor").close();
	}

	validateMessage() {
		webix.message({
			text: "El vendedor ya se encuentra en un Sales Team",
			type: "error",
			expire: -1,
		});
	}

	findProfileById(data, id){
		let that = this;
		for (let item of data) {
			if (item.content_type === 'profile' && parseInt(item.obj_id) === id) {
				return true;
			}
			if (item.data && that.findProfileById(item.data, parseInt(id))) {
				return true;
			}
		}
		return false;
	}

	attachEvents() {
		let treetable = $$("treetable");
		let item = treetable.getSelectedItem()
	}

	ready(_$view, _$url) {
		super.ready(_$view, _$url);
		const that = this;
		webix.ajax().get(master_url + "api/cat/profile/treetable/").then(function (response) {
			that.vendedor_display = response.json();
		});
	}
}