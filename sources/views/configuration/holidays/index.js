import {JetView} from "webix-jet";

import TableView from "./table";
import ListView from "./responsive/table";
import UnitsDetailView from "./details";

export default class forecastView extends JetView {
	config() {
		this.Table = new (this.app.Mobile ? ListView : TableView)(this.app);
		if (!this.app.Mobile)
			this.UnitsDetailView = new UnitsDetailView(this.app);

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				{
					view: "label",
					label: "Días inhábiles",
					batch: "default",
					localId: "label"
				},
				{
					view: "icon",
					icon: "mdi mdi-plus",
					localId: "addButton",
					css: "right_icon",
					click: () => {
						this.show("../configuration.holidays.actions.add");
					},
				}
			]
		};

		if (this.app.Mobile) return {
			rows: [
				search,
				this.Table,
				{$subview: true, popup: true}
			]
		};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.Table,
					]
				},
				{
					padding: 0,
					width: 384,
					rows: [
						{
							view: "template",
							template: "Detalles",
							type: "header"
						},
						this.UnitsDetailView,
					]
				},
			]
		};
	}

	urlChange() {
		this.id_holiday = this.getParam("id");
		this.$$("toolbar").showBatch("default");
		this.$$("addButton").show();

		this.$$("toolbar").showBatch("default");
		this.$$("addButton").show();

		// Data nav
		const table = this.Table.$$("data");
		let start = this.getParam("start");
		let count = this.getParam("count");
		let page = this.getParam("page");

		// Data loading
		const data_url = new URL(master_url + "api/crm/holiday/wlist/");

		if (start) {
			data_url.searchParams.append("start", start);
		} else {
			data_url.searchParams.append("start", 0);
		}
		if (count) {
			data_url.searchParams.append("count", count);
		} else {
			data_url.searchParams.append("count", 20);
		}

		webix.ajax(data_url.href).then(function (data) {
			let json_data = data.json();
			table.parse(json_data);
			let pager = table.getPager();
			if (pager) {
				pager.disabled();
				if (page) {
					pager.select(page);
				} else {
					pager.select(0);
				}
			}
		}).finally(function (){
			let pager = table.getPager();
			if (pager) pager.enable();
		});
	}
}
