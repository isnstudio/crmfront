import {JetView} from "webix-jet";
import PagerView from "../../../common/pager";

export default class ListView extends JetView {
	config() {
		return {
			type: "space",
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					{
						view: "list",
						localId: "data",
						pager: "pager",
						css: "multi-line-box",
						select: true,
						url: master_url + "api/crm/holiday/wlist/",
						type: {
							height: "auto",
							template: obj => `
                                    <b style="float:right">${obj.id}</b>
                                    <b>${obj.name}</b>
									<br>
									<span style="font-weight: 500;"><b>Fecha: </b> ${obj.date}</span>`
						},
						on: {
							onItemClick: (id) => {
								this.show("/home/configuration.holidays/configuration.holidays.popups.details?id=" + id);
							}
						}
					},
					PagerView
				]
			}
		};
	}
}