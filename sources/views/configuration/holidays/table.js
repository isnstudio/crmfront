import {JetView} from "webix-jet";
import PagerView from "../../common/pager";

export default class TableView extends JetView {
	config() {
		const that = this;
		return {
			rows: [
				{
					view: "datatable",
					localId: "data",
					css: "units_table",
					pager: "pager",
					url: master_url + "api/crm/holiday/wlist/",
					select: true,
					columns: [
						{
							id: "name",
							header: "Descripción",
							fillspace: true,
						},
						{
							id: "date",
							header: {text: "Fecha", css: "right"},
							css: "right",
						}
					],
					on: {
						onAfterSelect: (id) => {
							this.setParam("id", id, true);
							this._parent.UnitsDetailView.showDetail(id);
						},
						onAfterRender() {
							const that = this;
							if (!that.count()) {
								that.showOverlay("<p>No hay información para mostrar, verifique los filtros</p>");
							} else {
								that.hideOverlay();
							}
							setTimeout(function () {
								that.enable();
							}, 1000);
						},
					}
				},
				PagerView
			]
		};

	}

	init() {
		this.webix.extend(this.$$("data"), webix.OverlayBox);
		this.webix.extend(this.$$("data"), webix.ProgressBar);
	}

	urlChange(ui, url) {
		const month = this.getParam("month");
		const table = this.$$("data");

		const flush = this.getParam("flush");
		const _cache = `${month}`;

		this._cache = _cache;
		if (flush) {
			this.setParam("flush", false);
			table.clearAll();
			table.load(function () {
				return webix.ajax().get(master_url + "api/crm/holiday/wlist/", {});
			});
		}
	}
}