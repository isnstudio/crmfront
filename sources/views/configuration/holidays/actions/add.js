import {JetView, plugins} from "webix-jet";
import SaveService from "../../../../services/save";

export default class PersonView extends JetView {
	config() {
		const that = this;
		const dateFormat = webix.Date.dateToStr("%Y-%m-%d");
		const select_text = "Clic para seleccionar";
		const required_text = "Requerido";

		const description = {
			view: "text",
			name: "name",
			label: "Descripción",
			labelPosition: "top",
			placeholder: "Descripción",
			invalidMessage: required_text,
			required: true,
		};
		const date = {
			view: "datepicker",
			name: "date",
			label: "Fecha",
			labelPosition: "top",
			placeholder: select_text,
			invalidMessage: required_text,
			format: dateFormat,
			required: true,
		};

		const left_side = {
			margin: 10,
			rows: [
				description,
			]
		};
		const right_side = {
			margin: 10,
			rows: [
				date,
			]
		};

		const responsive_view = {
			margin: 10,
			rows: [
				description,
				date,
			]
		};

		const standard_view = {
			margin: 48,
			cols: [
				left_side,
				right_side
			]
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					height: 40,
					css: "webix_primary",
					id: "save",
					click: function() {
						var button = this;
						that.create(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/configuration.holidays?flush=true");
					}
				},
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [
				main_section,
			],
			rules: {
				name: webix.rules.isNotEmpty,
				date: webix.rules.isNotEmpty,
			}
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								{template: "Día inhábil", type: "header"},
								buttons,
								form,
							]
						}
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							{template: "Día inhábil", type: "header"},
							form,
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons
						]
					}
				]
			}
		};
	}

	init() {
		this.use(plugins.UrlParam, ["id"]);
	}

	create(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/holiday/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Dia inhábil guardado",
			path: "/home/configuration.holidays/",
			button: button,
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}