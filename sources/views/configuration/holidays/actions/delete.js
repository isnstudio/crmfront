import {JetView} from "webix-jet";
import UnitsDetailView from "jet-views/configuration/holidays/details";
import RemoveService from "../../../../services/remove";


export default class DeleteHolidayView extends JetView {
	config() {
		const that = this;
		this.UnitsDetailView = new UnitsDetailView(this.app);

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [
				{
					template: "¿Estás seguro de eliminar este dia inhabil?",
					type: "label",
					borderless: true,
					css: "question"
				},
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Eliminar",
					css: "webix_danger",
					height: 40,
					id: "remove",
					click: () => {
						var button = this.$$("remove");
						that.remove(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/configuration.holidays?id=" + this.lead_id + "&flush=true");
					}
				},
			]
		};


		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								{template: "Holiday - Eliminar", type: "header"},
								buttons,
								this.UnitsDetailView,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							{template: "Holiday - Eliminar", type: "header"},
							form,
							{
								localId: "spacer",
								css: "template-scroll-y",
							}
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons,
							this.UnitsDetailView,
						]
					}
				]
			}
		};
	}

	urlChange() {
		this.holiday_id = this.getParam("id");
		this.UnitsDetailView.showDetail(this.holiday_id, true);
	}

	remove(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/holiday/" + that.holiday_id + "/",
			validate: that.$$("form").validate(),
			message: "Días inhábil eliminado",
			path: "/home/configuration.holidays",
			button: button,
			context: that,
		};
		this.RemoveService = new RemoveService(this.app);
		this.RemoveService.remove(request_options);
	}
}