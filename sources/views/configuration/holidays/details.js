import {JetView} from "webix-jet";

export default class UnitsDetailView extends JetView {
	config() {
		this.holiday_id = this.getParam("id");
		const details = {
			rows: [
				{
					view: "property",
					id: "detail_info",
					disabled: true,
					autoheight: true,
					elements: [
						{label: "Nombre", type: "text", id: "name"},
						{label: "Fecha", type: "text", id: "date"},
					]
				},
				{
					localId: "detail_components",
					css: "wbackground",
					padding: 20,
					rows: []
				},
				{}
			]
		};

		return {
			rows: [
				details
			]
		};
	}

	urlChange() {
		this.showDetail();
	}

	
	init() {
		this.hideContents();
	}

	hideContents() {
		this.$$("detail_components").hide();
		this.$$("detail_info").hide();
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		this.$$("detail_components").show();
		this.$$("detail_info").show();
		this.RefreshDetails(id);
	}

	showDetail(id, hide_buttons) {
		if (id && id.constructor === Object && "id" in id) {
			id = id["id"];
		}
		if(hide_buttons) 
			this.hide_buttons = hide_buttons;
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				child_ids.push(child_id);
			}
			for (let childIdsKey in child_ids) {
				content.removeView(child_ids[childIdsKey]);
			}
		}
		this.holiday_id = id;
		if(!this.holiday_id)
			this.holiday_id = this.getParam("id");
		if (!isNaN(id) && id !== "") {
			if (this.getRoot()) {
				this.showContents(id);
			}
		}
		if (!isNaN(this.holiday_id) && this.holiday_id !== "" && this.app.Mobile) {
			if (this.getRoot()) {
				this.showContents(this.holiday_id);
			}
		}
	}




	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(master_url + "api/crm/holiday/" + id + "/wdetails/");
			const json_response = JSON.parse(xhr.response);
			if ("actions" in json_response && "id" in json_response) {
				const content = this.$$("detail_components");
				if (content) {
					for (const action_key in json_response["actions"]) {
						const action = json_response["actions"][action_key];
						if(!this.hide_buttons) {
							content.addView({
								view: "button",
								label: action["label"],
								height: 40,
								click: () => {
									this.show("/home/configuration.holidays.actions." + action["action"] + "?id=" + json_response["id"]);
								}
							}, 0);
						}
					}
				}
			}
			
			if ("id" in json_response) {
				const sets = this.$$("detail_info");
				if (sets) {
					sets.setValues(json_response);
				}
			}
		}
	}
}

