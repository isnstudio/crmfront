import {JetView} from "webix-jet";

export default class profileView extends JetView {
	config() {
		var xhr = webix.ajax().sync().get(master_url + "api/cat/profile/data/");
		this.profile = JSON.parse(xhr.response);
        
		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 24,
			height: 44,
			visibleBatch: "default",
			cols: [
				{
					view: "label",
					label: "Perfil",
					batch: "default",
					localId: "label",
				}
			],
		};

		const that = this;
		return {
			type: "space",
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					search,
					{
						view: "form",
						id: "profile_form",
						elements: [
							{
								height: 300,
								id: "foto_perfil",
								template: function() {
									if(that.profile.foto_perfil) {
										return "<p style='text-align: center;'><img src="+ master_url +  that.profile.foto_perfil + " width='200' height='200''></img></p>";
									} else {
										return "<p style='text-align: center;'><img src="+ master_url +  "/static/img/avatars/generic.png" + " width='200' height='200''></img></p>";
									}   
								},
							},
							{view: "text", label: "Nombre", value: that.profile.nombre, name: "nombre"},
							{view: "text", label: "Correo", value: that.profile.correo_electronico, name: "correo_electronico"},
							{view: "text", label: "Equipo", value: that.profile.sales_team__name, name: "sales_team"},
							{
								cols: [
									{view:"label", label:"Google calendar", name:"google_calendar"},
									{
										view: "button",
										value: "Vincular cuenta",
										css: "webix_primary",
										name:"google_calendar_button",
										hidden: false,
										height: 40,
										click: () => {
											var xhr = webix.ajax().sync().get(master_url + "api/cat/user/request_google_credentials/");
											const google_calendar = JSON.parse(xhr.response);
											window.open(google_calendar.data, "_blank");
										}
									},
									{
										view: "label",
										label: "Su cuenta se encuentra vinculada a Google calendar",
										name:"google_calendar_message",
										hidden: true
									}
								]
							},
						]
					}
				]
			}
		};
	}

	ready() {
		const form = this.$$("profile_form");
		for (var element in form.elements) {
			form.elements[element].disable();
		}
        
		form.elements["google_calendar_button"].enable();
		if(this.profile.valid_google_token) {
			form.elements["google_calendar_message"].show();
			form.elements["google_calendar_button"].hide();
		} else {
			form.elements["google_calendar_message"].hide();
			form.elements["google_calendar_button"].show();
		}
	}
}