import {JetView} from "webix-jet";
import Download_csv from "../../../helpers/download_csv";

export default class UnitsDetailView extends JetView {
	config() {
		let that = this;
		const details = {
			id: "details",
			rows: [
				{
					view: "property",
					id: "detail_info",
					disabled: true,
					autoheight: true,
					elements: [
						{label: "Información", type: "label"},
						{label: "Folio", type: "text", id: "id", localId: "folio_id"},
						{label: "Ejercicio", type: "text", id: "accounting_period__display_webix"},
						{ label: "Estatus", type: "text", id: "get_estatus_display" },
						{label: "Monto total", type: "text", id: "amount", format: webix.i18n.priceFormat},
					]
				},
				{
					localId: "detail_components",
					id: "detail_components",
					css: "wbackground",
					padding: 20,
					rows: []
				},

			]
		};

		return {
			rows: [
				{
					view: "toolbar",
					localId: "detalle_header",
					paddingX: 10,
					height: 44,
					cols: [
						{
							view: "label",
							label: "Detalle",
							localId: "label"
						},
					]
				},
				details,
				{
					view: "datatable",
					localId: "data",
					scroll: "y",
					rowHeight: 40,
					columns: [
						{id: "month", header: "Mes", adjust: true},
						{
							id: "amount",
							header: {text: "Monto", css: "right"},
							css: "right",
							fillspace: true,
							format: webix.i18n.priceFormat
						},
					]
				},
			]
		};
	}

	init() {
		this.hideContents();
	}

	hideContents() {
		this.$$("details").hide();
		this.$$("detalle_header").hide();
		this.$$("detail_components").hide();
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		this.$$("details").show();
		this.$$("detalle_header").show();
		this.$$("detail_components").show();
		this.RefreshDetails(id);
	}

	showDetail(id, hide_buttons) {
		if (id && id.constructor === Object && "id" in id) {
			id = id["id"];
		}
		this.budget_id = id;
		if (!this.budget_id)
			this.budget_id = this.getParam("id");

		if (hide_buttons)
			this.hide_buttons = hide_buttons;
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				child_ids.push(child_id);
			}
			for (let childIdsKey in child_ids) {
				content.removeView(child_ids[childIdsKey]);
			}
		}
		if (!isNaN(id) && id !== "") {
			if (this.getRoot()) {
				this.showContents(id);
			}
		}
		if (!isNaN(this.budget_id) && this.budget_id !== "" && this.app.Mobile) {
			if (this.getRoot()) {
				this.showContents(this.budget_id);
			}
		}
	}

	urlChange() {
		this.showDetail();
	}

	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(master_url + "api/crm/sales_budget/" + id + "/wdetails/");
			const json_response = JSON.parse(xhr.response);
			if(!this.hide_buttons) {
				if ("actions" in json_response && "id" in json_response) {
					const content = this.$$("detail_components");
					if (content) {
						for (const action_key in json_response["actions"]) {
							const action = json_response["actions"][action_key];
							content.addView({
								view: "button",
								label: action["label"],
								height: 40,
								click: () => {
									if (action.action === "dowload_csv_categorias") {
										const url = "api/crm/sales_budget/" + id + "/download_csv_categorias/";
										const download_csv = new Download_csv();
										download_csv.download_csv(url);
									}
									else if (action.action === "validar_presupuesto") {
										webix.ajax().put(master_url + "api/crm/sales_budget/" + id + "/validar_presupuesto/").then(function (response) {
											webix.message({
												text: response.json().mensaje,
												expire: -1,
												id: "validar_presupuesto"
											});
										});
									}
									else {
										this.show("/home/configuration.budgets.actions." + action["action"] + "?id=" + json_response["id"]);
									}
								}
							}, 0);
						}
					}
				}
			}

			const table = this.$$("data");
			this.json_response = json_response;

			if ("id" in json_response) {
				const sets = this.$$("detail_info");
				if (sets) {
					sets.setValues(this.json_response);
				}
				table.clearAll();
				table.load(function () {
					return webix.ajax().get(master_url + "api/crm/sales_budget/" + id + "/wdetails_breakdown/", {});
				});
			}
		}
	}
}

