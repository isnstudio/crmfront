import {JetView} from "webix-jet";
import PagerView from "../../../common/pager";

export default class ListView extends JetView {
	config() {
		return {
			type: "space",
			view: "scrollview",
			scroll: "y",
			body: {
				rows: [
					{
						view: "list",
						localId: "data",
						pager: "pager",
						css: "multi-line-box",
						select: true,
						url: master_url + "api/crm/sales_budget/wlist/",
						type: {
							height: "auto",
							template: obj => `
                                    <span style="float:right; text-transform: uppercase"><b>${obj.accounting_period__display_webix}</b></span>
                                    <span style="font-weight: 500;"><b>Fecha de creación: </b>${obj.created_by}</span>
                                    <br>
                                    <span style="font-weight: 500;"><b>Ejercicio: </b> ${obj.created_on}</span>
                                    <br>
                                    <span style="font-weight: 500;"><b>Monto total: </b> ${obj.amount}</span>
                                    <br>`
						},
						on: {
							onItemClick: (id) => {
								this.show("/home/configuration.budgets/configuration.budgets.popups.details?id=" + id);
							}
						}
					},
					PagerView
				]
			}
		};
	}
}