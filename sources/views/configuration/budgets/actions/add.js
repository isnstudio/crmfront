import {JetView} from "webix-jet";
import SaveService from "../../../../services/save";

export default class BudgetAddView extends JetView {
	config() {
		const that = this;
		that.input_accounting_period;
		const accounting_period = {
			view: "combo",
			name: "accounting_period",
			id: "input_accounting_period",
			label: "Ejercicio",
			labelPosition: "top",
			placeholder: "Clic para seleccionar",
			invalidMessage: "Requerido",
			suggest: {url: master_url + "api/cnt/ejercicio_fiscal/select_list/"},
			required: true,
		};

		const left_side = {
			margin: 10,
			rows: [accounting_period],
		};

		const responsive_view = {
			margin: 10,
			rows: [accounting_period],
		};
		const standard_view = {
			margin: 48,
			cols: [left_side],
		};

		const main_section = this.app.Mobile ? responsive_view : standard_view;

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Continuar",
					id: "continuar",
					height: 40,
					css: "webix_primary",
					hidden: true,
					click: () => {
						that.createSalesBudget()
					},
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/configuration.budgets");
					},
				},
			]
		};

		const form = {
			view: "form",
			id: "form",
			padding: 24,
			rows: [
				main_section,
			],
			rules: {
				accounting_period: webix.rules.isNotEmpty,
			},
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								{template: "Agregar - Presupuesto", type: "header"},
								buttons,
								form,
							],
						},
					],
				},
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							{template: "Agregar - Presupuesto", type: "header"},
							form,
						],
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384,
							},
							buttons,
						],
					},
				],
			},
		};
	}

	ready() {
		this.$$("input_accounting_period").attachEvent("onChange", function (value) {
			$$("continuar").show();
		})
	}

	createSalesBudget(){
		let that = this;
		const request_options = {
			url: master_url + "api/crm/sales_budget/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Presupuesto creado",
			path: "/home/configuration.budgets.actions.edit?id=",
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}
