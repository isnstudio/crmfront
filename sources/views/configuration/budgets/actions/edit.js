import {JetView} from "webix-jet";
import Errors from "../../../../helpers/errors";
import SaveService from "../../../../services/save";
import Download_csv from "../../../../helpers/download_csv";

export default class PersonView extends JetView {
	config() {
		this.budget_id = this.getParam("id");
		let that = this;
		const accounting_period = {
			view: "combo",
			name: "accounting_period",
			id: "input_accounting_period",
			label: "Ejercicio",
			labelPosition: "top",
			placeholder: "Clic para seleccionar",
			invalidMessage: "Requerido",
			suggest: {url: master_url + "api/cnt/ejercicio_fiscal/select_list/"},
			required: true,
			disabled: true
		};
		const folio = {
			view: "text",
			name: "id",
			label: "Folio",
			labelPosition: "top",
			disabled: true
		};
		const amount = {
			view: "text",
			name: "amount",
			label: "Monto total",
			labelPosition: "top",
			format: "$1,111.00",
			disabled: true
		};

		const left_side = {
			margin: 10,
			rows: [
				folio,
				amount,
			],
		};

		const right_side = {
			margin: 10,
			rows: [
				accounting_period,
			],
		};

		const responsive_view = {
			margin: 10,
			rows: [
				folio,
				accounting_period,
				amount
			],
		};
		const standard_view = {
			margin: 10,
			id: "form",
			cols: [
				left_side,
				right_side
			],
		};

		const main_section = this.app.Mobile ? responsive_view : standard_view;

		const download_csv_button_profiles = {
			rows: [
				{
					height: 40,
					view: "button",
					value: "Descargar lista de vendedores",
					click: function () {
						const url = "api/cat/profile/download_csv/";
						const download_csv = new Download_csv();
						download_csv.download_csv(url);
					}
				}
			]
		};

		const download_csv_button_categories = {
			rows: [
				{
					height: 40,
					view: "button",
					value: "Descargar lista de categorías",
					click: function () {
						const url = "api/crm/sales_budget/" + that.budget_id + "/download_csv_categorias/";
						const download_csv = new Download_csv();
						download_csv.download_csv(url);
					}
				}
			]
		};

		const validar_presupuesto_button = {
			rows: [
				{
					height: 40,
					view: "button",
					value: "Validar presupuesto",
					click: function () {
						webix.ajax().put(master_url + "api/crm/sales_budget/" + that.budget_id + "/validar_presupuesto/").then(function (response) {
							let type_message = "error"
							if (response.json().valido){
								type_message = "success"
							}
							webix.message({
								text: response.json().mensaje,
								type: type_message,
								expire: -1,
								id: "messageLead"
							});


						});
					}
				}
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/configuration.budgets?flush=true");
					},
				},
				download_csv_button_profiles,
				download_csv_button_categories,
				validar_presupuesto_button
			]
		};


		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [
				main_section
			],
			rules: {
				accounting_period: webix.rules.isNotEmpty,
			},
		};

		const header = {template: "Editar - Presupuesto", type: "header"};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								header,
								buttons,
								form,
							],
						},
					],
				},
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							header,
							form,
						],
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384,
							},
							buttons,
						],
					},
				],
			},
		};
	}

	ready() {
		const that = this;
		const form = this.$$("form");
		const xhr = webix.ajax().sync().get(master_url + "api/crm/sales_budget/" + this.budget_id );
		let json_response = JSON.parse(xhr.response);
		form.setValues(json_response);
		if(json_response.accounting_period) {
			const xhr = webix.ajax().sync().get(master_url + "api/cnt/ejercicio_fiscal/"+ json_response.accounting_period +"/periodos_fiscales/");
			let periodos_fiscales = JSON.parse(xhr.response);
			if($$("table_budgets"))
				that.$$("form").removeView("table_budgets")

			let columns_adviser = []
			let columns_category = []

			columns_adviser.push({ id: "vendedor", header: "Vendedor", tooltip: "Nombre del usuario", editor: "text" });
			columns_category.push({ id: "categoria", header: "Categoría", tooltip: "Nombre de la categoría", editor: "text" });
			columns_category.push({ id: "sales_team", header: "Equipo", tooltip: "Salesteam", editor: "text" });

			const num_cols_adviser = periodos_fiscales.length + columns_adviser.length;
			const num_cols_category = periodos_fiscales.length + columns_category.length;

			periodos_fiscales.forEach(function (v) {
				columns_category.push({
					id: v.nombre,
					id_month: v.id,
					header: v.nombre,
					tooltip: v.nombre,
					minWidth: v.nombre.length * 11,
					editor: "text",
					fillspace: 2
				})
				columns_adviser.push({
					id: v.nombre,
					id_month: v.id,
					header: v.nombre,
					tooltip: v.nombre,
					minWidth: v.nombre.length * 11,
					editor: "text",
					fillspace: 2
				})
			});
			const table_categorias = {
				view: "datatable",
				id: "table_categories",
				css: "units_table",
				tooltip: true,
				select: "cell",
				blockselect: true,
				clipboard: "block",
				editable: true,
				editaction: "dblclick",
				columns: columns_category,
				data: [{
					categoria: "",
					sales_team: "",
				}],
				on: {
					onPaste: function (text) {
						var parsedData = webix.csv.parse(text);
						var rows = parsedData.length;
						var table = $$("table_categories")
						var indexes = table.getSelectedId(true).map(a => table.data.getIndexById(a)).sort();
						var diff = indexes[0] + rows - table.data.count();

						for (; diff > 0; diff--) {
							table.add({});
						}

						parsedData.forEach((row, rowIndex) => {
							var item = table.getItem(table.getIdByIndex(indexes[0] + rowIndex));
							table.eachColumn((columnId) => {
								item[columnId] = row.shift() || "";
							});
							table.updateItem(item.id, item);
						});
					},
					onEditorChange: (id, value) => {
						const month = $$("table_budgets").getColumnConfig(id.column);
						let item = $$("table_categories").getItem(id.row)

						const params = {
							"sales_budget_id": parseInt(this.budget_id),
							"categoria_id": item.categoria_id,
							"sales_team_id": item.sales_team_id,
							"month": month.id_month,
							"amount": value
						};
						webix.ajax().headers({
							"Content-type": "application/json"
						}).put(master_url + "api/crm/sales_budget_categorizacion_articulo_detail/update_amount/", params).then(function (res) {
							webix.message({
								type: "success",
								text: "Mes Editado"
							});
						}).fail(function (res) {
							const error = new Errors();
							error.show_error(res);
						});
					}
				}
			}
			const table_vendedores = {
				view: "datatable",
				id: "table_budgets",
				css: "units_table",
				tooltip: true,
				select: "cell",
				blockselect: true,
				clipboard: "block",
				editable: true,
				editaction: "dblclick",
				columns: columns_adviser,
				data: [],
				autoConfig: true,
				on: {
					onPaste: function (text) {
						var parsedData = webix.csv.parse(text);
						var rows = parsedData.length;
						var table = $$("table_budgets")
						var indexes = table.getSelectedId(true).map(a => table.data.getIndexById(a)).sort();
						var diff = indexes[0] + rows - table.data.count();

						for (; diff > 0; diff--) {
							table.add({});
						}

						parsedData.forEach((row, rowIndex) => {
							var item = table.getItem(table.getIdByIndex(indexes[0] + rowIndex));
							table.eachColumn((columnId) => {
								item[columnId] = row.shift() || "";
							});
							table.updateItem(item.id, item);
						});
					},
					onEditorChange: (id, value) => {
						const month = $$("table_budgets").getColumnConfig(id.column);
						const params = {
							"sales_budget_id": parseInt(this.budget_id),
							"user_id": id.row,
							"month": month.id_month,
							"amount": value
						};
						webix.ajax().headers({
							"Content-type": "application/json"
						}).put(master_url + "api/crm/sales_budget_detail_month/update_amount/", params).then(function (res) {
							webix.message({
								type: "success",
								text: "Mes Editado"
							});
						}).fail(function (res) {
							const error = new Errors();
							error.show_error(res);
						});
					}
				}
			}
			const tabs = {
				view: "tabview",
				id: "tabs",
				cells: [
					{
						header: "Categorías",
						body: {
							id: "view_categories",
							rows: [
								{
									id: "save_categories",
									view: "button",
									css: "webix_primary",
									value: "Guardar presupuesto por categoría",
									height: 40,
									click: () => {
										that.create_categories(this)
									},
								},
								table_categorias
							]
						},
						id: "tareas_tab",
					},
					{
						header: "Vendedores",
						body: {
							id: "view_budgets",
							rows: [
								{
									id: "save_advisers",
									view: "button",
									css: "webix_primary",
									value: "Guardar presupuesto por vendedor",
									height: 40,
									click: () => {
										that.create_budgets(this)
									},
								},
								table_vendedores
							]
						}
					},
				]
			};
			that.$$("form").addView(tabs)
			const table_categories = this.$$("table_categories");
			const table_budgets = this.$$("table_budgets");
			table_categories.clearAll();
			table_budgets.clearAll();

			let category_url = "wdetails_breakdown_per_category"
			let adviser_url = "wdetails_breakdown_per_user"

			webix.ajax().get(master_url + "api/crm/sales_budget/" + that.budget_id + "/" + category_url).then(function (response) {
				let data = response.json()
				if (data.length){
					table_categories.define({ "data": data });
				} else {
					table_categories.define({ "data": {sales_team: ""} });
				}
			});
			webix.ajax().get(master_url + "api/crm/sales_budget/" + that.budget_id + "/" + adviser_url).then(function (response) {
				let data = response.json()
				if (data.length) {
					table_budgets.define({ "data": data });
				} else {
					table_budgets.define({ "data": { vendedor: "" } });
				}
			});
		}
	}

	create_categories(button) {
		const that = this;
		const table_categories_data = that.$$("table_categories").data.serialize();
		const get_values = this.$$("form").getValues();
		get_values["table_data"] = table_categories_data;

		const request_options = {
			url: master_url + "api/crm/sales_budget_categorizacion_articulo_detail/process_batch/",
			get_values: get_values,
			validate: that.$$("form").validate(),
			message: "Presupuesto por categoría guardado",
			// path: "/home/configuration.budgets.actions.edit?id=",
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}

	create_budgets(button) {
		const that = this;
		const table_budgets_data = that.$$("table_budgets").data.serialize();
		const get_values = this.$$("form").getValues();
		get_values["table_data"] = table_budgets_data;

		const request_options = {
			url: master_url + "api/crm/sales_budget/process_batch/",
			get_values: get_values,
			validate: that.$$("form").validate(),
			message: "Presupuesto por vendedor guardado",
			// path: "/home/configuration.budgets.actions.edit?id=",
			context: that,
			method: "post"
		};

		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}