import {JetView} from "webix-jet";
import UnitsDetailView from "jet-views/configuration/budgets/details";
import RemoveService from "../../../../services/remove";

export default class BudgetsDeleteView extends JetView {
	config() {
		const that = this;
		this.UnitsDetailView = new UnitsDetailView(this.app);
		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Eliminar",
					height: 40,
					css: "webix_danger",
					id: "remove",
					click: () => {
						var button = this.$$("remove");
						that.remove(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/configuration.budgets");
					}
				},
			]
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							{template: "Presupuesto - Eliminar", type: "header"},
							{template: "¿Estás seguro de eliminar el presupuesto?", height: 80},
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons,
							this.UnitsDetailView,
						]
					}
				]
			}
		};
	}
	urlChange() {
		this.budget_id = "";
		if ("id" in this._data) {
			this.budget_id = this._data.id;
			this.UnitsDetailView.showDetail(this.budget_id, true);
		}
	}

	remove(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/sales_budget/" + this.budget_id + "/",
			validate: true,
			message: "Presupuesto eliminado",
			path: "/home/configuration.budgets",
			button: button,
			context: that,
		};
		this.RemoveService = new RemoveService(this.app);
		this.RemoveService.remove(request_options);
	}
}