import {JetView} from "webix-jet";


export default class ValidarPresupuestoView extends JetView {
	config() {
		const that = this;
		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Validar",
					height: 40,
					css: "webix_primary",
					id: "validar",
					click: () => {
						var button = this.$$("validar");
						that.validar(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/configuration.budgets");
					}
				},
			]
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							{template: "Presupuesto - Validar", type: "header"},
							{template: "", height: 80},
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons
						]
					}
				]
			}
		};
	}

	urlChange() {
		this.budget_id = "";
		if ("id" in this._data) {
			this.budget_id = this._data.id;
		}
	}

	validar(button) {
		const that = this;
		webix.ajax().put(master_url + "api/crm/sales_budget/" + this.budget_id + "/validar_presupuesto/").then(function (response) {
			webix.message({
				text: response.json().mensaje,
				expire: -1,
				id: "validar_presupuesto"
			});
		});
	}
}