import {JetView} from "webix-jet";
import Download_csv from "../../../../helpers/download_csv";

export default class PersonView extends JetView {
	config() {
		this.budget_id = this.getParam("id");
		let that = this;
		const accounting_period = {
			view: "text",
			name: "accounting_period__display_webix",
			label: "Ejercicio",
			labelPosition: "top",
			disabled: true
		};
		const folio = {
			view: "text",
			name: "id",
			label: "Folio",
			labelPosition: "top",
			disabled: true
		};
		const amount = {
			view: "text",
			name: "amount",
			label: "Monto total",
			labelPosition: "top",
			format: "$1,111.00",
			disabled: true
		};

		const left_side = {
			margin: 10,
			rows: [
				folio,
				amount,
			],
		};

		const right_side = {
			margin: 10,
			rows: [
				accounting_period,
			],
		};

		const responsive_view = {
			margin: 10,
			rows: [
				folio,
				accounting_period,
				amount
			],
		};
		const standard_view = {
			margin: 10,
			cols: [
				left_side,
				right_side
			],
		};

		const main_section = this.app.Mobile ? responsive_view : standard_view;

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/configuration.budgets?flush=true");
					},
				},
				{
					height: 40,
					view: "button",
					value: "Descargar lista de vendedores",
					click: function () {
						const url = "api/cat/profile/download_csv/";
						const download_csv = new Download_csv();
						download_csv.download_csv(url);
					}
				},
				{
					height: 40,
					view: "button",
					value: "Descargar lista de categorías",
					click: function () {
						const url = "api/crm/sales_budget/" + that.budget_id + "/download_csv_categorias/";
						const download_csv = new Download_csv();
						download_csv.download_csv(url);
					}
				}
			]
		};


		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [
				main_section,
			],
			rules: {
				accounting_period: webix.rules.isNotEmpty,
			},
		};

		const header = {template: "Detalle - Presupuesto", type: "header"};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								header,
								buttons,
								form,
							],
						},
					],
				},
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							header,
							form,
						],
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384,
							},
							buttons,
						],
					},
				],
			},
		};
	}

	urlChange() {
		const budget_id = this.getParam("id");
		this.budget_id = budget_id;
	}

	ready() {
		const that = this;
		const form = this.$$("form");
		const xhr = webix.ajax().sync().get(master_url + "api/crm/sales_budget/" + this.budget_id );
		let json_response = JSON.parse(xhr.response);
		form.setValues(json_response);
		if(json_response.accounting_period) {
			const xhr = webix.ajax().sync().get(master_url + "api/cnt/ejercicio_fiscal/"+ json_response.accounting_period +"/periodos_fiscales/");
			let periodos_fiscales = JSON.parse(xhr.response);
			if($$("table_budgets"))
				that.$$("form").removeView("table_budgets")
			let columns_adviser = []
			let columns_category = []

			columns_adviser.push({id: "vendedor", header: "Vendedor", tooltip: "Nombre del usuario", editor: "text"});
			columns_category.push({ id: "categoria", header: "Categoría", tooltip: "Nombre de la categoría", editor: "text" });
			columns_category.push({ id: "sales_team", header: "Salesteam", tooltip: "Salesteam", editor: "text" });

			periodos_fiscales.forEach(function (v) {
				columns_adviser.push({
					id: v.nombre,
					header: v.nombre,
					tooltip: v.nombre,
					minWidth: v.nombre.length * 11,
					fillspace: 2,
					format: webix.i18n.priceFormat
				})
				columns_category.push({
					id: v.nombre,
					header: v.nombre,
					tooltip: v.nombre,
					minWidth: v.nombre.length * 11,
					fillspace: 2,
					format: webix.i18n.priceFormat
				})
			});

			const table_categorias = {
				view: "datatable",
				id: "table_categories",
				css: "units_table",
				tooltip: true,
				select: "cell",
				blockselect: true,
				clipboard: "block",
				editaction: "dblclick",
				columns: columns_category,
				data: [{
					categoria: "",
					salesteam: "",
				}],
			}
			const table_vendedores = {
				view: "datatable",
				id: "table_budgets",
				css: "units_table",
				tooltip: true,
				select: "cell",
				blockselect: true,
				clipboard: "block",
				editaction: "dblclick",
				columns: columns_adviser,
				data: [{
					adviser: "",
				}],
			};
			const tabs  = {
				view: "tabview",
				id: "tabs",
				cells: [
					{
						header: "Categorias",
						body: {
							id: "view_categories",
							rows: [
								table_categorias
							]
						},
						id: "tareas_tab",
					},
					{
						header: "Vendedores",
						body: {
							id: "view_budgets",
							rows: [
								table_vendedores
							]
						}
					},
				]
			};

			that.$$("form").addView(tabs)
			const table_categories = this.$$("table_categories");
			const table_budgets = this.$$("table_budgets");
			table_categories.clearAll();
			table_budgets.clearAll();

			let category_url =  "wdetails_breakdown_per_category"
			let adviser_url = "wdetails_breakdown_per_user"
			webix.ajax().get(master_url + "api/crm/sales_budget/" + that.budget_id + "/" + category_url).then(function (response) {
				let data = response.json()
				table_categories.define({ "data": data });
			});

			table_budgets.load(function () {
				return webix.ajax().get(master_url + "api/crm/sales_budget/" + that.budget_id + "/" + adviser_url);
			});

		}
	}
}
