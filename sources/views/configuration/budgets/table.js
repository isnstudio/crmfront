import {JetView} from "webix-jet";
import PagerView from "../../common/pager";
import Tables from "../../../helpers/tables";

export default class TableView extends JetView {
	config() {
		this.Tables = new Tables(this.app);
		return {
			rows: [
				{
					view: "datatable",
					localId: "data",
					css: "units_table",
					select: true,
					pager: "pager",
					columns: [
						{
							id: "accounting_period__display_webix",
							header: "Ejercicio",
						},
						{
							id: "created_by",
							header: "Creado por",
							fillspace: true,
						},
						{
							id: "created_on",
							header: {text: "Fecha de creación", css: "right"},
							css: "right",
							fillspace: true,
						},
						{
							id: "amount",
							header: {text: "Monto total", css: "right"},
							css: "right",
							fillspace: true,
							format: webix.i18n.priceFormat
						}
					],
					on: {
						onAfterSelect: (id) => {
							this.setParam("id", id, true);
							this._parent.UnitsDetailView.showDetail(id);
						},
						onAfterRender() {
							this.$scope.Tables.afterRender(this, true);
						},
						onBeforeRender() {
							this.$scope.hideContents();
						},
					}
				},
				PagerView
			]
		};
	}

	init() {
		this.webix.extend(this.$$("data"), webix.OverlayBox);
		this.webix.extend(this.$$("data"), webix.ProgressBar);
	}

	urlChange() {
		this.hideContents();
	}

	hideContents() {
		const details = [
			this._parent.UnitsDetailView,
		];
		const datatable = this.$$("data");
		this.Tables.hideContents(this, datatable, details);
	}
}