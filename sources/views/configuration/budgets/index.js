import {JetView} from "webix-jet";

import TableView from "./table";
import ListView from "./responsive/table";
import UnitsDetailView from "./details";
import Tables from "../../../helpers/tables";

export default class forecastView extends JetView {
	config() {
		this.Tables =  new Tables(this.app);
		this.Table = new (this.app.Mobile ? ListView : TableView)(this.app);
		this.UnitsDetailView = new UnitsDetailView(this.app);
		const search = {
			view: "toolbar", localId: "toolbar",
			paddingX: 10, height: 44, visibleBatch: "default",
			cols: [
				{
					view: "label",
					label: "Presupuestos",
					batch: "default",
					localId: "label"
				},
				{
					view: "icon",
					icon: "mdi mdi-plus",
					localId: "addButton",
					css: "right_icon",
					click: () => {
						this.show("../configuration.budgets.actions.add");
					},
				},
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					this.Table,
					{$subview: true, popup: true}
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.Table,
					]
				},
				{
					width: 384,
					rows: [
						this.UnitsDetailView
					]
				},
			]
		};
	}

	ready(view) {
		const datagrid = view.$scope.Table.$$("data");
		this.Tables.pagerParams(view, datagrid);
	}

	urlChange() {
		const url = master_url + "api/crm/sales_budget/wlist/";
		const table = this.Table.$$("data");
		this.Tables.dataLoading(table, url, this);
	}
}
