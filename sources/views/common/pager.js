import {JetView} from "webix-jet";

export default class PagerView extends JetView {
	config() {
		return {
			view: "pager",
			id: "pager",
			size: 20,
			group: 5,
			count: 20,
			template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
		};
	}
}
