import {JetView} from "webix-jet";
import Sidebar from "./sidebar";
import AuthService from "../services/auth";

export default class HomeView extends JetView {
	config() {
		const xhr = webix.ajax().sync().get(master_url + "api/cat/profile/data/");
		const profile = JSON.parse(xhr.response);
		localStorage.setItem("sales_team__leaf_node", profile.sales_team__leaf_node);
		localStorage.setItem("user_id", profile.id);
		const header = {
			view: "toolbar",
			css: "main_header",
			cols: [
				{
					cols: [
						{view: "icon", icon: "mdi mdi-menu", click: () => this.ToggleSidebar()},
						{
							view: "button",
							type: "image",
							css: "header_logo",
							image: "data/images/logo.png",
							responsive: "hide",
							click: () => {
								this.show("/home/crm.leads");
							},
						}
					],
				},
				{},
				{
					width: 400,
					type: "header",
					popup: "menu",
					cols: [
						{
							template: function () {
								if (profile.foto_perfil) {
									return "<img class='mainphoto main_user_avatar' src=" + master_url + profile.foto_perfil + ">";
								} else {
									return "<img class='mainphoto main_user_avatar' src=" + master_url + "/static/img/avatars/generic.png" + ">";
								}
							},
							css: "main_user_avatar_template right",
							borderless: true,
							localId: "avatar",
						},
						{
							rows: [
								{
									view: "label",
									label: profile.label,
									css: "main_user_name_label",
									popup: "menu",
									height: 16
								},
								{
									view: "label",
									label: profile.sales_team__name,
									css: "main_user_name_label",
									popup: "menu",
									height: 16
								},
								{
									view: "label",
									height: 18,
									label: VERSION,
									popup: "menu",
									borderless: true,
									css: "main_user_name_label right",
								}
							]
						}
					]
				}
			]
		};

		const header_responsive = {
			view: "toolbar",
			css: "main_header",
			cols: [
				{
					cols: [
						{view: "icon", icon: "mdi mdi-menu", click: () => this.ToggleSidebar()},
						{
							view: "button",
							type: "image",
							css: "header_logo",
							image: "data/images/logo.png",
							responsive: "hide",
							click: () => {
								this.show("/home/crm.leads");
							},
						},
						{
							view: "label",
							height: 18,
							width: 100,
							label: VERSION,
							popup: "menu",
							borderless: true,
							css: "main_user_name_label right",
						}
					],
				},
			]
		};

		if (this.app.Mobile)
			return {rows: [header_responsive, {$subview: true}]};

		return {
			cols: [
				{
					rows: [
						header,
						{
							cols: [
								Sidebar,
								{
									$subview: true
								}
							]
						}
					]
				}
			]
		};
	}

	init() {
		if (!$$("list") || !$$("menu")) {
			webix.ui({
				view: "popup",
				id: "menu",
				width: 200,
				body: {
					view: "list",
					id: "list",
					data: [
						{id: "logout", name: "Cerrar sesión"},
						{id: "profile", name: "Perfil",}
					],
					template: "#name#",
					autoheight: true,
					on: {
						onItemClick: (id) => {
							if (id === "logout") {
								const auth = new AuthService();
								auth.logout();
								$$("list").hide();
							}
							if (id === "profile") {
								window.location.replace("/#!/home/configuration.profile");
								$$("list").hide();
							}
						}
					}
				}
			});
		}

		if(!$$("webix:debugmenu").data.order.includes("add_hint"))
			$$("webix:debugmenu").data.add({
				id: "add_hint",
				value: "Add Hint"
			});

		if(!$$("webix:debugmenu").data.order.includes("start_hints"))
			$$("webix:debugmenu").data.add({
				id: "start_hints",
				value: "Start Hints"
			});

		$$("webix:debugmenu").attachEvent("OnItemClick", function (id, ev){
			//mixing two object result in confusion
			var obj = $$(this.config.lastTarget);

			if (id === "add_hint") {
				let view_hash = ev.view.location.hash.replace("#!", "");
				let url = MASTER_URL + "admin/cat/hint/add/?el=" + this.config.lastTarget + "&view=" + view_hash;
				window.open(url);
			}
			if (id === "start_hints") {
				let view_hash = ev.view.location.hash.replace("#!", "");
				let url = MASTER_URL + "api/cat/hint/?view=" + view_hash;


				webix.ajax().get(url).then(function(data){
					let data_steps = data.json();
					let hint = $$("hint");
					if (hint){
						hint.destructor();
					}
					webix.ui({
						view: "hint",
						id: "hint",
						steps: data_steps
					});

					$$("hint").start();
				});
			}
		});

		if (this.app.Mobile) {
			this.Sidemenu = this.ui({
				view: "sidemenu", position: "left",
				width: 220,
				body: Sidebar
			});

			this.on(this.app, "SidebarNavigation", () => {
				if (this.Sidemenu.isVisible())
					this.Sidemenu.hide();
			});
		}
	}

	ToggleSidebar() {
		if (this.app.Mobile)
			if (this.Sidemenu.isVisible())
				this.Sidemenu.hide();
			else
				this.Sidemenu.show();
		else
			this.getRoot().queryView({view: "sidebar"}).toggle();
	}

	urlChange() {
		if (!localStorage.getItem("Token")) {
			const auth = new AuthService();
			auth.logout();
		}
	}

	ready() {
		const that = this;
		webix.attachEvent("onBeforeAjax", function (mode, url, params, xhr, headers, files, defer) {
			if(that.app) {
				if(that.getSubView()) {
					const top = that.getSubView().getRoot().getTopParentView();
					that.webix.extend(top, webix.OverlayBox);
					that.webix.extend(top, webix.ProgressBar);
					if (!that.app.Mobile){
						top.showProgress();
					}
					if(defer) {
						defer.then(function(){
							setTimeout(function (){
								top.hideProgress();
							}, 200);
						}, function(){
							setTimeout(function (){
								top.hideProgress();
							}, 200);
						});
					}
					else setTimeout(function (){
						top.hideProgress();
					}, 200);
				}
			}
		});
	}
}
