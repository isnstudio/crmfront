import {JetView,} from "webix-jet";
import "webix/server_autocomplete";
import "webix/datepicker_iso";

export default class FormFieldsView extends JetView {
	config() {
		const select_text = "Clic para seleccionar";
		const required_text = "Requerido";
		const phone_mask = {mask: "###-###-####", allow: /[0-9]/g};

		const cotizacion = {
			template: "Detalle de cotización", type: "section"
		};

		const es_prospecto = {
			view: "checkbox",
			name: "es_prospecto",
			id: "es_prospecto",
			label: "¿Es prospecto nuevo?",
			labelPosition: "top",
			placeholder: select_text,
			invalidMessage: required_text,
		};

		const es_proyecto = {
			view: "checkbox",
			name: "es_proyecto",
			id: "es_proyecto",
			label: "¿Es proyecto nuevo?",
			labelPosition: "top",
			placeholder: select_text,
			invalidMessage: required_text,
			hidden: true
		};

		const lead = {
			view: "server_autocomplete",
			name: "lead",
			label: "Lead",
			id: "lead",
			labelPosition: "top",
			placeholder: "Lead",
			invalidMessage: required_text,
			url: master_url + "api/crm/lead/select_list/?available_for_quote=1",
			hidden: true
		};

		const correo = {
			view: "text",
			id: "correo",
			name: "correo",
			label: "Correo",
			labelPosition: "top",
			placeholder: "Correo",
			hidden: true
		};

		const direccion = {
			view: "text",
			id: "direccion",
			name: "direccion",
			label: "Dirección",
			labelPosition: "top",
			placeholder: "Dirección",
			hidden: true,
		};

		const telefono = {
			view: "text",
			id: "telefono",
			name: "telefono",
			label: "Teléfono",
			labelPosition: "top",
			placeholder: "Teléfono",
			pattern: phone_mask,
			hidden: true,
		};

		const cliente = {
			view: "server_autocomplete",
			id: "cliente",
			name: "cliente",
			label: "Cliente",
			placeholder: "Cliente",
			url: master_url + "api/cxc/cliente/select_list/?status_str=['Activo']",
			required: true
		};

		const atencion = {
			view: "text",
			name: "atencion",
			label: "Atención a",
			id: "atencion",
			labelPosition: "top",
			placeholder: "Atención a",
			hidden: true,
			required: true,
			invalidMessage: required_text,
		};

		const persona = {
			view: "server_autocomplete",
			name: "persona",
			label: "Atención a",
			id: "persona",
			labelPosition: "top",
			placeholder: "Atención a",
			invalidMessage: required_text,
			required: true,
			url: master_url + "api/rel/persona/select_list/"
		};

		const tipo_venta = {
			view: "combo",
			name: "tipo_venta",
			id: "tipo_venta",
			label: "Tipo de pago",
			labelPosition: "top",
			placeholder: "Tipo de pago",
			invalidMessage: required_text,
			suggest: {url: master_url + "api/vnt/cotizacion-venta/choice_field/tipo_venta/"},
		};

		const plazo = {
			view: "text",
			name: "plazo",
			id: "plazo",
			label: "Plazo",
			labelPosition: "top",
			placeholder: "Plazo",
			invalidMessage: required_text,
		};

		const condicion_pago = {
			view: "text",
			name: "condicion_pago",
			label: "Condición de pago",
			labelPosition: "top",
			placeholder: "Condición de pago",
			invalidMessage: required_text,
		};

		const forma_pago = {
			view: "server_autocomplete",
			id: "forma_pago_obj",
			name: "forma_pago_obj",
			label: "Forma de pago",
			placeholder: "Forma de pago",
			labelPosition: "top",
			url: master_url + "api/cat/metodo-pago/select_list/",
		};

		const entrega = {
			view: "combo",
			id: "entrega",
			name: "entrega",
			label: "Entrega",
			labelPosition: "top",
			placeholder: "Entrega",
			suggest: {url: master_url + "api/vnt/cotizacion-venta/choice_field/entrega/"},
		};

		const entrega_direccion_cliente = {
			view: "server_autocomplete",
			id: "entrega_direccion_cliente",
			name: "entrega_direccion_cliente",
			label: "Dirección de entrega",
			labelPosition: "top",
			url: master_url + "api/cxc/cliente-direcciones/select_list/?es_entrega=1",
			hidden: true
		};

		const fecha = {
			view: "datepicker_iso",
			id: "fecha",
			name: "fecha",
			label: "Fecha",
			labelPosition: "top",
		};

		const vigencia = {
			view: "datepicker_iso",
			id: "vigencia",
			name: "fecha_vigencia",
			label: "Fecha Vigencia",
			labelPosition: "top",
		};

		const tiempo_entrega = {
			view: "text",
			type: "number",
			name: "tiempo_entrega",
			label: "Tiempo entrega (semanas)",
			placeholder: "Tiempo entrega (semanas)",
			labelPosition: "top",
		};

		const moneda = {
			view: "combo",
			name: "moneda",
			id: "moneda",
			label: "Moneda",
			labelPosition: "top",
			suggest: {url: master_url + "api/vnt/cotizacion-venta/choice_field/moneda/"},
		};

		const tipo_cambio = {
			view: "text",
			id: "tipo_cambio",
			name: "tipo_cambio",
			label: "Tipo de cambio",
			labelPosition: "top",
			required: true,
			disabled: true
		};

		const centro_beneficio = {
			view: "server_autocomplete",
			name: "centro_beneficio",
			label: "Centro de costo",
			id: "centro_beneficio",
			labelPosition: "top",
			url: master_url + "api/cnt/centro-beneficio/select_list/",
			required: true,
		};

		const embarcado = {
			view: "textarea",
			name: "lab",
			label: "Embarcado",
			labelPosition: "top",
			height: 60
		};

		const observaciones = {
			view: "textarea",
			name: "observaciones",
			label: "Observaciones",
			labelPosition: "top",
			height: 60
		};

		const orden_compra = {
			view: "text",
			id: "orden_compra",
			name: "orden_compra",
			label: "Orden de compra",
			labelPosition: "top",
		};

		const adviser = {
			view: "server_autocomplete",
			id: "vendedor",
			name: "vendedor",
			label: "Vendedor",
			placeholder: select_text,
			url: master_url + "api/cat/user/select_list/?activo=1&leaf_node=1",
			labelPosition: "top",
			required: true,
		};

		const proyecto = {
			view: "server_autocomplete",
			id: "proyecto",
			name: "proyecto_venta",
			label: "Proyecto",
			placeholder: select_text,
			url: master_url + "api/vnt/proyecto-venta/select_list/",
			labelPosition: "top",
		};

		const almacen = {
			view: "server_autocomplete",
			id: "almacen",
			name: "almacen",
			label: "Almacén",
			labelPosition: "top",
			url: master_url + "api/inv/almacen/select_list/",
			required: true,
		};

		const orden_venta = {
			template: "Orden de venta", type: "section", id: "orden_venta", hidden: true,
		};

		const direccion_entrega = {
			view: "server_autocomplete",
			id: "direccion_entrega",
			name: "orden_venta__entrega_direccion_cliente",
			label: "Dirección de entrega",
			labelPosition: "top",
			url: master_url + "api/cxc/cliente-direcciones/select_list/",
			hidden: true
		};

		const tipo_entrega = {
			view: "combo",
			id: "tipo_entrega",
			name: "orden_venta__tipo_entrega_material",
			label: "Tipo de entrega",
			labelPosition: "top",
			placeholder: "Tipo de entrega",
			suggest: {url: master_url + "api/vnt/orden-venta/choice_field/tipo_entrega_material/"},
			hidden: true,
		};

		const responsive_view = {
			margin: 10,
			rows: [
				orden_venta,
				direccion_entrega,
				tipo_entrega,
				cotizacion,
				es_prospecto,
				es_proyecto,
				cliente,
				lead,
				correo,
				direccion,
				telefono,
				persona,
				atencion,
				tipo_venta,
				plazo,
				condicion_pago,
				forma_pago,
				entrega,
				entrega_direccion_cliente,
				fecha,
				vigencia,
				tiempo_entrega,
				moneda,
				tipo_cambio,
				centro_beneficio,
				embarcado,
				observaciones,
				orden_compra,
				adviser,
				proyecto,
				almacen
			]
		};
		const standard_view = {
			margin: 10,
			rows: [
				orden_venta,
				direccion_entrega,
				{

					cols: [
						{
							margin: 10,
							rows: [
								tipo_entrega
							]
						},
						{}

					]
				},
				cotizacion,
				{
					cols: [
						{
							margin: 10,
							rows: [
								es_prospecto,
								lead,
								telefono,
								cliente,
								persona,
								atencion,
								tipo_venta,
								plazo,
								condicion_pago,
								forma_pago,
								entrega,
								entrega_direccion_cliente,
								fecha,
								vigencia,
							]
						},
						{
							margin: 10,
							rows: [
								es_proyecto,
								correo,
								direccion,
								tiempo_entrega,
								moneda,
								tipo_cambio,
								centro_beneficio,
								embarcado,
								observaciones,
								orden_compra,
								adviser,
								proyecto,
								almacen
							]
						}

					]
				},
			],

		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section
			]
		};
	}

	init() {
	}
}
