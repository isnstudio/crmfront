import {JetView} from "webix-jet";
import SaveService from "../../../../services/save";
import "webix/server_autocomplete";
import "webix/breadcrumb";

export default class FormsView extends JetView {
	config() {
		this.quotation_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.quotation_id + " - Autorizar",
					link: "/home/crm.quotation.actions.authorize?id=" + this.quotation_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const motivo = {
			view: "text",
			name: "autorizada_margen_bajo_motivo",
			id: "autorizada_margen_bajo_motivo",
			label: "Motivo",
			labelPosition: "top",
			placeholder: "Motivo",
			required: true,
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Estás seguro de autorizar la cotización?",
					type: "label",
					borderless: true,
					css: "question"
				},
				motivo
			],
			rules: {
				autorizada_margen_bajo_motivo: webix.rules.isNotEmpty,
			}
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					css: "webix_primary",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation?id=" + this.quotation_id);
					}
				},
			]
		};

		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								form,
								{
									view: "template",
									template: "Acciones",
									type: "header",
								},
								buttons,
							]
						},
					]
				}
			};
		}


		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
						]
					}
				]
			}
		};
	}

	action(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/vnt/cotizacion-venta/" + this.quotation_id + "/autorizar_margen_bajo/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Cotización autorizada",
			path: "/home/crm.quotation?id=" + that.quotation_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}