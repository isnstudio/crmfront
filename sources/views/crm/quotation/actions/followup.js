import {JetView} from "webix-jet";
import Errors from "../../../../helpers/errors";
import TaskDetailView from "./task";
import "webix/server_autocomplete";
import "webix/datepicker_iso";
import "webix/breadcrumb";
import SaveService from "../../../../services/save";


export default class TimeLineView extends JetView {
	config() {
		const dateFormat = webix.Date.dateToStr("%Y-%m-%d %H:%i");
		const select_text = "Clic para seleccionar";
		const required_text = "Requerido";
		this.TaskDetailView = new TaskDetailView(this.app);
		this.quotation_id = this.getParam("id");
		this.folio = this.getParam("folio");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.folio + " - Seguimiento",
					link: "/home/crm.quotation.actions.followup?id=" + this.quotation_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const activity = {
			id: "activity",
			name: "activity",
			view: "server_autocomplete",
			label: "Actividad",
			url: master_url + "api/crm/activity/select_list/",
			required: true
		};

		const start_date = {
			view: "datepicker_iso",
			name: "start_date",
			id: "start_date",
			label: "Fecha y hora (Inicio)",
			labelPosition: "top",
			placeholder: select_text,
			timepicker: true,
			required: true,
			format: dateFormat
		};

		const date = {
			view: "datepicker_iso",
			name: "date",
			id: "date",
			label: "Fecha y hora (Fin)",
			labelPosition: "top",
			placeholder: select_text,
			timepicker: true,
			required: true,
			format: dateFormat
		};

		const reminder_time = {
			view: "combo",
			name: "reminder_time",
			localId: "reminder_time:combo",
			id: "reminder_time",
			label: "Notificación",
			labelPosition: "top",
			placeholder: select_text,
			invalidMessage: required_text,
			suggest: {url: master_url + "api/crm/task/choice_field/reminder_time/"},
			required: true,
		};

		const responsable = {
			id: "responsable",
			name: "responsable",
			view: "server_autocomplete",
			label: "Responsable (En caso de que no sea el Encargado de la Actividad)",
			url: master_url + "api/crm/user/select_list/?activo=1",
		};

		const description = {
			view: "textarea",
			name: "description",
			id: "description",
			label: "Comentarios",
			labelPosition: "top",
			placeholder: "Comentarios",
			invalidMessage: required_text,
			height: 75
		};

		const left_side = {
			margin: 10,
			rows: [
				activity,
				start_date,
				reminder_time,
			]
		};
		const right_side = {
			margin: 10,
			rows: [
				{},
				date,
				responsable,
			]
		};

		const standard_view = {
			margin: 10,
			cols: [
				left_side,
				right_side
			]
		};

		const responsive_view = {
			margin: 10,
			rows: [
				activity,
				date,
				reminder_time,
				responsable,
			]
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);

		const buttons = {
			autoheight: true,
			css: "wbackground",
			id: "buttons",
			padding: 20,
			rows: [
				{
					view: "button",
					id: "save",
					value: "Guardar",
					height: 40,
					css: "webix_primary",
					click: () => {
						const button = this.$$("save");
						this.create(button);
					},
				},
				{
					view: "button",
					value: "Regresar",
					id: "cancel",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation?id=" + this.quotation_id);
					}
				},
			]
		};

		const list_historial = {
			view: "scrollview",
			id: "history",
			scroll: "y",
			padding: 20,
			autoheight: true,
			body: {
				rows: [
					{
						view: "datatable",
						localId: "data_historial",
						autoheight: true,
						scroll: "xy",
						rowHeight: 40,
						columns: [
							{id: "log_type__display_webix", header: "Tipo", adjust: true},
							{id: "user__display_webix", header: "Usuario", adjust: true},
							{id: "description", header: "Comentarios", adjust: true},
							{fillspace: true},
							{
								id: "created_on",
								header: {text: "Fecha de finalización", css: "right"},
								css: "right",
								adjust: true,
								minWidth: 180
							},
						],
						on: {
							onAfterRender() {
								const that = this;
								if (that.count() === 0) {
									that.showOverlay("<p>No hay información para mostrar</p>");
								} else {
									that.hideOverlay();
								}
							},
						}
					}
				]
			}
		};

		const list = {
			view: "scrollview",
			scroll: "y",
			padding: 20,
			body: {
				rows: [
					{
						view: "datatable",
						localId: "data",
						scroll: "xy",
						rowHeight: 40,
						autoheight: true,
						select: true,
						columns: [
							{id: "activity", header: "Actividad", adjust: true},
							{id: "description", header: "Comentarios", adjust: true},
							{fillspace: true},
							{ id: "date", header: { text: "Fecha límite", css: "right" }, css: "right", adjust: true, minWidth: 150 },
							{ id: "created_on", header: { text: "Fecha creación", css: "right" }, css: "right", adjust: true, minWidth: 150 },
						],
						on: {
							onAfterSelect: (id) => {
								if (this.app.Mobile) {
									this.show("/home/crm.quotation.actions.followup?id=" + this.quotation_id + "/crm.quotation.actions.popup_activities?task_id=" + id +
										"?id=" + this.quotation_id);
								} else this.setParam("task_id", id, true);
							},
							onAfterRender() {
								const that = this;
								if (!that.count()) {
									that.showOverlay("<p>No hay información para mostrar</p>");
								} else {
									that.hideOverlay();
								}
							},
						}
					}
				]
			}
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			scroll: "y",
			rows: [
				main_section,
				description,
			]
		};

		const tabs = {
			view: "tabview",
			localId: "tabs",
			autoheight: true,
			cells: [
				{
					header: "Tareas",
					body: {
						rows: [
							form,
							list
						]
					},
					id: "tareas_tab",
				},
				{
					header: "Historial",
					body: list_historial
				},
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						tabs,
						{$subview: true, popup: true}
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							tabs,
						]
					},
					{
						localId: "right_space",
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
							this.TaskDetailView,
						]
					}
				]
			}
		};
	}

	init() {
		this.webix.extend(this.$$("data"), webix.OverlayBox);
		this.webix.extend(this.$$("data_historial"), webix.OverlayBox);

		const save_btn = this.$$("save");
		const that = this;
		this.$$("history").attachEvent("onViewShow", function () {
			save_btn.hide();
			if(!that.app.Mobile)
				that.TaskDetailView.$$("detail_view").hide();
		});

		this.$$("form").attachEvent("onViewShow", function () {
			save_btn.show();
			if(!that.app.Mobile)
				that.TaskDetailView.$$("detail_view").show();
		});
	}

	ready(ui, url) {
		const that = this;
		const table = this.$$("data");
		const table_historial = this.$$("data_historial");
		let id = null;
		if (url.length === 1) {
			if ("task" in url[0].params) {
				id = url[0].params["task"];
				if (table.exists(id)) {
					table.select(id);
					table.showItem(id);
				}
			} else {
				table.unselect();
			}
		}
		table.clearAll();
		table.load(function () {
			return webix.ajax().get(master_url + "api/vnt/cotizacion-venta/" + that.quotation_id + "/tasks_pending/");
		});
		table_historial.clearAll();
		table_historial.load(function () {
			return webix.ajax().get(master_url + "api/vnt/cotizacion-venta/" + that.quotation_id + "/logs/");
		});

		$$("activity").attachEvent("onChange", function (value) {
			if (value && value.constructor !== Object) {
				var xhr = webix.ajax().sync().get(master_url + "api/crm/activity/" + value + "/has_responsable/");
				const json_response = JSON.parse(xhr.response);
				const date = $$("date");
				const reminder_time = $$("reminder_time");
				const description = $$("description");
				if (json_response.has_responsable) {
					date.define("required", true);
					reminder_time.define("required", true);
					description.define("required", false);
				} else {
					date.define("required", false);
					reminder_time.define("required", false);
					description.define("required", true);
				}
				date.refresh();
				reminder_time.refresh();
				description.refresh();
			}
		});
	}

	create(button) {
		const that = this;
		var get_values = this.$$("form").getValues();
		get_values["cotizacion_venta"] = this.quotation_id;
		get_values["task_type"] = 3;
		const request_options = {
			url: master_url + "api/crm/task/",
			get_values: get_values,
			validate: that.$$("form").validate(),
			message: "Tarea guardada",
			button: button,
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}