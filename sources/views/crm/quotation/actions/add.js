import {JetView} from "webix-jet";
import FormFieldsView from "../fields";
import SaveService from "../../../../services/save";
import "webix/breadcrumb";

export default class PersonView extends JetView {
	config() {
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation"},
				{value: "Agregar", link: "/home/crm.quotation.actions.add"},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Agregar cotización",
					css: "webix_primary",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.create(button);
					}
				},
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation?flush=true");
					}
				}
			]
		};

		this.fields = new FormFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			paddingX: 20,
			paddingY: 0,
			rows: [
				this.fields,
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						form,
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		const that = this;
		this.cotizacion = {};
		var xhr = webix.ajax().sync().get(master_url + "api/vnt/cotizacion-venta/init/");
		var data = JSON.parse(xhr.response);
		if (xhr.status === 200) {
			if (data.fields) {
				that.$$("form").setValues(data.fields, true);
			}
		} else if (xhr.status === 401) {
			webix.message({
				text: data.message,
				type: "error",
				expire: -1,
			});
			that.show("/home/crm.quotation");
		}

		this.fields.$$("entrega").attachEvent("onChange", function (value) {
			if (value === "2") {
				const es_prospecto = that.fields.$$("es_prospecto").getValue();
				if (es_prospecto === 0) {
					that.fields.$$("entrega_direccion_cliente").show();
				}
			} else {
				that.fields.$$("entrega_direccion_cliente").hide();
			}
		});
		this.fields.$$("es_prospecto").attachEvent("onChange", function (value) {
			if (value === 1) {
				that.fields.$$("cliente").hide();
				that.fields.$$("es_proyecto").show();
				that.fields.$$("lead").show();
				that.fields.$$("correo").show();
				that.fields.$$("direccion").show();
				that.fields.$$("telefono").show();
				that.fields.$$("atencion").show();
				that.fields.$$("persona").hide();
				that.fields.$$("entrega_direccion_cliente").hide();
				//show
			} else {
				that.fields.$$("cliente").show();
				that.fields.$$("es_proyecto").hide();
				that.fields.$$("lead").hide();
				that.fields.$$("correo").hide();
				that.fields.$$("direccion").hide();
				that.fields.$$("telefono").hide();
				that.fields.$$("atencion").hide();
				that.fields.$$("persona").show();
				if (that.fields.$$("entrega").getValue() === "2")
					that.fields.$$("entrega_direccion_cliente").show();
				//hide
			}
		});

		this.fields.$$("es_proyecto").attachEvent("onChange", function (value) {
			const lead_field = that.fields.$$("lead");
			lead_field.define("url", master_url + "api/crm/lead/select_list/?available_for_quote=1&is_proyect=" + value);
		});

		this.fields.$$("cliente").attachEvent("onChange", function (value) {
			if (value && value.constructor !== Object) {
				const field = that.fields.$$("persona");
				field.define("url", master_url + "api/rel/persona/select_list/?cliente_id=" + value);
				field.refresh();
				const field_entrega = that.fields.$$("entrega_direccion_cliente");
				field_entrega.define("url", master_url + "api/cxc/cliente-direcciones/select_list/?es_entrega=1&cliente_id=" + value);
				field_entrega.refresh();

				const params = {
					cliente_id: value,
					tipo_venta: that.fields.$$("tipo_venta").getValue()
				};
				webix.ajax().get(master_url + "api/vnt/cotizacion-venta/select_cliente_data/", params).then(function (response) {
					that.cotizacion = response.json();
					that.$$("form").setValues(that.cotizacion, true);
				});
			}
		});

		that.fields.$$("tipo_venta").attachEvent("onChange", function (value) {
			if (value === 2) {
				that.fields.$$("plazo").disable();
			} else if (value === 1) {
				that.fields.$$("plazo").enable();
			}
		});

		this.fields.$$("lead").attachEvent("onChange", function (value) {
			if(value.constructor === Object)
				value = value.id;
			webix.ajax().get(master_url + "api/vnt/cotizacion-venta/select_lead_data/", {lead_id: value}).then(function (response) {
				const cotizacion = response.json();
				that.$$("form").setValues(cotizacion, true);
			});
		});
	}

	create(button) {
		const that = this;
		const get_values = this.$$("form").getValues();
		if ("dias_credito" in that.cotizacion)
			get_values["dias_credito"] = that.cotizacion.dias_credito;
		if ("cuenta_pago" in that.cotizacion)
			get_values["cuenta_pago"] = that.cotizacion.cuenta_pago;
		const request_options = {
			url: master_url + "api/vnt/cotizacion-venta/",
			get_values: get_values,
			validate: that.$$("form").validate(),
			message: "Cotización guardada",
			path: "/home/crm.quotation",
			button: button,
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}
