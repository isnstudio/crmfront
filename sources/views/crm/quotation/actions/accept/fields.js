import {JetView,} from "webix-jet";
import "webix/server_autocomplete";

export default class FormFieldsView extends JetView {
	config() {
		const tipo_entrega = {
			view: "combo",
			id: "tipo_entrega_material",
			name: "tipo_entrega_material",
			label: "Tipo de entrega",
			labelPosition: "top",
			placeholder: "Tipo de entrega",
			suggest: {url: master_url + "api/vnt/orden-venta/choice_field/tipo_entrega_material/"},
		};

		const cliente = {
			view: "server_autocomplete",
			id: "cliente",
			name: "cliente",
			label: "Cliente",
			placeholder: "Cliente",
			url: master_url + "api/cxc/cliente/select_list/?status_str=['Activo']",
			required: true
		};

		const persona = {
			view: "server_autocomplete",
			name: "persona",
			label: "Atención a",
			id: "persona",
			labelPosition: "top",
			placeholder: "Atención a",
			url: master_url + "api/rel/persona/select_list/",
			required: true,
			disabled: true
		};

		const uso_cfdi = {
			view: "server_autocomplete",
			id: "c_uso_cfdi",
			name: "c_uso_cfdi",
			label: "Uso del CFDI",
			placeholder: "Uso del CFDI",
			labelPosition: "top",
			url: master_url + "api/cat/c_uso_cfdi/select_list/",
			required: true
		};

		const direccion_facturacion = {
			view: "server_autocomplete",
			id: "domicilio_fiscal_facturacion",
			name: "domicilio_fiscal_facturacion",
			label: "Dirección de facturación",
			placeholder: "Dirección de facturación",
			labelPosition: "top",
			disabled: true,
			url: ""
		};

		const proyecto_venta = {
			view: "server_autocomplete",
			id: "proyecto_venta",
			name: "proyecto_venta",
			label: "Proyecto de venta",
			placeholder: "Proyecto de venta",
			labelPosition: "top",
			url: master_url + "api/vnt/proyecto-venta/select_list/",
		};

		const orden_de_compra = {
			template: "Orden de compra",
			type: "section",
		};

		const orden_compra = {
			view: "text",
			name: "orden_compra",
			id: "orden_compra",
			label: "Orden de compra",
			labelPosition: "top",
			required: true
		};

		const uploader = {
			height: 50,
			rows: [
				{
					height: 40,
					view: "uploader",
					id: "file_uploader",
					value: "Agregar archivo",
					css: "webix-secondary",
					accept: "application/pdf, image/*",
					autosend: true,
					multiple: false,
					link: "file_list",
					upload: master_url + "api/cat/file/webix_upload/",
					on: {
						onFileUploadError: function (item) {
							webix.message(item);
						}
					}
				}
			]
		};

		const file_list = {
			view: "list",
			id: "file_list",
			type: "uploader",
			autoheight: true,
			borderless: true
		};

		const responsive_view = {
			margin: 10,
			rows: [
				tipo_entrega,
				cliente,
				persona,
				uso_cfdi,
				direccion_facturacion,
				proyecto_venta,
				orden_de_compra,
				orden_compra,
				uploader,
				file_list
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				{
					cols: [
						{
							margin: 10,
							rows: [
								tipo_entrega,
								persona,
								direccion_facturacion,
							]
						},
						{
							margin: 10,
							rows: [
								cliente,
								uso_cfdi,
								proyecto_venta,
							]
						}
					]
				},
				orden_de_compra,
				{
					cols: [
						{
							margin: 10,
							rows: [
								orden_compra,
							]
						},
						{
							margin: 10,
							rows: [
								uploader,
								file_list
							]
						}
					]
				}
			],

		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section
			]
		};
	}
}
