import {JetView} from "webix-jet";
import FormFieldsView from "./fields.js";
import Errors from "../../../../../helpers/errors.js";
import "webix/breadcrumb";
import SaveService from "../../../../../services/save";

export default class PersonView extends JetView {
	config() {
		this.quotation_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.quotation_id + " - Aceptar",
					link: "/home/crm.quotation.actions.accept?id=" + this.quotation_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Aceptar",
					css: "webix_primary",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation?id=" + this.quotation_id);
					}
				}
			]
		};

		this.fields = new FormFieldsView(this.app);

		this.form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 20,
			rows: [
				this.fields,
			],
			rules: {}
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						this.form,
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							this.form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		const that = this;
		webix.ajax().get(master_url + "api/vnt/cotizacion-venta/" + this.quotation_id + "/aceptar_data/").then(function (response) {
			that.cotizacion = response.json();
			that.$$("form").setValues(that.cotizacion, true);
		});
	}

	urlChange() {
		this.file_id = null;
		const file_uploader = this.fields.$$("file_uploader");
		const that = this;
		file_uploader.attachEvent("onFileUpload", function (res) {
			that.file_id = res["value"];
		});

		this.fields.$$("cliente").attachEvent("onChange", function (value) {
			if (value.id) value = value.id;
			const field = that.fields.$$("persona");
			field.define("url", master_url + "api/rel/persona/select_list/?cliente_id=" + value);
			field.refresh();

			const domicilio_fiscal_facturacion = that.fields.$$("domicilio_fiscal_facturacion");
			domicilio_fiscal_facturacion.define("url", master_url + "api/cxc/cliente-direcciones/select_list/?fiscal=1&cliente_id=" + value);
			if (value !== "") {
				domicilio_fiscal_facturacion.enable();
				field.enable();
			} else {
				domicilio_fiscal_facturacion.setValue(null);
				field.setValue(null);
				domicilio_fiscal_facturacion.disable();
				field.disable();
			}
			domicilio_fiscal_facturacion.refresh();

			webix.ajax().get(master_url + "api/vnt/cotizacion-venta/aceptar_select_cliente_data/", {cliente_id: value}).then(function (response) {
				that.cotizacion = response.json();
				that.$$("form").setValues(that.cotizacion, true);
			});
		});
	}

	action(button) {
		const that = this;
		var orden_compra_file = this.file_id;
		const file_uploader = this.fields.$$("file_uploader");
		if (file_uploader.files.count() === 0) {
			orden_compra_file = null;
		}

		const get_values = this.$$("form").getValues();
		get_values["orden_compra_file"] = orden_compra_file;
		get_values["almacen"] = that.cotizacion.almacen;
		get_values["correo_envio_xml"] = that.cotizacion.correo_envio_xml;
		get_values["instruccion_facturacion"] = that.cotizacion.instruccion_facturacion;


		const request_options = {
			url: master_url + "api/vnt/cotizacion-venta/" + that.quotation_id + "/aceptar/",
			get_values: get_values,
			validate: that.$$("form").validate(),
			message: "Cotización aceptada",
			path: "/home/crm.quotation?id=" + that.quotation_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}