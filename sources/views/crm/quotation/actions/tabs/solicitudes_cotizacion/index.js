import {JetView} from "webix-jet";
import PagerView from "../../../../../common/pager";
import "webix/breadcrumb";

export default class TableView extends JetView {
	config() {
		this.quotation_id = this.getParam("id");
		this.orden_venta_id = this.getParam("orden_venta_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{value: "#" + this.quotation_id, link: "/home/crm.quotation.actions.details?id=" + this.quotation_id},
				{
					value: "Solicitudes",
					link: "/home/crm.quotation.actions.tabs.solicitudes_cotizacion?id=" + this.quotation_id + "&orden_venta_id=" + this.orden_venta_id
				}
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Solicitar cotización",
					css: "webix_primary",
					height: 40,
					click: () => {
						this.webix.message("Solicitar cotización form");
					}
				},
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation.actions.details?flush=true");
					}
				}
			]
		};

		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			css: "units_table",
			select: true,
			pager: "pager",
			scroll: "y",
			autoheight: true,
			yCount: 30,
			columns: [
				{
					id: "id",
					header: "Folio",
					sort: "string",
					width: 45,
				},
				{
					id: "fecha",
					header: "Fecha",
					sort: "string",
					width: 135
				},
				{
					id: "solicito",
					header: {text: "Solicitó"},
					css: "upper",
					fillspace: true,
				},
				{
					id: "estatus",
					header: "Estatus",
					sort: "string",
					width: 140,
				}
			]
		};

		if (this.app.Mobile)
			return {
				view: "accordion",
				rows: [
					this.Filters,
					search,
					this.Table,
					{$subview: true, popup: true}
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							{
								view: "scrollview",
								scroll: "y",
								body: {
									rows: [
										datatable,
									]
								}
							},
							PagerView
						],
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					}
				]
			}
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}