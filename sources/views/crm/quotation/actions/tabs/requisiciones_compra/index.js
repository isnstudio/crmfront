import {JetView} from "webix-jet";
import PagerView from "../../../../../common/pager";

export default class TableView extends JetView {
	config() {
		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation.actions.details?id=" + this.id_quotation);
					}
				},
			]
		};

		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "y",
			rowHeight: 40,
			autoheight: true,
			columns: [
				{
					id: "folio",
					header: "Folio",
					fillspace: true,
				},
				{
					id: "fecha",
					header: "Fecha",
					fillspace: true,
				},
				{
					id: "Solicitó",
					header: "solicito",
					fillspace: true,
				},
				{
					id: "estatus",
					header: "Estatus",
					fillspace: true,
				},
				{
					id: "Info OC",
					header: "info_oc",
					fillspace: true,
				},
			],
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							{
								view: "scrollview",
								scroll: "y",
								body: {
									rows: [
										{
											view: "template",
											template: "Requisiciones de compra",
											type: "header",
										},
										datatable,
									]
								}
							},
							PagerView
						],
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					}
				]
			}
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}


	urlChange(ui, url) {
		this.id_quotation = this.getParam("id");
	}
}