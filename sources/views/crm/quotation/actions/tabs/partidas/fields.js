import {JetView,} from "webix-jet";
import "webix/server_autocomplete";
import "webix/multi_server_autocomplete";

export default class FormFieldsView extends JetView {
	config() {
		const tipo = {
			view: "combo",
			id: "tipo",
			name: "tipo",
			label: "Tipo",
			labelPosition: "top",
			placeholder: "Tipo",
			suggest: {url: master_url + "api/vnt/cotizacion-venta-detalle/choice_field/tipo/"},
			value: 1
		};

		const articulo = {
			id: "articulo",
			view: "server_autocomplete",
			label: "Articulo",
			labelPosition: "top",
			name: "articulo",
			placeholder: "Articulo",
			url: master_url + "api/inv/articulo/select_list/",
		};

		const copiar = {
			view: "button",
			name: "copiar",
			id: "copiar",
			label: "¿Copiar datos de articulo?",
			labelPosition: "top",
			disabled: true,
			css: {"margin-top": "32px !important"},
			click: () => {
				this._parent.$$("form").setValues(this._parent.cotizacion, true);
			}
		};

		const nombre_articulo = {
			view: "text",
			id: "nombre_composicion",
			name: "nombre_composicion",
			label: "Nombre del articulo",
			labelPosition: "top",
			placeholder: "Nombre del articulo",
			required: true
		};

		const descripcion = {
			view: "text",
			id: "concepto",
			name: "concepto",
			label: "Descripción",
			labelPosition: "top",
			placeholder: "Descripción",
			required: true
		};

		const cantidad = {
			view: "text",
			type: "number",
			name: "cantidad",
			id: "cantidad",
			label: "Cantidad",
			labelPosition: "top",
			placeholder: "Cantidad",
			required: true,
		};

		const um = {
			view: "combo",
			id: "um",
			name: "unidad_medida",
			label: "Unidad de medida",
			labelPosition: "top",
			placeholder: "Unidad de medida",
			suggest: {url: master_url + "api/cat/unidad-medida/select_list/"},
			required: true
		};

		const tipo_calculo = {
			view: "combo",
			id: "tipo_calculo",
			name: "tipo_calculo",
			label: "Calcular en base a:",
			labelPosition: "top",
			placeholder: "Unidad de medida",
			options: [{id: 1, value: "Base a margen"}, {id: 2, value: "Base a precio"}],
			value: 1
		};

		const peso = {
			view: "text",
			type: "number",
			name: "peso_kg",
			id: "peso",
			label: "Peso",
			labelPosition: "top",
			placeholder: "Peso",
			hidden: true,
			disabled: true
		};

		const precio = {
			view: "text",
			name: "precio_raw",
			id: "precio_raw",
			label: "Precio",
			labelPosition: "top",
			placeholder: "Precio",
			format: "111.000",
			disabled: true
		};

		const margen = {
			view: "text",
			name: "rendimiento",
			id: "rendimiento",
			label: "Margen de venta (%)",
			labelPosition: "top",
			placeholder: "Margen",
			value: 0,
			disabled: false
		};

		const impuestos = {
			view: "multicombo",
			id: "tipo_impuesto",
			name: "tipo_impuesto",
			label: "Impuestos",
			labelPosition: "top",
			options: master_url + "api/cat/tipo-impuesto/select_list/?se_emite=1",
		};

		const tiempo_entrega = {
			view: "text",
			type: "number",
			name: "tiempo_entrega",
			label: "Tiempo entrega (dias)",
			labelPosition: "top",
			placeholder: "Tiempo entrega (dias)",
		};

		const responsive_view = {
			margin: 10,
			rows: [
				tipo,
				articulo,
				copiar,
				nombre_articulo,
				descripcion,
				cantidad,
				um,
				peso,
				tipo_calculo,
				precio,
				margen,
				impuestos,
				tiempo_entrega,
				{height: 10}
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				{
					cols: [
						{
							margin: 10,
							rows: [
								tipo,
								articulo,
								copiar,
								nombre_articulo,
								descripcion,
								tiempo_entrega
							]
						},
						{
							margin: 10,
							rows: [
								cantidad,
								um,
								peso,
								tipo_calculo,
								precio,
								margen,
								impuestos,
							]
						}

					]
				},
			],

		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section
			]
		};
	}
}