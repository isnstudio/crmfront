import {JetView} from "webix-jet";
import Tables from "../../../../../../helpers/tables";
import UnitsDetailView from "./details";
import TablePartidasView from "./table";
import TableTotalesView from "../../table_totales";
import "webix/breadcrumb";


export default class PartidasView extends JetView {
	config() {
		this.totales = new TableTotalesView(this.app);
		this.Table = new TablePartidasView(this.app);

		if (!this.app.Mobile)
			this.UnitsDetailView = new UnitsDetailView(this.app);

		this.quotation_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.quotation_id,
					link: "/home/crm.quotation.actions.details?id=" + this.quotation_id
				},
				{value: "Partidas", link: "/home/crm.quotation.actions.tabs.partidas?id=" + this.quotation_id}
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
				{
					view: "icon",
					icon: "mdi mdi-plus",
					localId: "addButton",
					css: "right_icon",
					click: () => {
						this.show("../crm.quotation.actions.tabs.partidas.actions.add?id=" + this.quotation_id);
					}
				}
			]
		};

		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								TableTotalesView,
								search,
								this.Table,
								{$subview: true, popup: true}
							],
						},
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							this.Table,
						],
					},
					{
						width: 384,
						rows: [
							this.totales,
							this.UnitsDetailView,
						]
					}
				]
			}
		};
	}

	urlChange() {
		const url = master_url + "api/vnt/cotizacion-venta/" + this.quotation_id + "/detalles_webix/";
		const table =  this.Table.$$("data");
		const tables = new Tables(this.app);
		tables.dataLoading(table, url, this);
	}
}