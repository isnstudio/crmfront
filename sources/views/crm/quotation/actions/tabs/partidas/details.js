import {JetView} from "webix-jet";

export default class UnitsDetailView extends JetView {
	config() {
		this.quotation_id = this.getParam("id");
		const details = {
			rows: [
				{
					view: "template",
					template: "Detalles",
					id: "detalle_header",
					type: "header",
				},
				{
					view: "list",
					id: "detail_info",
					scroll: "y",
					autoheight: true,
					type: {
						template: obj =>
							`
								<span style="font-weight: 500;"><b>Descripción</b>: ${obj.concepto || ""}</span>
								<br>
							`
					},
				},
				{
					localId: "detail_components",
					autoheight: true,
					css: "wbackground",
					padding: 20,
					rows: []
				}
			]
		};

		return {
			rows: [
				details
			]
		};
	}

	init() {
		this.hideContents();
	}

	hideContents() {
		this.$$("detalle_header").hide();
		this.$$("detail_components").hide();
		this.$$("detail_info").hide();
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		this.$$("detalle_header").show();
		this.$$("detail_components").show();
		this.$$("detail_info").show();
		this.RefreshDetails(id);
	}

	showDetail(id) {
		if (id && id.constructor === Object && "id" in id) {
			id = id["id"];
		}
		this.partida_id = id;
		if (!this.partida_id)
			this.partida_id = this.getParam("partida_id");
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				child_ids.push(child_id);
			}
			for (let childIdsKey in child_ids) {
				content.removeView(child_ids[childIdsKey]);
			}
		}
		if (!isNaN(this.partida_id) && this.partida_id !== "") {
			if (this.getRoot()) {
				this.showContents(this.partida_id);
			}
		}
		if (!isNaN(this.partida_id) && this.partida_id !== "" && this.app.Mobile) {
			if (this.getRoot()) {
				this.showContents(this.partida_id);
			}
		}
	}

	urlChange() {
		this.showDetail();
	}

	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(master_url + "api/vnt/cotizacion-venta-detalle/" + id + "/wdetails/");
			const json_response = JSON.parse(xhr.response);
			if ("actions" in json_response && "id" in json_response) {
				const content = this.$$("detail_components");
				if (content) {
					if (json_response["actions"].length) {
						for (const action_key in json_response["actions"]) {
							const action = json_response["actions"][action_key];
							content.addView({
								view: "button",
								label: action["label"],
								height: 40,
								click: () => {
									this.show("/home/crm.quotation.actions.tabs.partidas.actions." + action["action"] +
										"?partida_id=" + json_response["id"] + "&id=" + this.quotation_id);
								}
							}, 0);
						}
						this.$$("detail_components").show();
					} else {
						this.$$("detail_components").hide();
					}
				}
			}
			if ("id" in json_response) {
				const sets = this.$$("detail_info");
				sets.clearAll();
				if (sets) {
					sets.show();
					sets.data_setter(json_response);
				}
			}
		}
	}
}