import {JetView} from "webix-jet";
import FormFieldsView from "../fields.js";
import Errors from "../../../../../../../helpers/errors";
import ExistenciasTableView from "./existencias_table.js";
import NoExistenciasTableView from "./no_existencias_table.js";
import SaveService from "../../../../../../../services/save";
import "webix/breadcrumb";

export default class PersonView extends JetView {
	config() {
		this.ExistenciasTableView = new ExistenciasTableView(this.app);
		this.NoExistenciasTableView = new NoExistenciasTableView(this.app);

		this.quotation_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.quotation_id,
					link: "/home/crm.quotation.actions.details?id=" + this.quotation_id
				},
				{value: "Partidas", link: "/home/crm.quotation.actions.tabs.partidas?id=" + this.quotation_id},
				{
					value: "Agregar",
					link: "/home/crm.quotation.actions.tabs.partidas.actions.add?id=" + this.quotation_id
				}
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					css: "webix_primary",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.create(button);
					}
				},
				{
					view: "button",
					value: "Guardar y agregar otro",
					css: "webix_primary",
					height: 40,
					click: () => {
						const button = this.$$("save");
						this.create_refresh(button);
					}
				},
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation.actions.tabs.partidas?id=" + this.quotation_id);
					}
				}
			]
		};

		this.fields = new FormFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				this.fields
			],
			rules: {
				precio_raw: function (value) {
					return value > 0;
				},
				cantidad: function (value) {
					return value > 0;
				}
			}
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						form,
						this.ExistenciasTableView,
						this.NoExistenciasTableView
					]
				}
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						{
							view: "scrollview",
							scroll: "y",
							body: {
								rows: [
									form,
									this.ExistenciasTableView,
									this.NoExistenciasTableView
								]
							}
						}

					]
				},
				{
					width: 384,
					rows: [
						{
							view: "template",
							template: "Acciones",
							type: "header",
						},
						buttons,
						{}
					]
				}
			]
		};
	}

	ready() {
		const that = this;
		this.costo_raw = 0;

		var xhr = webix.ajax().sync().get(master_url + "api/vnt/cotizacion-venta-detalle/init/?cotizacion_venta_id=" + this.quotation_id);
		var data = JSON.parse(xhr.response);
		if (xhr.status === 200) {
			that.almacen_id = data.almacen_id;
			that.$$("form").setValues(data, 1);

			const field_impuesto = that.fields.$$("tipo_impuesto");
			field_impuesto.refresh();
		} else if (xhr.status === 401) {
			webix.message({
				text: data.message,
				type: "error",
				expire: -1,
			});
			that.show("/home/crm.quotation.actions.tabs.partidas?id=" + this.quotation_id);
		}

		this.cotizacion = {};
		this.fields.$$("articulo").attachEvent("onChange", function (value) {
			if (value && value.constructor !== Object) {
				const field_um = that.fields.$$("um");
				field_um.define("suggest", {url: master_url + "api/inv/articulo/" + value + "/unidades_compatibles/"});
				field_um.refresh();

				detalleCotizacion();

				var tipo_value = that.fields.$$("tipo").getValue();
				if (tipo_value) {
					that.fields.$$("copiar").enable();
					that.fields.$$("tipo").disable();
					if (tipo_value === 1) {
						that.fields.$$("peso").show();
					}
				}
			} else {
				that.fields.$$("copiar").disable();
				that.fields.$$("peso").hide();
				that.fields.$$("tipo").enable();
			}
		});

		this.fields.$$("tipo").attachEvent("onChange", function (value) {
			const field = that.fields.$$("articulo");
			field.define("url", master_url + "api/inv/articulo/select_list/?tipo_articulo=1&uso=" + value);
			field.refresh();

			const tipo_calculo = that.fields.$$("tipo_calculo");
			const rendimiento = that.fields.$$("rendimiento");
			const precio = that.fields.$$("precio_raw");
			const tabla_existencias = that.ExistenciasTableView.$$("data_existencia");
			const partidas_existencia_header = that.ExistenciasTableView.$$("partidas_existencia_header");
			const tabla_no_existencias = that.NoExistenciasTableView.$$("data_no_existencia");
			const partidas_no_existencia_header = that.NoExistenciasTableView.$$("partidas_no_existencia_header");
			if (value === 1) {
				partidas_existencia_header.show();
				partidas_no_existencia_header.show();
				tabla_existencias.show();
				tabla_no_existencias.show();
				tipo_calculo.enable();
				tipo_calculo.setValue(1);
				rendimiento.enable();
			} else if (value === 2) {
				partidas_existencia_header.hide();
				partidas_no_existencia_header.hide();
				tabla_existencias.hide();
				tabla_no_existencias.hide();
				tipo_calculo.disable();
				tipo_calculo.setValue(2);
				rendimiento.disable();
				precio.enable();
			}
		});


		this.fields.$$("tipo_calculo").attachEvent("onChange", function (value) {
			if (value === 1) {
				that.fields.$$("precio_raw").disable();
				that.fields.$$("rendimiento").enable();
			} else {
				that.fields.$$("precio_raw").enable();
				that.fields.$$("rendimiento").disable();
			}

		});

		this.fields.$$("rendimiento").attachEvent("onBlur", function () {
			const articulo = that.fields.$$("articulo").getValue();
			const um = that.fields.$$("um").getValue();
			const tipo = that.fields.$$("tipo").getValue();
			var rendimiento = that.fields.$$("rendimiento").getValue();
			if (!rendimiento)
				rendimiento = 0;
			if (articulo || um) {
				webix.ajax().get(master_url + "api/vnt/cotizacion-venta/" + that.quotation_id +
					"/margen_venta/?costo_raw=" + that.costo_raw +
					"&rendimiento=" + rendimiento + "&tipo=" + tipo).then(function (response) {
					var json_response = response.json();
					that.$$("form").setValues(json_response, 1);
					if (json_response.mensaje) {
						webix.message({
							text: json_response.mensaje,
							type: "error",
							expire: -1,
						});
					}
				});
			}
		});

		this.fields.$$("precio_raw").attachEvent("onBlur", function () {
			const articulo = that.fields.$$("articulo").getValue();
			const um = that.fields.$$("um").getValue();
			var precio_raw = that.fields.$$("precio_raw").getValue();
			if (!precio_raw)
				precio_raw = 0;
			const tipo = that.fields.$$("tipo").getValue();
			if (articulo || um) {
				webix.ajax().get(master_url + "api/vnt/cotizacion-venta/" + that.quotation_id +
					"/margen_venta/?costo_raw=" + that.costo_raw +
					"&precio_raw=" + precio_raw + "&tipo=" + tipo).then(function (response) {
					var json_response = response.json();
					that.$$("form").setValues(json_response, 1);
					if (json_response.mensaje) {
						webix.message({
							text: json_response.mensaje,
							type: "error",
							expire: -1,
						});
					}
				});
			}

		});

		this.fields.$$("um").attachEvent("onChange", function () {
			detalleCotizacion();
		});


		function detalleCotizacion() {
			var um = that.fields.$$("um").getValue();
			var articulo = that.fields.$$("articulo").getValue();
			if (articulo) {
				webix.ajax().get(master_url + "api/vnt/cotizacion-venta/" + that.quotation_id +
					"/select_articulo/?unidad_medida_id=" + um + "&articulo_id=" + articulo).then(function (response) {
					that.cotizacion = response.json();
					that.costo_raw = that.cotizacion.costo_raw;
					if(um) {
						that.cotizacion.unidad_medida = um;
					}
					that.$$("form").setValues(that.cotizacion, 1);
					const tabla_existencias = that.ExistenciasTableView.$$("data_existencia");
					const tabla_no_existencias = that.NoExistenciasTableView.$$("data_no_existencia");
					tabla_existencias.clearAll();
					tabla_no_existencias.clearAll();
					tabla_existencias.parse(that.cotizacion["partidas_vendibles"]);
					tabla_no_existencias.parse(that.cotizacion["partidas_no_vendibles"]);
				});
			}
		}
	}

	create(button) {
		var get_values = this.$$("form").getValues();
		get_values["cotizacion_venta"] = this.quotation_id;
		get_values["descripcion"] = get_values["concepto"];

		const that = this;
		const request_options = {
			url: master_url + "api/vnt/cotizacion-venta-detalle/",
			get_values: get_values,
			validate: that.$$("form").validate(),
			message: "Partida guardada",
			path: "/home/crm.quotation.actions.tabs.partidas?id=" + that.quotation_id,
			button: button,
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}

	create_refresh(button) {
		var get_values = this.$$("form").getValues();
		get_values["cotizacion_venta"] = this.quotation_id;
		get_values["descripcion"] = get_values["concepto"];
		const that = this;
		const request_options = {
			url: master_url + "api/vnt/cotizacion-venta-detalle/",
			get_values: get_values,
			validate: that.$$("form").validate(),
			message: "Partida guardada",
			button: button,
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}