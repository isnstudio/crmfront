import {JetView} from "webix-jet";

export default class ExistenciasTableView extends JetView {
	config() {
		const datatable = {
			view: "datatable",
			id: "data_existencia",
			localId: "data_existencia",
			select: true,
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "almacen",
					header: "Almacen",
					fillspace: true,
				},
				{
					id: "costo_promedio_mx",
					header: {text: "Costo MXP", css: "right"},
					adjust: true,
					minWidth: 120,
					format: webix.i18n.priceFormat,
					css: "right"
				},
				{
					id: "costo_promedio_usd",
					header: {text: "Costo USD", css: "right"},
					adjust: true,
					minWidth: 120,
					format: webix.i18n.priceFormat,
					css: "right"
				},
				{
					id: "existencia",
					header: {text: "Existencia", css: "right"},
					adjust: true,
					css: "right"
				},
				{
					id: "reserva",
					header: {text: "Reserva", css: "right"},
					adjust: true,
					css: "right"
				},
			],
		};

		return {
			height: 400,
			rows: [
				{
					view: "template",
					template: "Partidas de existencia disponibles para venta",
					id: "partidas_existencia_header",
					type: "header",
				},
				datatable,
			],
		};
	}
}