import {JetView} from "webix-jet";
import RemoveService from "../../../../../../../services/remove";
import "webix/breadcrumb";

export default class TaskConcludeView extends JetView {
	config() {
		this.quotation_id = this.getParam("id");
		this.partida_id = this.getParam("partida_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.quotation_id,
					link: "/home/crm.quotation.actions.details?id=" + this.quotation_id
				},
				{
					value: "Partidas",
					link: "/home/crm.quotation.actions.tabs.partidas?id=" + this.quotation_id + "&partida_id=" + this.partida_id
				},
				{
					value: "#" + this.partida_id + " - Eliminar",
					link: "/home/crm.quotation.actions.tabs.partidas.actions.delete?id=" + this.quotation_id + "&partida_id=" + this.partida_id
				}
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Estás seguro de eliminar la partida?",
					type: "label",
					borderless: true,
					css: "question"
				},
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Eliminar",
					css: "webix_danger",
					height: 40,
					id: "remove",
					click: () => {
						const button = this.$$("remove");
						this.remove(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation.actions.tabs.partidas?id=" + this.quotation_id + "&partida_id=" + this.partida_id);
					}
				},
			]
		};


		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}

	remove(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/vnt/cotizacion-venta-detalle/" + this.partida_id + "/",
			validate: that.$$("form").validate(),
			message: "Partida eliminada",
			path: "/home/crm.quotation.actions.tabs.partidas?id=" + that.quotation_id,
			button: button,
			context: that,
		};
		this.RemoveService = new RemoveService(this.app);
		this.RemoveService.remove(request_options);
	}
}