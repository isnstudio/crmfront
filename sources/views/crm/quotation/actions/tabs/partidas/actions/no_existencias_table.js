import {JetView} from "webix-jet";

export default class NoExistenciasTableView extends JetView {
	config() {
		const datatable = {
			view: "datatable",
			id: "data_no_existencia",
			select: true,
			scroll: "xy",
			height: 400,
			localId: "data_no_existencia",
			rowHeight: 40,
			columns: [
				{
					id: "almacen",
					header: "Almacen",
					fillspace: true,
				},
				{
					id: "costo_promedio_mx",
					header: {text: "Costo MXP", css: "right"},
					adjust: true,
					minWidth: 120,
					format: webix.i18n.priceFormat,
					css: "right"
				},
				{
					id: "costo_promedio_usd",
					header: {text: "Costo USD", css: "right"},
					adjust: true,
					minWidth: 120,
					format: webix.i18n.priceFormat,
					css: "right"
				},
				{
					id: "existencia",
					header: {text: "Existencia", css: "right"},
					adjust: true,
					css: "right"
				},
				{
					id: "reserva",
					header: {text: "Reserva", css: "right"},
					adjust: true,
					css: "right"
				},
			],
		};

		return {
			height: 400,
			rows: [
				{
					view: "template",
					template: "Partidas de existencia NO disponibles para venta",
					id: "partidas_no_existencia_header",
					type: "header",
				},
				datatable,
			],
		};
	}
}