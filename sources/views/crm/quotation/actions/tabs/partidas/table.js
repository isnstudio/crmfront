import {JetView} from "webix-jet";
import PagerView from "../../../../../common/pager";

export default class TablePartidasView extends JetView {
	config() {
		this.quotation_id = this.getParam("id");
		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "numero_linea",
					header: "Num. Linea",
					width: 120,
				},
				{
					id: "cantidad",
					header: "Cantidad",
				},
				{
					id: "unidad_medida",
					header: "UM",
				},
				{
					id: "articulo__clave",
					header: "Clave",
					fillspace: true,
				},
				{
					id: "tiempo_entrega",
					header: "Entrega",
					template: "#tiempo_entrega# Dia(s)",
				},
				{
					id: "precio_raw",
					header: {text: "Precio unitario", css: "right"},
					css: "right",
					format: webix.i18n.priceFormat,
				},
				{
					id: "importe_raw",
					header: {text: "Importe", css: "right"},
					css: "right",
					format: webix.i18n.priceFormat,
				},
				{
					id: "impuesto",
					header: {text: "Impuesto", css: "right"},
					css: "right",
					format: webix.i18n.priceFormat,
				},
				{
					id: "total",
					header: {text: "Total", css: "right"},
					css: "right",
					format: webix.i18n.priceFormat,
				},
			],
			on: {
				onAfterRender() {
					const that = this;
					if (!that.count()) {
						that.showOverlay("<p>No hay información para mostrar</p>");
					} else {
						that.hideOverlay();
					}
					setTimeout(function () {
						that.enable();
					}, 1000);
				},
				onAfterSelect: (id) => {
					if(!this.app.Mobile && this._parent.UnitsDetailView) {
						this.setParam("partida_id", id, true);
						this._parent.UnitsDetailView.showDetail(id);
					} else {
						this.show("/home/crm.quotation.actions.tabs.partidas?id=" + this.quotation_id +
							"/crm.quotation.actions.tabs.partidas.popup_partida?partida_id=" + id + "?id=" + this.quotation_id);
					}
				},
			}
		};

		return {
			rows: [
				{
					view: "scrollview",
					scroll: "y",
					body: {
						rows: [
							datatable,
						]
					}
				},
				PagerView
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}

	urlChange(ui, url) {
		const datatable = this.$$("data");
		let id = this.getParam("partida_id");
		if (id) {
			if (id.constructor === Object) {
				if ("id" in id) {
					id = id["id"];
				}
			} else {
				if (datatable.exists(id)) {
					datatable.select(id);
					datatable.showItem(id);
				}
			}
		}
		if (id) {
			if (datatable.exists(id)) {
				datatable.select(id);
			} else {
				if(!this.app.Mobile && this._parent.UnitsDetailView) {
					this._parent.UnitsDetailView.hideContents();
				}
			}
		} else {
			datatable.unselect();
			if(!this.app.Mobile && this._parent.UnitsDetailView) {
				this._parent.UnitsDetailView.hideContents();
			}
		}
	}
}