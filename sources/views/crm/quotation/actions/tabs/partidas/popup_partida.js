import ModalView from "views/sections/modal";
import UnitsDetailView from "./details";

export default class UnitDetailsPopupView extends ModalView {
	constructor(app) {
		super(app, {
			header: {
				view: "toolbar", height: 56, padding: {left: 6, right: 14},
				cols: [
					{view: "icon", icon: "wxi-close", click: () => this.Hide()},
					{view: "label", label: "Partida"},
				]
			},
			body: {
				view: "scrollview",
				body: {
					rows: [
						UnitsDetailView,
					]
				}
			}
		});
	}

	init() {
		super.init();
	}
}