import {JetView} from "webix-jet";
import PagerView from "../../../../../common/pager";

export default class TableView extends JetView {
	config() {
		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation.actions.details?id=" + this.id_quotation);
					}
				},
			]
		};

		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "y",
			rowHeight: 40,
			autoheight: true,
			columns: [
				{
					id: "num_linea",
					header: "Num. Linea",
					fillspace: true,
				},
				{
					id: "cantidad",
					header: "Camtidad",
					fillspace: true,
				},
				{
					id: "descripcion",
					header: "Descripcion",
					fillspace: true,
				},
				{
					id: "precio_unitario",
					header: "Precio unitario",
					fillspace: true,
				},
				{
					id: "importe",
					header: "Importe",
					fillspace: true,
				},
				{
					id: "impuesto",
					header: "Impuesto",
					fillspace: true,
				},
				{
					id: "total",
					header: "Total",
					fillspace: true,
				},
			],
			on: {
				onAfterSelect: (id) => {
					// this.setParam("id", id, true);
					// this._parent.UnitsDetailView.showDetail(id);
				},
			}
		};


		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							{
								view: "scrollview",
								scroll: "y",
								body: {
									rows: [
										{
											view: "template",
											template: "Artículos",
											type: "header",
										},
										datatable,
									]
								}
							},
							PagerView
						],
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					}
				]
			}
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}


	urlChange(ui, url) {
		this.id_quotation = this.getParam("id");
		// const datatable = this.$$("data");
		// let id = this.getParam("id");
		// if (id) {
		// 	if (id.constructor === Object) {
		// 		if ("id" in id) {
		// 			id = id["id"];
		// 		}
		// 	} else {
		// 		if (datatable.exists(id)) {
		// 			datatable.select(id);
		// 			datatable.showItem(id);
		// 		}
		// 	}
		// }
		// if (id) {
		// 	if (datatable.exists(id)) {
		// 		datatable.select(id);
		// 	} else {
		// 		this._parent.UnitsDetailView.hideContents();
		// 	}
		// } else {
		// 	datatable.unselect();
		// 	this._parent.UnitsDetailView.hideContents();
		// }
	}
}