import {JetView} from "webix-jet";
import PagerView from "../../../../../common/pager";
import "webix/breadcrumb";

export default class TableView extends JetView {
	config() {
		this.quotation_id = this.getParam("id");
		this.orden_venta_id = this.getParam("orden_venta_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{value: "#" + this.quotation_id, link: "/home/crm.quotation.actions.details?id=" + this.quotation_id},
				{
					value: "Archivos",
					link: "/home/crm.quotation.actions.tabs.arhivos?id=" + this.quotation_id + "&orden_venta_id=" + this.orden_venta_id
				}
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Agregar archivo",
					css: "webix_primary",
					height: 40,
					click: () => {
						this.webix.message("Agregar archivo form");
					}
				},
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation.actions.details?id=" + this.id_quotation);
					}
				}
			]
		};

		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			css: "units_table",
			select: true,
			pager: "pager",
			scroll: "y",
			autoheight: true,
			yCount: 30,
			columns: [
				{
					id: "id",
					header: "Referencia",
					sort: "string",
					fillspace: true
				},
			]
		};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							{
								view: "scrollview",
								scroll: "y",
								body: {
									rows: [
										datatable,
									]
								}
							},
							PagerView
						],
					},
					{
						rows:[
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					}
				]
			}
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}

	urlChange(ui, url) {
		this.id_quotation = "";
		if ("id" in this._data) {
			this.id_quotation = this._data.id;
		}
	}
}