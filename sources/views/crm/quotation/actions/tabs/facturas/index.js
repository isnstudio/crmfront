import {JetView} from "webix-jet";
import UnitsDetailView from "./details";
import TableView from "./table";
import TableTotalesView from "../../table_totales";
import "webix/breadcrumb";


export default class PartidasView extends JetView {
	config() {
		this.totales = new TableTotalesView(this.app);
		this.Table = new TableView(this.app);
		if (!this.app.Mobile)
			this.UnitsDetailView = new UnitsDetailView(this.app);
		this.quotation_id = this.getParam("id");
		this.orden_venta_id = this.getParam("orden_venta_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{value: "#" + this.quotation_id, link: "/home/crm.quotation.actions.details?id=" + this.quotation_id},
				{value: "Facturas", link: "/home/crm.quotation.actions.tabs.partidas?id=" + this.quotation_id}
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		if (this.app.Mobile)
			return {
				view: "accordion",
				rows: [
					search,
					this.totales,
					this.Table,
					{$subview: true, popup: true}
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							this.Table,
						],
					},
					{
						width: 384,
						rows: [
							this.totales,
							{
								view: "template",
								template: "Detalles",
								id: "detalle_header",
								type: "header",
							},
							this.UnitsDetailView,
						]
					}
				]
			}
		};
	}

	urlChange() {
		// Data nav
		const table = this.Table.$$("data");
		let start = this.getParam("start");
		let count = this.getParam("count");
		let page = this.getParam("page");
		let selected_id = this.getParam("id_partida");
		if (!this.app.Mobile)
			this.UnitsDetailView.showDetail(selected_id);

		// Data loading
		const data_url = new URL(master_url + "api/vnt/orden-venta/" + this.orden_venta_id + "/facturas/");

		if (start) {
			data_url.searchParams.append("start", start);
		} else {
			data_url.searchParams.append("start", 0);
		}
		if (count) {
			data_url.searchParams.append("count", count);
		} else {
			data_url.searchParams.append("count", 20);
		}

		webix.ajax(data_url.href).then(function (data) {
			let json_data = data.json();
			table.parse(json_data);
			let pager = table.getPager();
			if (pager) {
				pager.disabled();
				if (page) {
					pager.select(page);
				} else {
					pager.select(0);
				}
			}
		}).finally(function (){
			let pager = table.getPager();
			if (pager) pager.enable();
		});
	}
}