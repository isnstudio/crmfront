import {JetView} from "webix-jet";

export default class UnitsDetailView extends JetView {
	config() {
		this.quotation_id = this.getParam("id");

		const details = {
			rows: [
				{
					localId: "detail_components",
					autoheight: true,
					css: "wbackground",
					padding: 20,
					rows: []
				}
			]
		};

		return {
			rows: [
				details
			]
		};
	}

	init() {
		this.hideContents();
	}

	hideContents() {
		this._parent.$$("detalle_header").hide();
		this.$$("detail_components").hide();
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		this._parent.$$("detalle_header").show();
		this.$$("detail_components").show();
		this.RefreshDetails(id);
	}

	showDetail(id) {
		if (id && id.constructor === Object && "id" in id) {
			id = id["id"];
		}
		this.factura_id = id;
		if (!this.factura_id)
			this.factura_id = this.getParam("factura_id");
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				child_ids.push(child_id);
			}
			for (let childIdsKey in child_ids) {
				content.removeView(child_ids[childIdsKey]);
			}
		}
		if (!isNaN(this.factura_id) && this.factura_id !== "") {
			if (this.getRoot()) {
				this.showContents(this.factura_id);
			}
		}
		if (!isNaN(this.factura_id) && this.factura_id !== "" && this.app.Mobile) {
			if (this.getRoot()) {
				this.showContents(this.factura_id);
			}
		}
	}

	urlChange() {
		this.showDetail();
	}

	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(master_url + "api/cxc/factura-electronica/" + id + "/ov_wdetails/");
			const json_response = JSON.parse(xhr.response);
			if ("actions" in json_response && "id" in json_response) {
				const content = this.$$("detail_components");
				if (content) {
					if (json_response["actions"].length) {
						for (const action_key in json_response["actions"]) {
							const action = json_response["actions"][action_key];
							const url = master_url.substring(0, master_url.length - 1) + action["action"];
							content.addView({
								view: "button",
								label: action["label"],
								height: 40,
								click: () => {
									window.open(url, "_blank");
								}
							}, 0);
						}
						this.$$("detail_components").show();
					} else {
						this.$$("detail_components").hide();
					}
				}
			}
		}
	}
}