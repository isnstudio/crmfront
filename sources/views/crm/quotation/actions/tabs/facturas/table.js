import {JetView} from "webix-jet";
import PagerView from "../../../../../common/pager";

export default class TableView extends JetView {
	config() {
		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "folio",
					header: "Folio",
					width: 80,
				},
				{
					id: "get_estatus_display",
					header: "Estatus",
					adjust: true,
				},
				{
					id: "created_on",
					header: "Fecha",
					adjust: true,
				},
				{
					id: "fecha_emision",
					header: "Fecha Emisión",
					adjust: true,
				},
				{
					id: "get_moneda_display",
					header: "Moneda",
					fillspace: true,
				},
				{
					id: "total",
					header: {text: "Total", css: "right"},
					css: "right",
					format: webix.i18n.priceFormat,
					adjust: true,
				},
				{
					id: "saldo",
					header: {text: "Saldo", css: "right"},
					css: "right",
					format: webix.i18n.priceFormat,
					adjust: true,
				},
			],
			on: {
				onAfterSelect: (id) => {
					this.setParam("factura_id", id, true);
					this._parent.UnitsDetailView.showDetail(id);
				},
			}
		};

		return {
			rows: [
				{
					view: "scrollview",
					scroll: "y",
					body: {
						rows: [
							datatable,
						]
					}
				},
				PagerView
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}


	urlChange(ui, url) {
		const datatable = this.$$("data");
		let id = this.getParam("factura_id");
		if (id) {
			if (id.constructor === Object) {
				if ("id" in id) {
					id = id["id"];
				}
			} else {
				if (datatable.exists(id)) {
					datatable.select(id);
					datatable.showItem(id);
				}
			}
		}
		if (id) {
			if (datatable.exists(id)) {
				datatable.select(id);
			} else {
				this._parent.UnitsDetailView.hideContents();
			}
		} else {
			datatable.unselect();
			this._parent.UnitsDetailView.hideContents();
		}
	}
}