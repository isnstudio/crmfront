import {JetView} from "webix-jet";
import PagerView from "../../../../../common/pager";

export default class TableView extends JetView {
	config() {
		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation.actions.details?flush=true");
					}
				},
				{
					view: "button",
					value: "Solicitar material",
					css: "webix_primary",
					height: 40,
					click: () => {
						this.webix.message("Solicitar material form")
						//this.show("/home/crm.quotation.actions.details?flush=true");
					}
				}
			]
		};

		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			css: "units_table",
			select: true,
			pager: "pager",
			scroll: "y",
			autoheight: true,
			yCount: 30,
			columns: [
				{
					id: "id",
					header: "Folio",
					sort: "string",
					width: 45,
				},
				{
					id: "fecha",
					header: "Fecha",
					sort: "string",
					width: 135
				},
				{
					id: "solicito",
					header: {text: "Solicitó"},
					css: "upper",
					fillspace: true,
				},
				{
					id: "solicitado_almacen",
					header: "Solicitado al almacén",
					sort: "string",
					width: 140,
				},
				{
					id: "surtir",
					header: "Para surtir el almacén",
					fillspace: true,
				},
				{
					id: "estatus",
					header: "Estatus",
					fillspace: true,
				},
			],
			on: {
				onAfterSelect: (id) => {
					// this.setParam("id", id, true);
					// this._parent.UnitsDetailView.showDetail(id);
				},
			}
		};


		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							{
								view: "scrollview",
								scroll: "y",
								body: {
									rows: [
										{
											view: "template",
											template: "Solicitudes de material",
											type: "header",
										},
										datatable,
									]
								}
							},
							PagerView
						],
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					}
				]
			}
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}


	urlChange(ui, url) {
		// const datatable = this.$$("data");
		// let id = this.getParam("id");
		// if (id) {
		// 	if (id.constructor === Object) {
		// 		if ("id" in id) {
		// 			id = id["id"];
		// 		}
		// 	} else {
		// 		if (datatable.exists(id)) {
		// 			datatable.select(id);
		// 			datatable.showItem(id);
		// 		}
		// 	}
		// }
		// if (id) {
		// 	if (datatable.exists(id)) {
		// 		datatable.select(id);
		// 	} else {
		// 		this._parent.UnitsDetailView.hideContents();
		// 	}
		// } else {
		// 	datatable.unselect();
		// 	this._parent.UnitsDetailView.hideContents();
		// }
	}
}