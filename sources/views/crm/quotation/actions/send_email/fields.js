import {JetView,} from "webix-jet";
import "webix/multi_server_autocomplete";

export default class FormFieldsView extends JetView {
	config() {
		const remitente = {
			view: "text",
			id: "remitente",
			name: "remitente",
			label: "Remitente",
			labelPosition: "top",
			placeholder: "Remitente",
		};

		const destinatarios = {
			view: "multi_server_autocomplete",
			id: "destinatarios",
			name: "destinatarios",
			label: "Destinatarios",
			placeholder: "Destinatarios",
			url: master_url + "api/rel/persona/select_list/",
		};

		const adicionales = {
			view: "text",
			id: "adicionales",
			name: "adicionales",
			label: "Adicionales (separados por coma)",
			labelPosition: "top",
			placeholder: "Adicionales (separados por coma)",
		};

		const asunto = {
			view: "text",
			id: "asunto",
			name: "asunto",
			label: "Asunto",
			labelPosition: "top",
			placeholder: "Asunto",
		};

		const uploader = {
			height: 50,
			rows: [
				{
					height: 40,
					view: "uploader",
					id: "file_uploader",
					value: "Agregar archivo",
					css: "webix-secondary",
					accept: "application/pdf,  application/vnd.openxmlformats-officedocument.wordprocessingml.document, image/png, image/jpeg",
					autosend: true,
					multiple: false,
					link: "file_list",
					upload: master_url + "api/cat/file/webix_upload/",
					on: {
						onFileUploadError: function (item) {
							webix.message(item);
						}
					}
				}
			]
		};

		const file_list = {
			view: "list",
			id: "file_list",
			type: "uploader",
			autoheight: true,
			borderless: true
		};

		const editor =  {
			view:"nic-editor", id:"correo_html",
		};

		const responsive_view = {
			margin: 10,
			rows: [
				remitente,
				destinatarios,
				adicionales,
				asunto,
				uploader,
				file_list
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				{
					cols: [
						{
							margin: 10,
							rows: [
								remitente,
								adicionales,
							]
						},
						{
							margin: 10,
							rows: [
								destinatarios,
								asunto,
								uploader,
								file_list
							]
						}

					]
				},
				editor
			],
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section
			]
		};
	}
}