import {JetView} from "webix-jet";

export default class TableTotalesView extends JetView {
	config() {
		this.quotation_id = this.getParam("id");
		return {
			rows: [
				{
					header: "Totales",
					type: "header",
					template: "Totales",
					id: "totales_header",
				},
				{
					view: "datatable",
					id: "totales",
					localId: "totales",
					scroll: "y",
					autoheight: true,
					url: master_url + "api/vnt/cotizacion-venta/" + this.quotation_id + "/totals/",
					columns: [
						{
							id: "label",
							header: "",
							css: "bold",
							sort: "string",
							fillspace: true
						},
						{
							id: "mn",
							header: {text: "MXP", css: "right"},
							format: webix.i18n.priceFormat,
							adjust: true
						},
						{
							id: "me",
							header: {text: "USD", css: "right"},
							format: webix.i18n.priceFormat,
							adjust: true
						},
					],
				}
			]
		};
	}
}