import {JetView, plugins} from "webix-jet";
import Errors from "../../../../../helpers/errors.js";
import "webix/breadcrumb";

export default class TaskConcludeView extends JetView {
	config() {
		const required_text = "Requerido";

		this.quotation_id = this.getParam("id");
		this.task_id = this.getParam("task_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.quotation_id + " - Seguimiento",
					link: "/home/crm.quotation.actions.followup?id=" + this.quotation_id + "&task_id=" + this.task_id
				},
				{
					value: "Tarea #" + this.task_id + " - Cancelar",
					link: "/home/crm.quotation.actions.task.cancel?id=" + this.quotation_id + "&task_id=" + this.task_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const cancel_motive = {
			view: "textarea",
			name: "cancel_motive",
			label: "Motivo",
			labelPosition: "top",
			placeholder: "Motivo",
			invalidMessage: required_text,
			height: 150,
			required: true,
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Estás seguro de marcar la tarea como no realizada?",
					type: "label",
					borderless: true,
					css: "question"
				},
				cancel_motive
			],
			rules: {
				cancel_motive: webix.rules.isNotEmpty,
			}
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					css: "webix_primary",
					height: 40,
					click: () => {
						const that = this;
						const form = this.$$("form");
						if (form.validate()) {
							webix.ajax().put(master_url + "api/crm/task/" + this.task_id + "/cancel/", form.getValues()).then(function (res) {
								webix.message({
									text: "Tarea no realizada",
									type: "success",
									expire: -1
								});
								that.show("/home/crm.quotation.actions.followup?id=" + that.quotation_id + "&task_id=" + that.task_id);
							}).fail(function (res) {
								const error = new Errors();
								error.show_error(res);
							});
						} else {
							webix.message({
								text: "Faltan campos por llenar",
								type: "error",
								expire: -1,
								id: "messageLead"
							});
						}
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation.actions.followup?id=" + this.quotation_id + "&task_id=" + this.task_id);
					}
				},
			]
		};

		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}
}
