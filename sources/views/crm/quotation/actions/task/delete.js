import {JetView, plugins} from "webix-jet";
import Errors from "../../../../../helpers/errors.js"
import "webix/breadcrumb";

export default class TaskConcludeView extends JetView {
	config() {
		this.quotation_id = this.getParam("id");
		this.task_id = this.getParam("task_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{value: "Seguimiento", link: "/home/crm.quotation.actions.followup?id=" + this.quotation_id + "&task_id=" + this.task_id},
				{value: "Eliminar tarea", link: "/home/crm.quotation.actions.task.delete?id=" + this.quotation_id + "&task_id=" + this.task_id},
			],
		};

		const search = {
			view: "toolbar", localId: "toolbar",
			paddingX: 10, height: 44, visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [
				{
					template: "¿Estás seguro de eliminar la tarea?",
					type: "label",
					borderless: true,
					css: "question"
				},
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Eliminar",
					css: "webix_danger",
					height: 40,
					click: () => {
						const that = this;
						const form = this.$$("form");
						if (form.validate()) {
							webix.ajax().del(master_url + "api/crm/task/" + that.task_id + "/").then(function (res) {
								webix.message({
									text: "Tarea eliminada",
									type: "success",
									expire: -1,
								});
								that.show("/home/crm.quotation.actions.followup?id=" + that.quotation_id + "&task_id=" + that.task_id);
							}).fail(function (res) {
								const error = new Errors();
								error.show_error(res);
							});
						}
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation.actions.followup?id=" + this.quotation_id + "&task_id=" + this.task_id);
					}
				},
			]
		};


		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}
}