import {JetView} from "webix-jet";
import SaveService from "../../../../services/save";
import "webix/breadcrumb";

export default class SendQuotationView extends JetView {

	config() {
		this.quotation_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.quotation_id + " - Enviar",
					link: "/home/crm.quotation.actions.send?id=" + this.quotation_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const type = {
			view: "combo",
			id: "notification_type",
			name: "notification_type",
			placeholder: "Tipo de mensaje",
			options: [
				{value: "SMS", id: "sms"},
				{value: "WhatsApp", id: "whatsapp"},
				{value: "Email", id: "email"}
			],
			required: true,
		};

		const contacts = {
			view: "multicombo",
			id: "contacts",
			name: "contacts",
			labelPosition: "top",
			label: "Contactos",
			placeholder: "Contactos",
			options: [],
			required: true,
			hidden: true
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Por cuál medio quiere enviar su mensaje?",
					type: "label",
					borderless: true,
					css: "question"
				},
				type,
				contacts
			],
			rules: {
				notification_type: webix.rules.isNotEmpty,
			}
		};


		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Enviar mensaje",
					height: 40,
					css: "webix_primary",
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation?id=" + this.quotation_id);
					}
				},
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								form,
								buttons,
							]
						}
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
						]
					}
				]
			}
		};
	}

	init() {
		var xhr = webix.ajax().sync().get(master_url + "api/vnt/cotizacion-venta/" + this.quotation_id + "/send_init/");
		const json_response = JSON.parse(xhr.response);
		this.es_prospecto = json_response.es_prospecto;
		this.cliente_id = json_response.cliente_id;
	}

	urlChange() {
		const that = this;

		this.$$("notification_type").attachEvent("onChange", function (value) {
			that.$$("contacts").show();
			let url = master_url + "api/vnt/cotizacion-venta/" + that.quotation_id;
			if ((value === "whatsapp") || (value === "sms")) {
				url = that.es_prospecto ? url + "/lead_contacts_phone/" : master_url + "api/rel/persona/select_list_send/?show=phone";
			} else if (value === "email") {
				url = that.es_prospecto ? url + "/lead_contacts_email/" : master_url + "api/rel/persona/select_list_send/?show=email";
			}
			if (!that.es_prospecto && that.cliente_id) {
				url = url + "&cliente_id=" + that.cliente_id;
			}
			const field = that.$$("contacts");
			field.define("suggest", {url: url});
			field.refresh();
		});
	}

	action(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/vnt/cotizacion-venta/" + this.quotation_id + "/send_notification/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Mensaje enviado",
			path: "/home/crm.quotation?id=" + that.quotation_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}