import {JetView, plugins} from "webix-jet";
import FormFieldsView from "../fields";
import TableView from "./table_totales";
import TablePartidasView from "./tabs/partidas/table";
import Tables from "../../../../helpers/tables";
import "webix/breadcrumb";

export default class PersonView extends JetView {
	config() {
		this.tableTotales = new TableView(this.app);
		this.tablePartidas = new TablePartidasView(this.app);
		this.quotation_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.quotation_id,
					link: "/home/crm.quotation.actions.details?id=" + this.quotation_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: []
		};

		this.fields = new FormFieldsView(this.app);

		this.form = {
			view: "form",
			localId: "form",
			id: "form",
			paddingX: 20,
			paddingY: 10,
			rows: [
				this.fields
			],
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							view: "accordion",
							rows: [
								search,
								buttons,
								{
									view: "accordionitem",
									header: "Cabecera cotización",
									type: "header",
									collapsed: true,
									body: this.form,
								},
								{
									view: "accordionitem",
									header: "Partidas",
									type: "header",
									collapsed: true,
									body: this.tablePartidas,
								}
							]
						}
					]
				}
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						{
							view: "scrollview",
							body: this.form
						},
						{
							view: "template",
							template: "Partidas",
							type: "header",
						},
						{
							view: "scrollview",
							body: this.tablePartidas
						}
					]
				},
				{
					width: 384,
					rows: [
						{
							view: "template",
							template: "Acciones",
							type: "header",
						},
						buttons,
						this.tableTotales,
						{}
					]
				}
			]

		};
	}

	init() {
		this.use(plugins.UrlParam, ["id"]);
	}

	urlChange() {
		this.id_quotation = "";
		if ("id" in this._data) {
			this.id_quotation = this._data.id;
		}
		const form = this.$$("form");
		for (var element in form.elements) {
			form.elements[element].disable();
		}
		if (!isNaN(this.id_quotation) && this.id_quotation !== "") {
			if (this.getRoot()) {
				var xhr = webix.ajax().sync().get(master_url + "api/vnt/cotizacion-venta/" + this.id_quotation + "/");
				var json_response = JSON.parse(xhr.response);

				if ("actions" in json_response && "id" in json_response) {
					const content = this.$$("buttons");
					if (content) {
						if (json_response["actions"].length) {
							for (const action_key in json_response["actions"]) {
								const action = json_response["actions"][action_key];
								content.addView({
									view: "button",
									label: action["label"],
									height: 40,
									click: () => {
										this.show("/home/crm.quotation.actions." + action["action"] + "?id=" + json_response["id"] + "&orden_venta_id=" + json_response["orden_venta"]);
									}
								}, 0);
							}
							this.$$("buttons").show();
						} else {
							this.$$("buttons").hide();
						}
					}
				}

				this.quotation = json_response;
				if (this.quotation.orden_venta__id) {
					this.fields.$$("orden_venta").show();
					this.fields.$$("direccion_entrega").show();
					this.fields.$$("tipo_entrega").show();
				}
				this.$$("form").setValues(this.quotation);

				const entrega = this.fields.$$("entrega").getValue();
				if (entrega === "2") {
					const es_prospecto = this.fields.$$("es_prospecto").getValue();
					if (es_prospecto === 0) {
						this.fields.$$("entrega_direccion_cliente").show();
					}
					this.fields.$$("entrega_direccion_cliente").show();
				} else {
					this.fields.$$("entrega_direccion_cliente").hide();
				}
			}
		}
		const url = master_url + "api/vnt/cotizacion-venta/" + this.quotation_id + "/detalles_webix/";
		const table = this.tablePartidas.$$("data");
		const tables = new Tables(this.app);
		tables.dataLoading(table, url, this);
	}

	ready() {
		const that = this;
		const value = this.fields.$$("es_prospecto").getValue();
		if (value === 1) {
			that.fields.$$("cliente").hide();
			that.fields.$$("es_proyecto").show();
			that.fields.$$("lead").show();
			that.fields.$$("correo").show();
			that.fields.$$("direccion").show();
			that.fields.$$("telefono").show();
			that.fields.$$("atencion").show();
			that.fields.$$("persona").hide();
			that.fields.$$("entrega_direccion_cliente").hide();
			//show
		} else {
			that.fields.$$("cliente").show();
			that.fields.$$("es_proyecto").hide();
			that.fields.$$("lead").hide();
			that.fields.$$("correo").hide();
			that.fields.$$("direccion").hide();
			that.fields.$$("telefono").hide();
			that.fields.$$("atencion").hide();
			that.fields.$$("persona").show();
			if (that.fields.$$("entrega").getValue() === "2")
				that.fields.$$("entrega_direccion_cliente").show();
			//hide
		}
	}
}