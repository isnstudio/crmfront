import {JetView,} from "webix-jet";
import "webix/server_autocomplete";

export default class FormFieldsView extends JetView {
	config() {
		const factura_general = {
			template: "Información de factura general", type: "section", id: "factura_section"
		};

		const proyecto = {
			template: "Información de proyecto", type: "section", id: "proyecto_section", hidden: true
		};

		const cliente = {
			view: "text",
			id: "cliente",
			name: "cliente",
			label: "Cliente",
			labelPosition: "top",
			placeholder: "Cliente",
			disabled: true,
		};

		const almacen = {
			view: "text",
			id: "almacen",
			name: "almacen",
			label: "Almacén",
			labelPosition: "top",
			disabled: true
		};

		const direccion_entrega = {
			view: "server_autocomplete",
			id: "domicilio_fiscal_facturacion",
			name: "domicilio_fiscal_facturacion",
			label: "Dirección",
			labelPosition: "top",
			url: master_url + "api/cxc/cliente-direcciones/select_list/ ",
			required: true,
		};

		const instrucciones = {
			view: "textarea",
			name: "descripcion",
			id: "descripcion",
			height: 130,
			label: "Instrucciones para el almacen",
			labelPosition: "top",
		};

		const forma_pago = {
			view: "combo",
			id: "forma_pago_obj",
			name: "forma_pago_obj",
			label: "Forma de pago",
			placeholder: "Forma de pago",
			labelPosition: "top",
			suggest: {url: master_url + "api/cat/metodo-pago/select_list/"},
			required: true
		};

		const cerrar_orden = {
			view: "checkbox",
			name: "cerrar_orden",
			id: "cerrar_orden",
			label: "Cerrar orden",
			labelPosition: "top",
			value: true,
			required: true,
		};

		const metodo_pago = {
			view: "combo",
			name: "c_metodo_pago",
			id: "c_metodo_pago",
			label: "Metodo de pago",
			labelPosition: "top",
			suggest: {url: master_url + "api/cat/c-metodo-pago/select_list/"},
			required: true
		};

		const base = {
			view: "combo",
			name: "tipo_enmascarado",
			id: "tipo_enmascarado",
			label: "En base a",
			labelPosition: "top",
			suggest: {url: master_url + "api/vnt/orden-venta-datos-facturacion/choice_field/tipo_enmascarado/"},
			value: 1,
			required: true
		};

		const proyecto_venta = {
			view: "server_autocomplete",
			id: "proyecto_venta",
			name: "proyecto_venta",
			label: "Proyecto",
			url: master_url + "api/vnt/proyecto-venta/select_list/",
			labelPosition: "top",
			hidden: true
		};

		const unidad_medida = {
			view: "server_autocomplete",
			id: "unidad_medida",
			name: "unidad_medida",
			label: "Unidad de medida",
			url: master_url + "api/cat/unidad-medida/select_list/",
			labelPosition: "top",
			hidden: true
		};

		const clave_producto = {
			view: "server_autocomplete",
			id: "c_clave_productos_servicios_sat",
			name: "c_clave_productos_servicios_sat",
			label: "Clave de producto",
			url: master_url + "api/cat/sat/clave-productos-servicios/select_list/",
			labelPosition: "top",
			hidden: true
		};

		const desglosar = {
			view: "checkbox",
			name: "detallado",
			id: "detallado",
			label: "¿Desglosar material?",
			labelPosition: "top",
			hidden: true,
		};

		const cuenta_pago = {
			view: "text",
			name: "cuenta_pago",
			id: "cuenta_pago",
			label: "Cuenta pago",
			labelPosition: "top",
		};

		const uso_cfdi = {
			view: "server_autocomplete",
			id: "c_uso_cfdi",
			name: "c_uso_cfdi",
			label: "Uso del CFDI",
			placeholder: "Uso del CFDI",
			labelPosition: "top",
			url: master_url + "api/cat/c_uso_cfdi/select_list/",
			required: true
		};

		const responsive_view = {
			margin: 10,
			rows: [
				factura_general,
				cliente,
				almacen,
				direccion_entrega,
				instrucciones,
				forma_pago,
				cerrar_orden,
				metodo_pago,
				base,
				cuenta_pago,
				uso_cfdi,
				proyecto,
				proyecto_venta,
				clave_producto,
				unidad_medida,
				desglosar
			]
		};
		const standard_view = {
			margin: 10,
			rows: [
				factura_general,
				{
					cols: [
						{
							margin: 10,
							rows: [
								cliente,
								direccion_entrega,
								forma_pago,
								metodo_pago,
								cuenta_pago,
								uso_cfdi
							]
						},
						{
							margin: 10,
							rows: [
								almacen,
								instrucciones,
								cerrar_orden,
								base,
							]
						}

					]
				},
				proyecto,
				{
					cols: [
						{
							margin: 10,
							rows: [
								proyecto_venta,
								clave_producto
							]
						},
						{
							margin: 10,
							rows: [
								unidad_medida,
								desglosar
							]
						}

					]
				},
			],

		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section
			]
		};
	}
}