import {JetView} from "webix-jet";
import Errors from "../../../../../../../helpers/errors";

export default class TableConceptoView extends JetView {
	config() {
		const that = this;
		const datatable = {
			view: "datatable",
			id: "data_articulo",
			select: true,
			scroll: "xy",
			rowHeight: 40,
			editable: true,
			columns: [
				{
					id: "concepto",
					header: "Concepto",
					adjust: true,
				},
				{
					id: "unidad_medida_base",
					header: "UMB",
					fillspace: true,
				},
				{
					id: "cantidad_solicitada",
					header: "Solicitada",
					adjust: true,
				},
				{
					id: "saldo_pendiente",
					header: "Pendiente",
					adjust: true,
				},
				{
					id: "cantidad_a_facturar",
					header: "Cant. Fact.",
					minWidth: 140,
					editor: "text",
					format: "111.000",
					adjust: true,
					css: {"background-color": "#e7e9ed"},
				},
				{
					id: "precio_raw",
					header: {text: "Precio unitario", css: "right"},
					format: webix.i18n.priceFormat,
					css: "right",
					adjust: true,
				},
				{
					id: "cantidad",
					header: "Cant.",
					minWidth: 140,
					format: "1,111.000",
				},
				{
					id: "unidad_medida",
					header: "UM",
					adjust: true,
					editor: "select",
					minWidth: 80,
					options: [],
					css: {"background-color": "#e7e9ed"},
					template: "#unidad_medida__display_webix#"
				},
				{
					id: "importe",
					header: {text: "Importe", css: "right"},
					format: webix.i18n.priceFormat,
					css: "right",
					width: 150
				},
			],
			on: {
				onEditorChange: (id, value) => {
					var item = that.$$("data_articulo").getItem(id.row);
					const url_request = master_url + "api/vnt/orden-venta-datos-facturacion-detalle/"
						+ id.row + "/unidad_medida/";
					if (id.column == "cantidad_a_facturar") {
						const values = {
							"cantidad_a_facturar": value,
							"unidad_medida_id": item.unidad_medida
						};
						request(url_request, values);
					} else {
						const values = {"unidad_medida_id": value};
						request(url_request, values);
					}

					function request(url_request, values) {
						webix.ajax().put(url_request, values).then(function (res) {
							const response_json = res.json();
							that.$$("data_articulo").updateItem(id.row, {"importe": response_json.importe});
							that.$$("data_articulo").updateItem(id.row, {"cantidad": response_json.cantidad});
							that.$$("data_articulo").updateItem(id.row, {"unidad_medida__display_webix": response_json.unidad_medida__display_webix});
						}).fail(function (res) {
							const error = new Errors();
							error.show_error(res);
						});
					}
				},
				onBeforeEditStart: function (id) {
					if (id.column == "unidad_medida") {
						var config = this.getColumnConfig(id.column);
						config.editor = "select";
						var item = this.getItem(id.row);
						var xhr = webix.ajax().sync().get(master_url + "api/vnt/orden-venta-datos-facturacion-detalle/" + item.id + "/unidades_compatibles/");
						var data = JSON.parse(xhr.response);
						config.options = data;
					}
				},
			}
		};

		return {
			rows: [
				{
					view: "scrollview",
					scroll: "y",
					body: {
						rows: [
							{
								view: "template",
								template: "Conceptos de facturación",
								id: "articulos",
								type: "header",
							},
							datatable,
						]
					}
				},
			],
		};
	}
}