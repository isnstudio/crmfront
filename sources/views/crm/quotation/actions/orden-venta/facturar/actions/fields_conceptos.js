import {JetView,} from "webix-jet";
import "webix/server_autocomplete";

export default class FieldsConceptosView extends JetView {
	config() {

		const cantidad = {
			view: "text",
			id: "cantidad",
			name: "cantidad",
			label: "Cantidad",
			labelPosition: "top",
			placeholder: "Cantidad",
			disabled: true,
		};

		const unidad_medida = {
			view: "text",
			id: "unidad_medida",
			name: "unidad_medida",
			label: "Unidad medida",
			labelPosition: "top",
			disabled: true
		};

		const concepto = {
			view: "textarea",
			id: "concepto",
			name: "concepto",
			label: "Concepto",
			height: 130,
			labelPosition: "top",
		};

		const precio = {
			view: "text",
			id: "precio_raw",
			name: "precio_raw",
			label: "Precio",
			labelPosition: "top",
			labelAlign: "right",
			disabled: true,
			format: "$111,111.00",
			css: "right_input"
		};

		const responsive_view = {
			margin: 10,
			rows: [
				cantidad,
				unidad_medida,
				concepto,
				precio
			]
		};
		const standard_view = {
			margin: 10,
			cols: [
				{
					margin: 10,
					rows: [
						{
							cols: [
								cantidad,
								unidad_medida,
								precio
							]
						},
						concepto,
					]
				},
			]

		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section
			]
		};
	}
}