import {JetView} from "webix-jet";
import Errors from "../../../../../../../helpers/errors.js";
import TableArticuloView from "./table_articulos";
import FieldsConceptosView from "./fields_conceptos.js";
import "webix/breadcrumb";
import RequestService from "../../../../../../../services/request";

export default class PersonView extends JetView {
	config() {
		this.quotation_id = this.getParam("cotizacion_id");
		this.orden_venta_id = this.getParam("id");
		this.data_id = this.getParam("data_id");
		this.TableArticulo = new TableArticuloView(this.app);

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.quotation_id,
					link: "/home/crm.quotation.actions.details?id=" + this.quotation_id
				},
				{
					value: "Proceso de facturación", link: "/home/crm.quotation.actions.orden-venta.facturar?id=" +
						this.orden_venta_id + "&cotizacion_id=" + this.quotation_id + "&data_id=" + this.data_id
				},
				{
					value: "Detalles",
					link: "/home/crm.quotation.actions.orden-venta.facturar.actions.details_proyecto?id=" +
						this.orden_venta_id + "&cotizacion_id=" + this.quotation_id + "&data_id=" + this.data_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Facturar",
					css: "webix_primary",
					height: 40,
					id: "request",
					click: () => {
						const button = this.$$("request");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation.actions.orden-venta.facturar?id=" +
							this.orden_venta_id + "&cotizacion_id=" + this.quotation_id + "&data_id=" + this.data_id);
					}
				},
			]
		};

		this.fields = new FieldsConceptosView(this.app);

		this.form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 20,
			rows: [
				this.fields,
			],
			rules: {}
		};

		const tabs = {
			view: "tabview",
			localId: "tabs",
			autoheight: true,
			cells: [
				{
					header: "Articulos",
					body: this.TableArticulo,
					id: "articulos",
				},
				{
					header: "Conceptos de facturación",
					body: this.form
				},
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						tabs
					]
				}
			};

		return {
			type: "space",
			cols: [
				{
					scroll: "y",
					rows: [
						search,
						{
							view: "scrollview",
							scroll: "y",
							autoheight: true,
							body: {
								rows: [
									tabs
								]
							}
						},
					]
				},
				{
					width: 384,
					rows: [
						{
							view: "template",
							template: "Acciones",
							type: "header",
						},
						buttons,
						{
							localId: "spacer",
							css: "template-scroll-y",
							borderless: true,
						}
					]
				}
			]
		};
	}


	ready() {
		const that = this;
		var xhr;
		var data;
		if (!that.data_id) {
			xhr = webix.ajax().sync().get(master_url + "api/vnt/orden-venta-datos-facturacion-detalle/init/?orden_venta_id=" + this.orden_venta_id);
			data = JSON.parse(xhr.response);
			if (xhr.status === 200) {
				const table_articulo = that.TableArticulo.$$("data_articulo");
				table_articulo.parse(data);
			} else if (xhr.status === 401) {
				webix.message({
					text: data.message,
					type: "error",
					expire: -1,
				});
				that.show("/home/crm.quotation");
			}
		} else {
			xhr = webix.ajax().sync().get(master_url + "api/vnt/orden-venta-datos-facturacion/" + that.data_id + "/detalles/");
			data = JSON.parse(xhr.response);
			const table_articulo = that.TableArticulo.$$("data_articulo");
			table_articulo.parse(data);

			that.conceptos_de_facturacion();
		}


		this.fields.$$("concepto").attachEvent("onBlur", function () {
			const concepto = that.fields.$$("concepto").getValue();
			webix.ajax().put(master_url + "api/vnt/orden-venta-datos-facturacion/" + that.data_id + "/concepto_proyecto/", {concepto_proyecto: concepto}).then(function () {
				that.webix.message("Concepto actualizado");
			}).fail(function (res) {
				const error = new Errors();
				error.show_error(res);
			});
		});
	}

	conceptos_de_facturacion() {
		var xhr = webix.ajax().sync().get(master_url + "api/vnt/orden-venta-datos-facturacion/" + this.data_id + "/conceptos_de_facturacion/");
		var data = JSON.parse(xhr.response);
		const form_conceptos = this.$$("form");
		form_conceptos.setValues(data);
	}

	action(button) {
		const that = this;
		var get_values = {orden_venta_datos_facturacion_id: this.data_id};
		const request_options = {
			ajax:webix.ajax().put(master_url + "api/vnt/orden-venta/" + this.orden_venta_id + "/facturar/", get_values),
			message: "Factura generada",
			path: "/home/crm.quotation?id=" + that.quotation_id,
			button: button,
			context: that,
		};
		this.RequestService = new RequestService(this.app);
		this.RequestService.request(request_options);
	}
}