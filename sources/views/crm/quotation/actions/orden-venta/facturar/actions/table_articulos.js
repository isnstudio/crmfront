import {JetView} from "webix-jet";
import Errors from "../../../../../../../helpers/errors";

export default class TableArticuloView extends JetView {
	config() {
		const that = this;
		const datatable = {
			view: "datatable",
			id: "data_articulo",
			select: true,
			scroll: "xy",
			rowHeight: 40,
			editable: true,
			columns: [
				{
					id: "numero_linea",
					header: "Num. Linea",
					minWidth: 120,
					adjust: true,
				},
				{
					id: "unidad_medida",
					header: "UM",
					adjust: true,
				},
				{
					id: "articulo__clave",
					header: "Clave",
					fillspace: true,
				},
				{
					id: "cantidad_a_facturar",
					header: "Cantidad a facturar",
					minWidth: 180,
					editor: "text",
					format: "111.000",
					adjust: true,
				},
				{
					id: "saldo_pendiente",
					header: "Pendiente",
					adjust: true,
				},
				{
					id: "cantidad_solicitada",
					header: "Solicitada",
					adjust: true,
				},
				{
					id: "precio_raw",
					header: {text: "Precio unitario", css: "right"},
					format: webix.i18n.priceFormat,
					css: "right",
					adjust: true,
				},
				{
					id: "importe",
					header: {text: "Importe", css: "right"},
					format: webix.i18n.priceFormat,
					css: "right",
					width: 200
				},
			],
			on: {
				onEditorChange: (id, value) => {
					webix.ajax().put(master_url + "api/vnt/orden-venta-datos-facturacion-detalle/"
						+ id.row + "/cantidad_a_facturar/", {"cantidad_a_facturar": value}).then(function (res) {
						const response_json = res.json();
						that.$$("data_articulo").updateItem(id.row, {"importe": response_json.importe});
						if (that._parent.conceptos_de_facturacion)
							that._parent.conceptos_de_facturacion();
					}).fail(function (res) {
						const error = new Errors();
						error.show_error(res);
					});
				}
			}
		};

		return {
			rows: [
				{
					view: "scrollview",
					scroll: "y",
					body: {
						rows: [
							{
								view: "template",
								template: "Articulos a facturar",
								id: "articulos",
								type: "header",
							},
							datatable,
						]
					}
				},
			],
		};
	}
}