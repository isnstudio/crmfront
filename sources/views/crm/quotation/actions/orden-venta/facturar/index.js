import {JetView} from "webix-jet";
import FormFieldsView from "./fields.js";
import Errors from "../../../../../../helpers/errors.js";
import "webix/breadcrumb";

export default class PersonView extends JetView {
	config() {
		this.quotation_id = this.getParam("cotizacion_id");
		this.orden_venta_id = this.getParam("id");
		this.data_id = this.getParam("data_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.quotation_id,
					link: "/home/crm.quotation.actions.details?id=" + this.quotation_id
				},
				{
					value: "OV#" + this.orden_venta_id + " - Facturación",
					link: "/home/crm.quotation.actions.orden-venta.facturar?id=" + this.orden_venta_id + "&cotizacion_id=" + this.quotation_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Siguiente",
					css: "webix_primary",
					height: 40,
					click: () => {
						const that = this;
						if (this.$$("form").validate()) {
							var get_values = this.$$("form").getValues();
							get_values["orden_venta"] = that.orden_venta_id;
							if (!that.data_id) {
								webix.ajax().post(master_url + "api/vnt/orden-venta-datos-facturacion/", get_values).then(function (res) {
									AfterRequest(res, "Datos generados");
								}).fail(function (res) {
									const error = new Errors();
									error.show_error(res);
								});
							} else {
								webix.ajax().put(master_url + "api/vnt/orden-venta-datos-facturacion/" + that.data_id + "/", get_values).then(function (res) {
									AfterRequest(res, "Datos actualizados");
								}).fail(function (res) {
									const error = new Errors();
									error.show_error(res);
								});
							}

							function AfterRequest(res, message) {
								that.webix.message(message);
								var json_response = res.json();
								const tipo_enmascarado = get_values["tipo_enmascarado"];
								var details = "details_general";
								if (tipo_enmascarado == "2") {
									details = "details_proyecto";
								} else if (tipo_enmascarado == "3") {
									details = "details_um";
								}
								that.show("/home/crm.quotation.actions.orden-venta.facturar.actions." + details + "?id=" +
									that.orden_venta_id + "&cotizacion_id=" + that.quotation_id + "&data_id=" + json_response.id);
							}
						} else {
							webix.message({
								text: "Faltan campos por llenar",
								type: "error",
								expire: -1,
								id: "messageLead"
							});
						}
					}
				},
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation?id=" + this.quotation_id);
					}
				},
			]
		};

		this.fields = new FormFieldsView(this.app);

		this.form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 20,
			rows: [
				this.fields,
			],
			rules: {}
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						this.form,
					]
				}
			};

		return {
			type: "space",
			cols: [
				{
					scroll: "y",
					rows: [
						search,
						{
							view: "scrollview",
							scroll: "y",
							autoheight: true,
							body: {
								rows: [
									this.form,
								]
							}
						},
					]
				},
				{
					width: 384,
					rows: [
						{
							view: "template",
							template: "Acciones",
							type: "header",
						},
						buttons,
						{
							localId: "spacer",
							css: "template-scroll-y",
							borderless: true,
						}
					]
				}
			]
		};
	}


	ready() {
		const that = this;
		if (!this.data_id) {
			var xhr = webix.ajax().sync().get(master_url + "api/vnt/orden-venta-datos-facturacion/init/?orden_venta_id=" + that.orden_venta_id);
			var data = JSON.parse(xhr.response);
			that.$$("form").setValues(data, true);
		} else {
			var xhr = webix.ajax().sync().get(master_url + "api/vnt/orden-venta-datos-facturacion/" + that.data_id + "/");
			var data = JSON.parse(xhr.response);
			that.$$("form").setValues(data, true);
			if (data.tipo_enmascarado) {
				tipo_enmascarado(data.tipo_enmascarado);
			}
		}

		this.fields.$$("tipo_enmascarado").attachEvent("onChange", function (value) {
			tipo_enmascarado(value);
		});

		function tipo_enmascarado(value) {
			if ((value == 1) || (value == 3)) {
				that.fields.$$("proyecto_section").hide();
				that.fields.$$("proyecto_venta").hide();
				that.fields.$$("c_clave_productos_servicios_sat").hide();
				that.fields.$$("unidad_medida").hide();
				that.fields.$$("detallado").hide();
			} else {
				that.fields.$$("proyecto_section").show();
				that.fields.$$("proyecto_venta").show();
				that.fields.$$("c_clave_productos_servicios_sat").show();
				that.fields.$$("unidad_medida").show();
				that.fields.$$("detallado").show();
			}
		}
	}
}