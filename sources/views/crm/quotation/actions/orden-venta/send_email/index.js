import {JetView} from "webix-jet";
import FormFieldsView from "./fields.js";
import SaveService from "../../../../../../services/save";
import "webix/breadcrumb";

export default class PersonView extends JetView {
	config() {
		this.orden_venta_id = this.getParam("id");
		this.quotation_id = this.getParam("cotizacion_id");
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de Venta", link: "/home/crm.quotation?id=" + this.orden_venta_id},
				{
					value: "#" + this.quotation_id,
					link: "/home/crm.quotation.actions.details?id=" + this.quotation_id
				},
				{
					value: "OV #" + this.orden_venta_id + " - Enviar por Correo",
					link: "/home/crm.quotation.actions.orden-venta.send_email?id=" + this.orden_venta_id + "&cotizacion_id=" + this.quotation_id
				},
			],
		};

		const search = {
			view: "toolbar",
			ocalId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					css: "webix_primary",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation?id=" + this.orden_venta_id);
					}
				}
			]
		};

		this.fields = new FormFieldsView(this.app);

		this.form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 20,
			rows: [
				this.fields,
			],
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						this.form,
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							this.form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		var xhr = webix.ajax().sync().get(master_url + "/api/vnt/orden-venta/" + this.orden_venta_id + "/init_correo/");
		var data = JSON.parse(xhr.response);
		this.fields.$$("correo_html").setValue(data.correo_html);
		this.$$("form").setValues(data, true);

		const destinatarios = this.fields.$$("destinatarios");
		var url_destinatarios = master_url + "api/rel/persona/select_list/?persona=";
		for (let i = 0; i < data.destinatarios.length; i++) {
			if(i === 0)
				url_destinatarios += data.destinatarios[i];
			else url_destinatarios += "&persona=" + data.destinatarios[i];
		}
		destinatarios.define("url", url_destinatarios);
		this.file_id = null;
		const file_uploader = this.fields.$$("file_uploader");
		const that = this;
		file_uploader.attachEvent("onFileUpload", function (res) {
			that.file_id = res["value"];
		});
	}

	action(button) {
		var file_id = this.file_id;
		const file_uploader = this.fields.$$("file_uploader");
		if (file_uploader.files.count() === 0) {
			file_id = null;
		}
		const get_values = this.$$("form").getValues();
		get_values["file"] = file_id;
		const api = get_values["file"] ? "/enviar_por_correo_con_archivo/" : "/enviar_por_correo/";

		const that = this;
		const request_options = {
			url: master_url + "api/vnt/orden-venta/" + that.orden_venta_id + api,
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Orden de venta enviada",
			path: "/home/crm.quotation?id=" + that.quotation_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}