import {JetView} from "webix-jet";
import "webix/breadcrumb";

export default class QuotationPrintView extends JetView {
	config() {
		this.quotation_id = this.getParam("cotizacion_id");
		this.orden_venta_id = this.getParam("id");

		return {
			id: "pdf-view",
			rows: []
		};
	}

	ready() {
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "OV #" + this.orden_venta_id + " - Imprimir",
					link: "/home/crm.quotation.actions.orden-venta.print?id=" + this.quotation_id
				},
			],
		};

		const content = this.$$("pdf-view");
		content.addView({
			view: "pdfviewer",
			id: "pdf",
			toolbar: "toolbar",
			url: "binary->" + master_url + "api/vnt/orden-venta/" + this.orden_venta_id + "/imprimir/"
		}, 0);
		content.addView({
			view: "pdfbar", id: "toolbar"
		}, 0);
		content.addView({
			view: "toolbar", localId: "toolbar",
			paddingX: 10, height: 44, visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		}, 0);
	}
}
