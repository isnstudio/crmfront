import {JetView} from "webix-jet";
import "webix/breadcrumb";

export default class QuotationPrintView extends JetView {
	config() {
		this.quotation_id = this.getParam("id_cotizacion");
		this.orden_venta_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "OV #" + this.orden_venta_id + " - Imprimir",
					link: "/home/crm.quotation.actions.orden-venta.imprimir_orden_compra?id=" + this.quotation_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		return {
			id: "pdf-view",
			rows: [
				search,
				{view: "pdfbar", id: "toolbar"},
				{view: "pdfviewer", id: "pdf", toolbar: "toolbar"}
			]
		};
	}

	init() {
		this.quotation_id = this.getParam("id");
		this.$$("pdf").url_setter("binary->" + master_url + "api/vnt/orden-venta/" + this.orden_venta_id + "/imprimir_orden_compra/");
	}
}
