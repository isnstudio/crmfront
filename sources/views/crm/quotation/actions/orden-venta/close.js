import {JetView} from "webix-jet";
import SaveService from "../../../../../services/save";
import "webix/breadcrumb";

export default class FormsView extends JetView {
	config() {
		this.orden_venta_id = this.getParam("id");
		this.quotation_id = this.getParam("cotizacion_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de Venta", link: "/home/crm.quotation?id=" + this.orden_venta_id},
				{
					value: "#" + this.quotation_id,
					link: "/home/crm.quotation.actions.details?id=" + this.quotation_id
				},
				{
					value: "OV #" + this.orden_venta_id + " - Cerrar",
					link: "/home/crm.quotation.actions.close?id=" + this.orden_venta_id + "&cotizacion_id=" + this.quotation_id
				},
			],
		};

		const search = {
			view: "toolbar",
			ocalId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Estás seguro de cerrar la orden de venta?",
					type: "label",
					borderless: true,
					css: "question"
				},
			],
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Cerrar",
					css: "webix_danger",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.quotation?id=" + this.orden_venta_id);
					}
				},
			]
		};

		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								form,
								{
									view: "template",
									template: "Acciones",
									type: "header",
								},
								buttons,
							]
						},
					]
				}
			};
		}


		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
						]
					}
				]
			}
		};
	}

	action(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/vnt/orden-venta/" + this.orden_venta_id + "/cerrar/",
			get_values: {orden_venta: this.orden_venta_id},
			validate: that.$$("form").validate(),
			message: "Orden de venta cerrada",
			path: "/home/crm.quotation?id=" + that.orden_venta_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}