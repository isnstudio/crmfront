import {JetView} from "webix-jet";
import FormFieldsView from "../fields";
import SaveService from "../../../../services/save";
import "webix/breadcrumb";

export default class PersonView extends JetView {
	config() {
		this.quotation_id = this.getParam("id");
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation?id=" + this.quotation_id},
				{
					value: "#" + this.quotation_id + " - Editar",
					link: "/home/crm.quotation.actions.edit?id=" + this.quotation_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					height: 40,
					css: "webix_primary",
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.edit(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						window.history.back();
					}
				},
			]
		};

		this.fields = new FormFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			paddingX: 20,
			paddingY: 0,
			rows: [
				this.fields,
			],
			rules: {
				tipo_cambio: webix.rules.isNotEmpty,
				centro_beneficio: webix.rules.isNotEmpty,
				vendedor: webix.rules.isNotEmpty,
				almacen: webix.rules.isNotEmpty
			}
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						form,
					]
				}
			};

		return {

			type: "space",
			cols: [
				{
					rows: [
						search,
						{
							view: "scrollview",
							scroll: "y",
							body: form,
						}
					]
				},
				{
					rows: [
						{
							view: "template",
							template: "Acciones",
							type: "header",
							width: 384
						},
						buttons,
						{}
					]
				}
			]

		};
	}

	urlChange() {
		if (!isNaN(this.quotation_id) && this.quotation_id !== "") {
			if (this.getRoot()) {
				const that = this;
				webix.ajax().get(master_url + "api/vnt/cotizacion-venta/" + that.quotation_id + "/").then(function (response) {
					that.quotation = response.json();
					that.$$("form").setValues(that.quotation);
				});
			}
		}
	}

	ready() {
		const that = this;
		this.fields.$$("tipo_cambio").disable();
		this.fields.$$("plazo").disable();
		this.fields.$$("moneda").disable();
		this.fields.$$("es_prospecto").disable();
		this.fields.$$("es_prospecto").attachEvent("onChange", function (value) {
			if (value) {
				that.fields.$$("cliente").hide();
				that.fields.$$("es_proyecto").show();
				that.fields.$$("lead").show();
				that.fields.$$("correo").show();
				that.fields.$$("direccion").show();
				that.fields.$$("telefono").show();
				that.fields.$$("atencion").show();
				that.fields.$$("persona").hide();
				that.fields.$$("entrega_direccion_cliente").hide();
				//show
			} else {
				that.fields.$$("cliente").show();
				that.fields.$$("es_proyecto").hide();
				that.fields.$$("lead").hide();
				that.fields.$$("correo").hide();
				that.fields.$$("direccion").hide();
				that.fields.$$("telefono").hide();
				that.fields.$$("atencion").hide();
				that.fields.$$("persona").show();
				if (that.fields.$$("entrega").getValue() === "2")
					that.fields.$$("entrega_direccion_cliente").show();
				//hide
			}
		});

		this.fields.$$("cliente").attachEvent("onChange", function (value) {
			if (typeof value == "string") {
				const field = that.fields.$$("persona");
				field.define("url", master_url + "api/rel/persona/select_list/?cliente_id=" + value);
				field.refresh();

				const field_entrega = that.fields.$$("entrega_direccion_cliente");
				field_entrega.define("url", master_url + "api/cxc/cliente-direcciones/select_list/?es_entrega=1&cliente_id=" + value);
				field_entrega.refresh();
			}
		});

		this.fields.$$("es_proyecto").attachEvent("onChange", function (value) {
			const lead_field = that.fields.$$("lead");
			lead_field.define("url", master_url + "api/crm/lead/select_list/?available_for_quote=1&is_proyect=" + value);
		});

		this.fields.$$("lead").attachEvent("onChange", function (value) {
			webix.ajax().get(master_url + "api/vnt/cotizacion-venta/select_lead_data/", {lead_id: value}).then(function (response) {
				const cotizacion = response.json();
				that.$$("form").setValues(cotizacion, true);
			});
		});

		that.fields.$$("tipo_venta").attachEvent("onChange", function (value) {
			if (value === 2) {
				that.fields.$$("plazo").disable();
			} else if (value === 1) {
				that.fields.$$("plazo").enable();
			}
		});

		this.fields.$$("entrega").attachEvent("onChange", function (value) {
			if (value === "2") {
				const es_prospecto = that.fields.$$("es_prospecto").getValue();
				if (es_prospecto === 0) {
					that.fields.$$("entrega_direccion_cliente").show();
				}
			} else {
				that.fields.$$("entrega_direccion_cliente").hide();
			}
		});
	}

	edit(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/vnt/cotizacion-venta/" + this.quotation_id + "/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Cotización guardada",
			path: "/home/crm.quotation?id=" + that.quotation_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}