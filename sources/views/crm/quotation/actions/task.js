import {JetView} from "webix-jet";
import Errors from "../../../../helpers/errors";

export default class TaskDetailView extends JetView {
	config() {
		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Concluir",
					height: 40,
					css: "webix_primary",
					click: () => {
						const that = this;
						webix.ajax().put(master_url + "api/crm/task/" + this.task_id + "/conclude/").then(function (res) {
							webix.message("Tarea concluida");
							that.show("/home/crm.leads.actions.followup?id=" + that.lead_id + "&flush=true");
						}).fail(function (res) {
							const error = new Errors();
							error.show_error(res);
						});
					},
				},
				{
					view: "button",
					value: "No realizado",
					height: 40,
					css: "webix_danger",
					click: () => {
						const that = this;
						if (this.$$("cancel_form").validate()) {
							const get_values = this.$$("cancel_form").getValues();
							webix.ajax().put(master_url + "api/crm/task/" + this.task_id + "/cancel/", get_values).then(function (res) {
								webix.message("Tarea no realizada");
								that.show("/home/crm.leads.actions.followup?id=" + that.lead_id + "&flush=true");
							}).fail(function (res) {
								const error = new Errors();
								error.show_error(res);
							});
							this.$$("motivo").setValue("");
							this._parent.$$("description").setValue("");
							this._parent.$$("date").setValue("");
							this._parent.$$("reminder_time").setValue("");
							this._parent.$$("responsable").setValue("");
							this._parent.$$("activity").setValue("");
						} else {
							webix.message({
								text: "Faltan campos por llenar",
								type: "error",
								expire: -1,
								id: "messageLead"
							});
						}
					},
				},
				{
					view: "form",
					id: "cancel_form",
					margin: 10,
					elements: [
						{
							margin: 10,
							view: "textarea",
							name: "description",
							id: "motivo",
							label: "Motivo",
							labelPosition: "top",
							placeholder: "Comentarios",
							invalidMessage: "Motivo requerido",
							height: 60,
							required: true
						},
					],
					rules: {
						description: webix.rules.isNotEmpty,
					}
				}
			]
		};

		const details = {
			rows: [
				{
					view: "property",
					id: "sets",
					disabled: true,
					autoheight: true,
					elements: [
						{label: "Folio", type: "text", id: "pk"},
						{label: "Tipo", type: "text", id: "get_task_type_display"},
						{label: "Usuario", type: "text", id: "owner__display_webix"},
						{label: "Actividad", type: "text", id: "activity__display_webix"},
						{label: "Fecha límite", type: "text", id: "date"},
						{label: "Estatus", type: "text", id: "get_status_display"},
						{label: "Responsable", type: "text", id: "responsable__display_webix"},
						{label: "Descripción", type: "text", id: "description"},
						{ label: "Cliente", type: "text", id: "customer__display_webix" },
					]
				},
			]
		};

		return {
			css: "auto-height",
			autoheight: true,
			id: "detail_view",
			rows: [
				{
					view: "toolbar",
					localId: "detalle_header",
					paddingX: 10,
					height: 44,
					cols: [
						{
							view: "label",
							label: "Tarea - Detalle",
							localId: "label"
						}
					]
				},
				details,
				buttons
			]
		};
	}

	urlChange() {
		this.lead_id = this.getParam("id");
		this.task_id = this.getParam("task_id");
		if (this.task_id === undefined) {
			this.$$("detalle_header").hide();
			this.$$("sets").hide();
			this.$$("buttons").hide();
			this.RefreshDetails(undefined);
		}
		if (!isNaN(this.task_id)) {
			if (this.getRoot()) {
				this.$$("detalle_header").show();
				this.$$("sets").show();
				this.$$("buttons").show();
				this.RefreshDetails(this.task_id);
			}
		} else {
			this.$$("detalle_header").hide();
			this.$$("sets").hide();
			this.$$("buttons").hide();
			this.RefreshDetails(undefined);
		}
		if (!this.task_id) {
			this.$$("detalle_header").hide();
			this.$$("sets").hide();
			this.$$("buttons").hide();
		}
	}

	RefreshDetails(id) {
		const sets = this.$$("sets");
		if (id) {
			const content = this.$$("buttons");
			if (content) {
				let childs = content.getChildViews();
				let child_ids = [];
				for (let i = 0; i < childs.length; i++) {
					child_ids.push(childs[i].config.id);
				}
				for (let childIdsKey in child_ids) {
					content.removeView(child_ids[childIdsKey]);
				}
				var xhr = webix.ajax().sync().get(master_url + "api/crm/task/" + id + "/wdetail/");
				const json_response = JSON.parse(xhr.response);
				if ("actions" in json_response && "pk" in json_response) {
					for (const action_key in json_response["actions"]) {
						const action = json_response["actions"][action_key];
						content.addView({
							view: "button",
							label: action["label"],
							height: 40,
							click: () => {
								this.show("/home/crm.quotation.actions.task." + action["action"] + "?id=" + this.lead_id + "&task_id=" + json_response["pk"]);
							}
						}, 0);
					}
				}
				sets.setValues(json_response);
			}
		}
	}

	ready() {
		const buttons = this.$$("buttons");
		if (!this.app.Mobile) {
			this._parent.$$("history").attachEvent("onViewShow", function () {
				buttons.hide();
			});
			const that = this;
			this._parent.$$("form").attachEvent("onViewShow", function () {
				const id = that.getParam("task");
				if (id)
					buttons.show();
			});
		}
	}
}