import {JetView} from "webix-jet";

import TableView from "./table";
import ListView from "./responsive/table";
import Tables from "../../../helpers/tables";
import FilterListView from "jet-views/crm/quotation/sidebar/filters";
//import UnitsDetailCotizacionView from "jet-views/crm/quotation/sidebar/details_cotizacion";
import UnitsDetailFollowupView from "jet-views/crm/quotation/sidebar/details_followup";
//import UnitsDetailOrdenVentaView from "jet-views/crm/quotation/sidebar/details_orden_venta";
import "webix/breadcrumb";

export default class leadsView extends JetView {
	config() {
		this.Tables =  new Tables(this.app);
		this.Filters = new FilterListView(this.app);
		this.Table = new (this.app.Mobile ? ListView : TableView)(this.app);
		if (!this.app.Mobile) {
			//this.UnitsDetailView = new UnitsDetailCotizacionView(this.app);
			this.UnitsDetailFollowupView = new UnitsDetailFollowupView(this.app);
			//this.UnitsDetailOrdenVentaView = new UnitsDetailOrdenVentaView(this.app);
		}

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Cotizaciones de venta", link: "/home/crm.quotation"},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
				/*
				{
					view: "icon",
					icon: "mdi mdi-plus",
					localId: "addButton",
					css: "right_icon",
					click: () => {
						this.show("../crm.quotation.actions.add");
					}
				}
				*/
			]
		};

		if (this.app.Mobile)
			return {
				view: "accordion",
				rows: [
					this.Filters,
					search,
					this.Table,
					{$subview: true, popup: true}
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.Table,
					]
				},
				{
					width: 384,
					view: "accordion",
					rows: [
						this.Filters,
						//this.UnitsDetailView,
						//this.UnitsDetailOrdenVentaView,
						this.UnitsDetailFollowupView,
					]
				}
			]
		};
	}

	ready(view) {
		const datagrid = view.$scope.Table.$$("data");
		this.Tables.pagerParams(view, datagrid);
	}

	urlChange() {
		const url = master_url + "api/vnt/cotizacion-venta/wlist/";
		const table = this.Table.$$("data");
		const filters = this.Filters;
		const filter_default = "estatus==1,2;";
		this.Tables.dataLoading(table, url, this, filters, filter_default);
	}
}
