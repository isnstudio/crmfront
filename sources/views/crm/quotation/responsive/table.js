import {JetView} from "webix-jet";
import PagerView from "../../../common/pager";

export default class ListView extends JetView {
	config() {
		return {
			rows: [
				{
					view: "list",
					localId: "data",
					css: "multi-line-box",
					select: true,
					pager: "pager",
					type: {
						height: "auto",
						template: obj =>
							`
								<span style="float:right; text-transform: uppercase"><b>${obj.folio}</b></span>
								<span style="font-weight: 500;"><b>Fecha</b>: ${obj.fecha || ""}</span>
								<br>
								<span style="font-weight: 500;"><b>Asesor comercial</b>: ${obj.vendedor__display_webix || ""}</span>
								<br>
								<span style="font-weight: 500;"><b>Empresa</b>: ${obj.empresa || ""}</span>
								<br>
								<span style="font-weight: 500;"><b>Estatus</b>: ${obj.get_estatus_display || ""}</span>
								<br>
								<span style="font-weight: 500;"><b>Moneda</b>: ${obj.get_moneda_display || ""}</span>
								<br>
								<span style="font-weight: 500;"><b>Total</b>: ${"$ " + obj.total || ""}</span>
								<br>
								<span style="font-weight: 500;"><b>Atención a</b>: ${obj.atencion || ""}</span>
							`
					},
					on: {
						onItemClick: (id) => {
							this.show("/home/crm.quotation/crm.quotation.popups.details?id=" + id);
						}
					}
				},
				PagerView
			]
		};
	}
}
