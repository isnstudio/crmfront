import {JetView} from "webix-jet";

export default class UnitsDetailOrdenVentaView extends JetView {
	config() {
		const details = {
			rows: [
				{
					view: "property",
					id: "detail_info_orden_venta",
					disabled: true,
					autoheight: true,
					elements: [
						{label: "Folio", type: "text", id: "folio"},
						{label: "Estatus", type: "text", id: "get_estatus_display"},
					]
				},
				{
					id: "detail_components_orden_venta",
					css: "wbackground",
					padding: 20,
					rows: []
				},
				{}
			]
		};

		return {
			view: "accordionitem",
			id: "detalle_header_orden_venta",
			template: "Detalles - Orden de venta",
			header: "Detalles - Orden de venta",
			type: "header",
			collapsed: true,
			body: details,
		};
	}

	hideContents() {
		this.$$("detalle_header_orden_venta").hide();
		this.$$("detail_components_orden_venta").hide();
		this.$$("detail_info_orden_venta").hide();
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		this.$$("detalle_header_orden_venta").show();
		this.$$("detail_components_orden_venta").show();
		this.$$("detail_info_orden_venta").show();
		this.RefreshDetails(id);
	}

	showDetail(id) {
		if (id && id.constructor === Object && "id" in id) {
			id = id["id"];
		}
		this.orden_venta_id = id;
		if (!this.orden_venta_id)
			this.orden_venta_id = this.getParam("id");
		const content_orden_venta = this.$$("detail_components_orden_venta");
		if (content_orden_venta) {
			let childs = content_orden_venta.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				child_ids.push(child_id);
			}
			for (let childIdsKey in child_ids) {
				content_orden_venta.removeView(child_ids[childIdsKey]);
			}
		}

		if (!isNaN(id) && id !== "") {
			if (this.getRoot()) {
				this.showContents(id);
			}
		}
		if (!isNaN(this.orden_venta_id) && this.orden_venta_id !== "" && this.app.Mobile) {
			if (this.getRoot()) {
				this.showContents(this.orden_venta_id);
			}
		}
	}

	urlChange() {
		this.showDetail();
	}

	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(master_url + "api/vnt/orden-venta/cv_wdetails/?cotizacion_venta_id=" + id);
			if (xhr.status !== 400) {
				const orden_venta = JSON.parse(xhr.response);
				if ("actions" in orden_venta && "id" in orden_venta) {
					const content = this.$$("detail_components_orden_venta");
					if (content) {
						if (orden_venta["actions"].length) {
							content.addView({
								height: 40,
							}, 0);
							for (const action_key in orden_venta["actions"]) {
								const action = orden_venta["actions"][action_key];
								content.addView({
									view: "button",
									label: action["label"],
									height: 40,
									click: () => {
										this.show("/home/crm.quotation.actions.orden-venta." + action["action"] + "?id=" + orden_venta["id"] + "&cotizacion_id=" + this.orden_venta_id);
									}
								}, 0);
							}
							content.show();
						} else {
							content.hide();
						}
					}
				}
				if ("id" in orden_venta) {
					const sets = this.$$("detail_info_orden_venta");
					if (sets) {
						sets.setValues(orden_venta);
					}
				}
			} else {
				const content = this.$$("detail_components_orden_venta");
				let childs = content.getChildViews();
				let child_ids = [];
				for (let i = 0; i < childs.length; i++) {
					let child_id = childs[i].config.id;
					child_ids.push(child_id);
				}
				for (let childIdsKey in child_ids) {
					content.removeView(child_ids[childIdsKey]);
				}
				content.addView({
					template: "No hay información para mostrar",
					height: 40,
					borderless: true
				}, 0);
				this.$$("detail_info_orden_venta").hide();
			}
		}
	}
}

