import {JetView} from "webix-jet";

export default class UnitsDetailFollowupView extends JetView {
	config() {
		const details = {
			rows: [
				{
					localId: "detail_followup_components",
					padding: 20,
					rows: [
						{
							view: "timeline",
							localId: "traces_timeline",
							borderless: true,
							hidden: true,
						},
					]
				},
			]
		};

		return {
			view: "accordionitem",
			id: "detalle_followup_header",
			template: "Seguimiento",
			header: "Seguimiento",
			type: "header",
			collapsed: true,
			hidden: true,
			body: details,
		};
	}

	hideContents() {
		this.$$("detalle_followup_header").hide();
		this.$$("detail_followup_components").hide();
		this.$$("traces_timeline").hide();
		this.RefreshDetails(undefined);
	}

	showContents(id, folio) {
		this.$$("detalle_followup_header").show();
		this.$$("detail_followup_components").show();
		this.$$("traces_timeline").show();
		this.RefreshDetails(id, folio);
	}

	showDetail(id, folio) {
		if (id && id.constructor === Object && "id" in id) {
			id = id["id"];
		}
		this.quotation_id = id;
		if(!this.quotation_id)
			this.quotation_id = this.getParam("id");
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				if (!child_id.includes("$timeline")) {
					child_ids.push(child_id);
				}
			}
			for (let childIdsKey in child_ids) {
				content.removeView(child_ids[childIdsKey]);
			}
		}
		if (!isNaN(id) && id !== "") {
			if (this.getRoot()) {
				this.showContents(id, folio);
			}
		}
		if (!isNaN(this.quotation_id) && this.quotation_id !== "" && this.app.Mobile) {
			if (this.getRoot()) {
				this.showContents(this.quotation_id, folio);
			}
		}
	}

	urlChange() {
		this.showDetail();
	}

	RefreshDetails(id, folio) {
		const that = this;
		const content = that.$$("detail_followup_components");
		if (id) {
			var xhr = webix.ajax().sync().get(master_url + "api/vnt/cotizacion-venta/" + id + "/timeline/");
			const json_response = JSON.parse(xhr.response);

			const seguimiento = this.$$("seguimiento");
			if(!seguimiento) {
				addSeguimiento();
			} else {
				content.removeView("seguimiento");
				addSeguimiento();
			}
			const traces = this.$$("traces_timeline");
			if (traces) {
				if (json_response.length > 0) {
					traces.clearAll();
					traces.data_setter(json_response);
					traces.show();
				}
			}
		}
		function addSeguimiento() {
			content.addView({
				view: "button",
				label: "Seguimiento",
				id: "seguimiento",
				height: 40,
				click: () => {
					that.show("/home/crm.quotation.actions.followup?id=" + id + "&folio=" + folio);
				}
			}, 0);
		}
	}
}

