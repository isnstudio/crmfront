import {JetView} from "webix-jet";
import "webix/server_autocomplete";

export default class FilterListView extends JetView {
	config() {
		const select_text = "Clic para seleccionar";

		const form = {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			scroll: "y",
			paddingX: 20,
			paddingY: 10,
			rows: [
				{
					id: "cliente_id",
					view: "server_autocomplete",
					label: "Cliente",
					labelPosition: "top",
					name: "cliente_id",
					placeholder: "Cliente",
					url: master_url + "api/cxc/cliente/select_list_name/",
					select_text: select_text,
				},
				{
					id: "lead_id",
					view: "server_autocomplete",
					label: "Lead",
					labelPosition: "top",
					name: "lead_id",
					placeholder: "Lead",
					url: master_url + "api/crm/lead/select_list/",
					select_text: select_text,
				},
				{
					view: "text",
					name: "orden_compra",
					localId: "orden_compra",
					id: "orden_compra",
					label: "Orden compra",
					labelPosition: "top",
					placeholder: "Orden compra"
				},
				{
					view: "text",
					name: "folio",
					localId: "folio",
					id: "folio",
					label: "Folio",
					labelPosition: "top",
					placeholder: "Folio"
				},
				{
					view: "multicombo",
					name: "estatus",
					localId: "estatus",
					id: "estatus",
					label: "Estatus",
					labelPosition: "top",
					placeholder: "Estatus",
					options: master_url + "api/vnt/cotizacion-venta/choice_field/estatus/",
					value: "1,2"
				},
				{
					id: "sales_team",
					view: "server_autocomplete",
					label: "Sales team",
					labelPosition: "top",
					name: "sales_team_id",
					placeholder: "Sales Team",
					url: master_url + "api/crm/sales_team/select_list/",
					select_text: select_text,
				},
				{
					id: "vendedor",
					view: "server_autocomplete",
					label: "Asesor comercial",
					labelPosition: "top",
					name: "vendedor",
					placeholder: "Asesor comercial",
					url: master_url + "api/crm/user/select_list/?es_vendedor=true",
					select_text: select_text,
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon", icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						let querystring = "";
						const values = this.$$("filterform").getValues();
						if (Object.keys(values).length !== 0) {
							for (let k in values) {
								if (values[k]) {
									querystring += k + "==" + values[k] + ";";
								}
							}
						}

						let table = this._parent.Table.$$("data");
						let current_querystring = this._parent.getParam("filter");
						if (querystring != current_querystring) {
							table.clearAll();
						}
						this._parent.setParam("page", 0, false);
						this._parent.setParam("start", 0, false);
						this._parent.setParam("filter", querystring, true);
					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						let table = this._parent.Table.$$("data");
						this._parent.setParam("page", 0, false);
						this._parent.setParam("start", 0, false);
						this._parent.setParam("filter", "", true);
						this.$$("filter_form").clear();
						table.clearAll();
					}
				}
			]
		};

		return {
			view: "accordionitem",
			template: "Filtros",
			header: "Filtros",
			type: "header",
			id: "filters_header",
			collapsed: false,
			body: form,
		};
	}

	urlChange() {
        // Extract the filter parameters from the URL
        this.populateFiltersFromUrl();
    }

    populateFiltersFromUrl() {
        // Get the filter parameters from the URL
        const urlFilter = this.getParam("filter", true);

        // If there's a filter, parse it and update the form
        if (urlFilter) {
            const decodedFilter = decodeURIComponent(urlFilter);
            const filterParams = this.parseQueryString(decodedFilter);

            // Set the form values based on the parsed filter
            this.$$("filterform").setValues(filterParams);
        }
    }

    parseQueryString(queryString) {
        // Convert the filter query string into an object
        const params = {};
        const pairs = queryString.split(";");
        pairs.forEach(pair => {
            const [key, value] = pair.split("==");
            if (key && value) {
                params[key] = value;
            }
        });
        return params;
    }

}