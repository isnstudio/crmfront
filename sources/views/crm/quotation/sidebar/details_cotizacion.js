import {JetView} from "webix-jet";

export default class UnitsDetailCotizacionView extends JetView {
	config() {
		const details = {
			rows: [
				{
					view: "property",
					id: "detail_info_cotizacion",
					disabled: true,
					autoheight: true,
					nameWidth: 140,
					elements: [
						{label: "Centro de costo", type: "text", id: "centro_beneficio__display_webix"},
						{label: "Almacén", type: "text", id: "almacen__display_webix"},
						{label: "Orden de compra", type: "text", id: "orden_compra"},
						{label: "Atención a", type: "text", id: "atencion"},
						{label: "Proyecto venta", type: "text", id: "proyecto_venta"}
					]
				},
				{
					localId: "detail_components_cotizacion",
					css: "wbackground",
					padding: 20,
					rows: []
				},
				{}
			]
		};

		return {
			view: "accordionitem",
			id: "detalle_header_cotizacion",
			template: "Detalles - Cotización",
			header: "Detalles - Cotización",
			type: "header",
			collapsed: true,
			hidden: true,
			body: details,
		};
	}

	hideContents() {
		this.$$("detalle_header_cotizacion").hide();
		this.$$("detail_components_cotizacion").hide();
		this.$$("detail_info_cotizacion").hide();
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		this.$$("detalle_header_cotizacion").show();
		this.$$("detail_components_cotizacion").show();
		this.$$("detail_info_cotizacion").show();
		this.RefreshDetails(id);
	}

	showDetail(id) {
		if (id && id.constructor === Object && "id" in id) {
			id = id["id"];
		}
		this.quotation_id = id;
		if (!this.quotation_id)
			this.quotation_id = this.getParam("id");
		const content_cotizacion = this.$$("detail_components_cotizacion");
		if (content_cotizacion) {
			let childs = content_cotizacion.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				child_ids.push(child_id);
			}
			for (let childIdsKey in child_ids) {
				content_cotizacion.removeView(child_ids[childIdsKey]);
			}
		}
		if (!isNaN(id) && id !== "") {
			if (this.getRoot()) {
				this.showContents(id);
			}
		}
		if (!isNaN(this.quotation_id) && this.quotation_id !== "" && this.app.Mobile) {
			if (this.getRoot()) {
				this.showContents(this.quotation_id);
			}
		}
	}

	urlChange() {
		this.showDetail();
	}

	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(master_url + "api/vnt/cotizacion-venta/" + id + "/wdetails/");
			const cotizacion_venta = JSON.parse(xhr.response);
			if (cotizacion_venta) {
				if ("actions" in cotizacion_venta && "id" in cotizacion_venta) {
					const content = this.$$("detail_components_cotizacion");
					if (content) {
						if (cotizacion_venta["actions"].length) {
							content.addView({
								height: 20,
							}, 0);
							for (const action_key in cotizacion_venta["actions"]) {
								const action = cotizacion_venta["actions"][action_key];
								content.addView({
									view: "button",
									label: action["label"],
									height: 40,
									click: () => {
										this.show("/home/crm.quotation.actions." + action["action"] + "?id=" + cotizacion_venta["id"]);
									}
								}, 0);
							}
							this.$$("detail_components_cotizacion").show();
						} else {
							this.$$("detail_components").hide();
						}
					}
				}
				if ("id" in cotizacion_venta) {
					const sets = this.$$("detail_info_cotizacion");
					if (sets) {
						sets.setValues(cotizacion_venta);
					}
				}
			} else {
				this.$$("detalle_header_cotizacion").hide();
				this.$$("detail_components_cotizacion").hide();
				this.$$("detail_info_cotizacion").hide();
			}
		}
	}
}

