import ModalView from "views/sections/modal";
//import UnitsDetailView from "../sidebar/details_cotizacion";
//import UnitsDetailOrdenVentaView from "jet-views/crm/quotation/sidebar/details_orden_venta";
import UnitsDetailFollowupView from "jet-views/crm/quotation/sidebar/details_followup";

export default class UnitDetailsPopupView extends ModalView {
	constructor(app) {
		super(app, {
			header: {
				view: "toolbar", height: 56, padding: {left: 6, right: 14},
				cols: [
					{view: "icon", icon: "wxi-close", click: () => this.Hide()},
					{view: "label", label: "Detalles"},
				]
			},
			body: {
				view: "scrollview",
				body: {
					view: "accordion",
					rows: [
						//UnitsDetailView,
						//UnitsDetailOrdenVentaView,
						UnitsDetailFollowupView,
					]
				}
			}
		});
	}

	init() {
		super.init();
		this.on(this.app, "onDetailsRefresh", data => {
			this.$$("id").setValue(data.id);
		});
	}
}