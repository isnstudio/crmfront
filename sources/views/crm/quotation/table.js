import {JetView} from "webix-jet";
import PagerView from "../../common/pager";
import Tables from "../../../helpers/tables";

export default class TableView extends JetView {
	config() {
		this.Tables = new Tables(this.app);
		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "folio",
					header: "Folio",
					width: 80,
					template: function (obj) {
						return `<a href="${master_url.replace(/\/$/, '')}/vnt/cotizacion-venta/${obj.id}/" target="_blank">${obj.folio}</a>`;
					}
				},
				{
					id: "fecha",
					header: "Fecha",
					width: 110,
				},
				{
					id: "vendedor__display_webix",
					header: "Asesor comercial",
					width: 240,
				},
				{
					id: "empresa",
					header: "Empresa",
					css: "upper",
					width: 380,
				},
				{
					id: "get_estatus_display",
					header: {text: "Estatus", css: "right"},
					css: "right",
					width: 120,
				},
				{
					id: "get_moneda_display",
					header: "Moneda",
					width: 70,
				},
				{
					id: "total",
					header: {text: "Total", css: "right"},
					format: webix.i18n.priceFormat,
					css: "right",
					width: 120,
				},
				{
					id: "atencion",
					header: "Atención a",
					minWidth: 240,
					fillspace: true,
				},
				{
					id: "project__display_webix",
					header: { text: "Proyecto", css: "right" },
					css: "right",
					minWidth: 175,
					adjust: true,
				}
			],
			on: {
				onAfterRender() {
					this.$scope.Tables.afterRender(this, true);
				},
				onBeforeRender() {
					this.$scope.hideContents();
				},
				onAfterSelect: (id) => {
					let record = $$("grid").getItem(id);
					/*
					const details = [
						this._parent.UnitsDetailView,
						this._parent.UnitsDetailOrdenVentaView,
						//this._parent.UnitsDetailFollowupView
					];
					const header = this._parent.UnitsDetailView.$$("detalle_header_cotizacion");
					const header_collapse = [
						this._parent.Filters.$$("filters_header"),
						this._parent.UnitsDetailFollowupView.$$("detalle_followup_header"),
						this._parent.UnitsDetailOrdenVentaView.$$("detalle_header_orden_venta")
					];
					this.Tables.afterSelect(details, header,header_collapse, id);
 					*/
					this._parent.UnitsDetailFollowupView.showDetail(id, record.folio);
					this._parent.UnitsDetailFollowupView.$$("detalle_followup_header").expand();
					this.setParam("id", id, true);
				}
			}
		};


		return {
			rows: [
				{
					view: "scrollview",
					scroll: "y",
					body: {
						rows: [
							datatable,
						]
					}
				},
				PagerView
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}


	urlChange() {
		this.hideContents();
	}

	hideContents() {
		const details = [
			//this._parent.UnitsDetailView,
			//this._parent.UnitsDetailOrdenVentaView,
			this._parent.UnitsDetailFollowupView
		];
		const datatable = this.$$("data");
		this.Tables.hideContents(this, datatable, details);
	}
}