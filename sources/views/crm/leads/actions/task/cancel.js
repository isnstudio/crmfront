import {JetView} from "webix-jet";
import "webix/breadcrumb";
import SaveService from "../../../../../services/save";

export default class TaskConcludeView extends JetView {
	config() {
		const required_text = "Requerido";
		this.lead_id = this.getParam("id");
		this.task_id = this.getParam("task_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Leads", link: "/home/crm.leads?id=" + this.lead_id},
				{
					value: "#" + this.lead_id + " - Seguimiento",
					link: "/home/crm.leads.actions.followup?id=" + this.lead_id + "&task_id=" + this.task_id
				},
				{
					value: "Tarea #" + this.task_id + " - Cancelar",
					link: "/home/crm.leads.actions.task.cancel?id=" + this.lead_id + "&task_id=" + this.task_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const cancel_motive = {
			view: "textarea",
			name: "cancel_motive",
			label: "Motivo",
			labelPosition: "top",
			placeholder: "Motivo",
			invalidMessage: required_text,
			height: 150,
			required: true,
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Estás seguro de marcar la tarea como no realizada?",
					type: "label",
					borderless: true,
					css: "question"
				},
				cancel_motive
			],
			rules: {
				cancel_motive: webix.rules.isNotEmpty,
			}
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					css: "webix_primary",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.leads.actions.followup?id=" + this.lead_id + "&task_id=" + this.task_id);
					}
				},
			]
		};

		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}

	action(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/task/" + this.task_id + "/cancel/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Tarea no realizada",
			path: "/home/crm.leads.actions.followup?id=" + that.lead_id + "&task_id=" + that.task_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}
