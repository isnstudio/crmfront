import {JetView} from "webix-jet";
import FormFieldsView from "../fields";
import SaveService from "../../../../services/save";
import "webix/breadcrumb";

export default class PersonView extends JetView {
	config() {
		const that = this;
		this.lead_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Leads", link: "/home/crm.leads?id=" + this.lead_id},
				{
					value: "#" + this.lead_id + " - Editar",
					link: "/home/crm.leads.actions.edit?id=" + this.lead_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					height: 40,
					css: "webix_primary",
					id: "save",
					click: function() {
						var button = this;
						that.edit(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.leads?id=" + this.lead_id);
					}
				},
			]
		};

		this.Fields = new FormFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			paddingX: 24,
			paddingY: 10,
			rows: [
				this.Fields,
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						form,
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}

	urlChange() {
		const that = this;
		webix.ajax().get(master_url + "api/crm/lead/" + that.lead_id + "/").then(function (response) {
			that.lead = response.json();
			that.$$("form").setValues(that.lead);
			that.edited = true
			if (that.lead.is_project) {
				that.$$("form").elements["is_project"].setValue(1);
				that.Fields.$$("is_project").disable()
				if (that.lead.is_project) {
					that.Fields.$$("adviser").disable()
					that.Fields.$$("business_name").disable()
				} else {
					that.Fields.$$("company").disable();
					that.Fields.$$("contact_1_name").disable();
					that.Fields.$$("contact_1_email").disable();
					that.Fields.$$("contact_1_phone").disable();
					that.Fields.$$("address").disable();
				}
			}
		});
	}

	ready() {
		const that = this;
		that.$$("form").elements["is_project"].attachEvent("onChange", function (value) {
			if (value === 1 || value) {
				that.Fields.$$("customer").show();
				that.Fields.$$("project").show();
				that.Fields.$$("company").disable();
				that.Fields.$$("rfc").disable();
				that.Fields.$$("business_name").disable();
			} else if (value === 0 || !value) {
				that.Fields.$$("customer").hide();
				that.Fields.$$("project").hide();
				that.Fields.$$("company").enable();
				that.Fields.$$("rfc").enable();
				that.Fields.$$("business_name").enable();
			}
		});

		this.Fields.$$("customer").attachEvent("onChange", function (value) {
			if (value && value.constructor !== Object)
				webix.ajax().get(master_url + "api/cxc/cliente/" + value + "/data/").then(function (response) {
					const lead = response.json();
					const city = that.Fields.$$("city");
					if (that.edited){
						delete lead.adviser
						delete lead.comments
					}
					that.$$("form").setValues(lead, true);
				});
		});

		this.Fields.$$("input_tentative_closing_date").disable();
		this.Fields.$$("input_probability_closure").disable();
	}

	edit(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/lead/" + that.lead_id + "/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Lead guardado",
			path: "/home/crm.leads?id=" + that.lead_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}