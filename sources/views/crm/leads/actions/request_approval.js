import {JetView} from "webix-jet";
import "webix/breadcrumb";
import RequestService from "../../../../services/request";

export default class FormsView extends JetView {
	config() {
		this.lead_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Leads", link: "/home/crm.leads?id=" + this.lead_id},
				{
					value: "#" + this.lead_id + " - Solicitar aprobación",
					link: "/home/crm.leads.actions.request_approval?id=" + this.lead_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [
				{
					template: "¿Estás seguro de solicitar una aprobación?",
					type: "label",
					borderless: true,
					css: "question"
				},
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Solicitar Aprobación",
					height: 40,
					css: "webix_primary",
					id: "request",
					click: () => {
						const button = this.$$("request");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.leads?id=" + this.lead_id);
					}
				},
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form
							]
						}
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
						]
					}
				]
			}
		};
	}

	action(button) {
		const that = this;
		const request_options = {
			ajax: webix.ajax().put(master_url + "api/crm/lead/" + that.lead_id + "/request_approval/"),
			message: "Aprobación solicitada",
			path: "/home/crm.leads?id=" + that.lead_id,
			button: button,
			context: that,
		};
		this.RequestService = new RequestService(this.app);
		this.RequestService.request(request_options);
	}
}
