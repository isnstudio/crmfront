import {JetView} from "webix-jet";
import "webix/breadcrumb";
import SaveService from "../../../../services/save";

export default class FormsView extends JetView {
	config() {
		const required_text = "Requerido";
		this.lead_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Leads", link: "/home/crm.leads?id=" + this.lead_id},
				{
					value: "#" + this.lead_id + " - Rechazar",
					link: "/home/crm.leads.actions.reject?id=" + this.lead_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const reject_motive = {
			view: "textarea",
			name: "reject_motive",
			label: "Motivo",
			labelPosition: "top",
			placeholder: "Motivo",
			invalidMessage: required_text,
			height: 150,
			required: true,
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Estás seguro de rechazar la aprobación?",
					type: "label",
					borderless: true,
					css: "question"
				},
				reject_motive
			],
			rules: {
				reject_motive: webix.rules.isNotEmpty,
			}
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Rechazar",
					css: "webix_danger",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.leads?id=" + this.lead_id);
					}
				},
			]
		};


		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}

	action(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/lead/" + that.lead_id + "/reject/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Lead rechazado",
			path: "/home/crm.leads?id=" + that.lead_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}
