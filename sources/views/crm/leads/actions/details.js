import {JetView, plugins} from "webix-jet";
import FormFieldsView from "../fields";
import "webix/breadcrumb";

export default class PersonView extends JetView {
	config() {
		this.lead_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{
					value: "Leads",
					link: "/home/crm.leads?id=" + this.lead_id
				},
				{
					value: "#" + this.lead_id + " - Detalles",
					link: "/home/crm.leads.actions.details?id=" + this.lead_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Editar",
					css: "webix_primary",
					height: 40,
					click: () => {
						this.show("/home/crm.leads.actions.edit?id=" + this.lead_id);
					}
				},
				{
					view: "button",
					value: "Regresar",
					height: 40,
					click: () => {
						this.show("/home/crm.leads?flush=true");
					}
				}
			]
		};

		const fields = FormFieldsView;

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			paddingX: 24,
			rows: [
				fields
			],
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
								template: function (obj) {
									return "";
								}
							}
						]
					}
				]
			}
		};
	}

	init() {
		this.use(plugins.UrlParam, ["id"]);
	}

	urlChange() {
		this.id_lead = "";
		if ("id" in this._data) {
			this.id_lead = this._data.id;
		}
		const form = this.$$("form");
		for (var element in form.elements) {
			form.elements[element].disable();
		}
		if (!isNaN(this.id_lead) && this.id_lead !== "") {
			if (this.getRoot()) {
				const that = this;
				webix.ajax().get(master_url + "api/crm/lead/" + that.id_lead + "/").then(function (response) {
					that.lead = response.json();
					that.$$("form").setValues(that.lead);
					if (that.lead.is_project) {
						$$("form").elements["is_project"].setValue(1);
					}

					const content = that.$$("buttons");
					if (content) {
						for (const action_key in that.articulo["actions"]) {
							const action = that.articulo["actions"][action_key];
							let url = "/home/crm.leads.actions." + action["action"] + "/" + that.id_lead;
							content.addView({
								view: "button",
								label: action["label"],
								height: 40,
								click: () => that.show(url)
							}, 0);
						}
					}
				});
			}
		}
	}

	ready() {
		$$("form").elements["is_project"].attachEvent("onChange", function (value) {
			if (value == 1) {
				$$("customer").show();
				$$("project").show();
			} else if (value == 0) {
				$$("customer").hide();
				$$("project").hide();
			}
		});
	}
}