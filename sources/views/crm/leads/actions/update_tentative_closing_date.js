import {JetView} from "webix-jet";
import "webix/breadcrumb";
import RequestService from "../../../../services/request";

export default class UpdateTentativeclosingDateView extends JetView {
	config() {
		this.lead_id = this.getParam("id");
		const dateFormat = webix.Date.dateToStr("%Y-%m-%d");
		const select_text = "Clic para seleccionar";

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Leads", link: "/home/crm.leads?id=" + this.lead_id},
				{
					value: "#" + this.lead_id + " - Editar fecha tentativa de cierre"
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [
				{
					cols: [
						{
							view: "datepicker",
							name: "tentative_closing_date",
							id: "input_tentative_closing_date",
							label: "Fecha tentativa de cierre",
							labelPosition: "top",
							placeholder: select_text,
							format: dateFormat,
							required: true
						},
						{}
					]
				},
				{
					view: "textarea",
					id: "description",
					name: "description",
					label: "Motivo del cambio",
					placeholder: "Motivo del cambio",
					labelPosition: "top",
					height: 150,
					required: true
				}
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					height: 40,
					css: "webix_primary",
					id: "request",
					click: () => {
						const button = this.$$("request");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.leads?id=" + this.lead_id);
					}
				},
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form
							]
						}
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
						]
					}
				]
			}
		};
	}

	action(button) {
		const that = this;
		if (that.$$("form").validate()){
			const request_options = {
				ajax: webix.ajax().put(master_url + "api/crm/lead/" + that.lead_id + "/update_tentative_closing_date/", that.$$("form").getValues()),
				message: "Fecha tentativa de cierre actualizada",
				path: "/home/crm.leads?id=" + that.lead_id,
				button: button,
				context: that,
			};
			this.RequestService = new RequestService(this.app);
			this.RequestService.request(request_options);
		}
	}
}
