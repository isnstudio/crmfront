import ModalView from "views/sections/modal";
import TaskDetailView from "./task";

export default class ActivityPopupView extends ModalView {
	constructor(app) {
		super(app, {
			header: {
				view: "toolbar", height: 56, padding: {left: 6, right: 14},
				cols: [
					{view: "icon", icon: "wxi-close", click: () => this.Hide()},
					{view: "label", id: "folio", css: "details_popup_header_label", label: "Actividad"},
				]
			},
			body: TaskDetailView
		});
	}

	init() {
		super.init();
	}
}