import {JetView} from "webix-jet";
import "webix/breadcrumb";
import "webix/server_autocomplete";
import RequestService from "../../../../services/request";

export default class IncreaseProbabilityView extends JetView {
	config() {
		this.lead_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Leads", link: "/home/crm.leads?id=" + this.lead_id},
				{
					value: "#" + this.lead_id + " - Aumentar probabilidad",
					link: "/home/crm.leads.actions.increase_probability?id=" + this.lead_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [
				{
					view: "server_autocomplete",
					id: "probability_closure",
					name: "probability_closure",
					label: "Probabilidad",
					placeholder: "Probabilidad",
					url: master_url + "api/crm/lead/choice_field/probability_closure/",
					required: true
				},
				{
					view: "textarea",
					id: "description",
					name: "description",
					label: "Descripción",
					placeholder: "Descripción",
					labelPosition: "top",
					height: 150,
					required: true
				}
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					height: 40,
					css: "webix_primary",
					id: "request",
					click: () => {
						const button = this.$$("request");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.leads?id=" + this.lead_id);
					}
				},
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form
							]
						}
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons,
						]
					}
				]
			}
		};
	}

	action(button) {
		const that = this;
		if (that.$$("form").validate()){
			const request_options = {
				ajax: webix.ajax().put(master_url + "api/crm/lead/" + that.lead_id + "/increase_probability/", that.$$("form").getValues()),
				message: "Probabilidad actualizada",
				path: "/home/crm.leads?id=" + that.lead_id,
				button: button,
				context: that,
			};
			this.RequestService = new RequestService(this.app);
			this.RequestService.request(request_options);
		}
	}
}
