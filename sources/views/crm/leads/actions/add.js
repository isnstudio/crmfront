import {JetView} from "webix-jet";
import FormFieldsView from "../fields";
import "webix/breadcrumb";
import SaveService from "../../../../services/save";

export default class PersonView extends JetView {
	config() {
		const that = this;
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Leads", link: "/home/crm.leads"},
				{value: "Agregar", link: "/home/crm.leads.actions.add"},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Agregar",
					height: 40,
					css: "webix_primary",
					id: "save",
					click: function() {
						var button = this;
						that.create(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						window.history.back();
					}
				},
			]
		};

		this.Fields = new FormFieldsView(this.app);

		this.form = {
			view: "form",
			localId: "form",
			id: "form",
			paddingX: 20,
			rows: [
				this.Fields,
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						this.form,
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							this.form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}



	ready() {
		const that = this;
		webix.ajax().get(master_url + "api/crm/lead/init/").then(function (response) {
			const json_response = response.json();
			that.$$("form").setValues(json_response, true);
		});

		that.$$("form").elements["is_project"].attachEvent("onChange", function (value) {
			if (value === 1) {
				that.Fields.$$("customer").show();
				that.Fields.$$("project").show();
				that.Fields.$$("company").disable();
				that.Fields.$$("rfc").disable();
				that.Fields.$$("business_name").disable();
			} else if (value === 0) {
				that.Fields.$$("customer").hide();
				that.Fields.$$("project").hide();
				that.Fields.$$("company").enable();
				that.Fields.$$("rfc").enable();
				that.Fields.$$("business_name").enable();
			}
		});

		this.Fields.$$("customer").attachEvent("onChange", function (value) {
			if (value && value.constructor !== Object)
				webix.ajax().get(master_url + "api/cxc/cliente/" + value + "/data/").then(function (response) {
					const lead = response.json();
					const city = that.Fields.$$("city");
					that.$$("form").setValues(lead, true);
					city.define("url", master_url + "api/cat/ciudad/select_list/?estado_id=" + lead.estado_id);
					city.refresh();
				});
		});
	}

	create(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/lead/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Lead guardado",
			path: "/home/crm.leads/",
			button: button,
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}
