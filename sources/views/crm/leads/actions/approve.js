import {JetView} from "webix-jet";
import RequestService from "../../../../services/request";
import "webix/breadcrumb";

export default class LeadApproveView extends JetView {

	config() {
		this.lead_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Leads", link: "/home/crm.leads"},
				{
					value: "#" + this.lead_id + " - Aprobar",
					link: "/home/crm.leads.actions.approve?id=" + this.lead_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 24,
			rows: [
				{
					template: "¿Estás seguro de aprobar el lead?",
					type: "label",
					borderless: true,
					css: "question"
				},
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Aprobar",
					height: 40,
					css: "webix_primary",
					id: "request",
					click: () => {
						const button = this.$$("request");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.leads");
					}
				},
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
							]
						}
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}

	action(button) {
		const that = this;
		const request_options = {
			ajax: webix.ajax().put(master_url + "api/crm/lead/" + that.lead_id + "/approve/"),
			message: "Lead aprobado",
			path: "/home/crm.leads?id=" + that.lead_id,
			button: button,
			context: that,
		};
		this.RequestService = new RequestService(this.app);
		this.RequestService.request(request_options);
	}
}
