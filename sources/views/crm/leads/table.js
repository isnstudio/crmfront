import {JetView} from "webix-jet";
import PagerView from "../../common/pager";
import Tables from "../../../helpers/tables";

export default class TableView extends JetView {
	config() {
		this.Tables = new Tables(this.app);
		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "id",
					header: "Folio",
					sort: "string",
					width: 80,
				},
				{
					id: "rfc",
					header: "RFC",
					sort: "string",
					width: 150,
				},
				{
					id: "adviser__display_webix",
					header: "Asesor comercial",
					width: 240,
				},
				{
					id: "company",
					header: {text: "Empresa"},
					css: "upper",
					fillspace: true,
					minWidth: 380,
				},
				{
					id: "get_status_display",
					header: "Estatus del lead",
					sort: "string",
					width: 135
				},
				{
					id: "last_activity",
					header: "Ultima Act.",
					width: 175
				},
				{
					id: "last_date",
					header: {text: "Fecha Ultima Act.", css: "right"},
					width: 175,
					css: "right"
				},
				{
					id: "get_sales_potential_display",
					header: {text: "Potencial de venta", css: "right"},
					css: "right",
					width: 175
				},
				{
					id: "tentative_closing_date",
					header: { text: "Fecha Cierre", css: "right" },
					width: 175,
					css: "right"
				},
				{
					id: "project__display_webix",
					header: { text: "Proyecto", css: "right" },
					css: "right",
					width: 175
				},
				{
					id: "get_probability_closure_display",
					header: { text: "Probabilidad de cierre", css: "right" },
					width: 200,
					css: "right"
				},
			],
			on: {
				onAfterRender() {
					this.$scope.Tables.afterRender(this, true);
				},
				onAfterSelect: (id) => {
					const details = [this._parent.UnitsDetailView];
					const header = this._parent.UnitsDetailView.$$("detalle_header");
					const headers_collapse = [this._parent.Filters.$$("form_header"), this._parent.Actions.$$("actions_header")];
					this.Tables.afterSelect(details, header, headers_collapse, id);
					this.setParam("id", id, true);
				},
			}
		};


		return {
			rows: [
				{
					view: "scrollview",
					scroll: "y",
					body: {
						rows: [
							datatable,
						]
					}
				},
				PagerView
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}


	urlChange() {
		const details = [
			this._parent.UnitsDetailView
		];
		const datatable = this.$$("data");
		this.Tables.hideContents(this, datatable, details);
	}
}