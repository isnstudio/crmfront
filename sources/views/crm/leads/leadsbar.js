import {JetView} from "webix-jet";

export default class leadBarView extends JetView {
	config() {
		return {
			view: "toolbar",
			elements: [
				{
					view: "button",
					value: "add",
					click: () => {
						this.app.show("crm.leads.actions.add");
					},
				},
			],
		};
	}
}
