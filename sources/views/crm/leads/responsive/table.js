import {JetView} from "webix-jet";
import PagerView from "../../../common/pager";

export default class ListView extends JetView {
	config() {
		return {
			rows: [
				{
					view: "list",
					localId: "data",
					css: "multi-line-box",
					select: true,
					pager: "pager",
					type: {
						height: "auto",
						template: obj =>
							`
								<span style="float:right; text-transform: uppercase"><b>${obj.id}</b></span>
								<span style="font-weight: 500;"><b>RFC</b>: ${obj.rfc}</span>
								<br>
								<span style="font-weight: 500;"><b>Asesor comercial</b>: ${obj.adviser__display_webix}</span>
								<br>
								<span style="font-weight: 500;"><b>Empresa</b>:${obj.company}</span>
								<br>
								<span style="font-weight: 500;"><b>Estatus del lead</b>: ${obj.get_status_display}</span>
								<br>
								<span style="font-weight: 500;"><b>Ultima actividad</b>: ${obj.last_activity}</span>
								<br>
								<span style="font-weight: 500;"><b>Fecha de ultima actividad</b>: ${obj.last_date}</span>
								<br>
								<span style="font-weight: 500;"><b>Potencial de venta</b>: ${obj.get_sales_potential_display || ''}</span>
							`
					},
					on: {
						onItemClick: (id) => {
							this.show("/home/crm.leads/crm.leads.popups.details?id=" + id);
						}
					}
				},
				PagerView
			]
		};
	}
}