import {JetView,} from "webix-jet";
import "webix/server_autocomplete";

export default class FormFieldsView extends JetView {
	config() {
		const dateFormat = webix.Date.dateToStr("%Y-%m-%d");
		const select_text = "Clic para seleccionar";
		const required_text = "Requerido";
		const phone_mask = {mask: "###-###-####", allow: /[0-9]/g};

		const contact_type = {
			view: "combo",
			name: "contact_type",
			localId: "contact_type:combo",
			id: "contact_type",
			label: "Tipo de cliente",
			labelPosition: "top",
			placeholder: select_text,
			invalidMessage: required_text,
			suggest: {url: master_url + "api/crm/lead/choice_field/contact_type/"},
			required: true,
		};
		const customer = {
			view: "server_autocomplete",
			id: "customer",
			name: "customer",
			label: "Cliente",
			placeholder: "Cliente",
			url: master_url + "api/cxc/cliente/select_list/",
			required: true,
			hidden: true
		};
		const company = {
			view: "text",
			name: "company",
			id: "company",
			label: "Nombre de empresa",
			labelPosition: "top",
			placeholder: "Nombre de empresa",
			invalidMessage: required_text,
			tooltip: "El nombre de la empresa es " + "#value#",
			css: "uppercase-input",
			required: true
		};
		const rfc = {
			view: "text",
			name: "rfc",
			label: "RFC",
			id: "rfc",
			labelPosition: "top",
			placeholder: "RFC",
			invalidMessage: required_text,
			tooltip: "El RFC de la empresa es " + "#value#",
			css: "uppercase-input",
			required: true,
			attributes: {maxlength: 13, minlength: 13}
		};
		const business_name = {
			view: "text",
			name: "business_name",
			id: "business_name",
			label: "Razón social",
			labelPosition: "top",
			placeholder: "Razón social",
			invalidMessage: required_text,
			tooltip: "La razón social de la empresa es " + "#value#",
			css: "uppercase-input",
			required: true,
		};
		const address = {
			view: "text",
			id: "address",
			name: "address",
			label: "Dirección",
			labelPosition: "top",
			placeholder: "Dirección",
			invalidMessage: required_text,
			tooltip: "la dirección es " + "#value#",
			css: "uppercase-input",
			required: true,
		};
		const city = {
			view: "server_autocomplete",
			id: "city",
			name: "city",
			label: "Ciudad",
			placeholder: "Ciudad",
			url: master_url + "api/cat/ciudad/select_list/",
			required: true
		};

		const is_project = {
			view: "checkbox",
			id: "is_project",
			name: "is_project",
			label: "¿Es un proyecto?",
			labelPosition: "top"
		};

		const project = {
			view: "text",
			id: "project",
			name: "project",
			label: "Proyecto",
			labelPosition: "top",
			placeholder: "Proyecto",
			tooltip: "El nombre del proyecto es " + "#value#",
			hidden: true
		};
		const line_business = {
			view: "server_autocomplete",
			id: "line_business",
			name: "line_business",
			label: "Giro",
			placeholder: select_text,
			url: master_url + "api/crm/line_business/select_list/",
			required: true
		};
		const industry = {
			view: "server_autocomplete",
			id: "industry",
			name: "industry",
			label: "Industria",
			placeholder: select_text,
			url: master_url + "api/crm/industry/select_list/",
			required: true
		};
		const contact_1_name = {
			view: "text",
			id: "contact_1_name",
			name: "contact_1_name",
			label: "Nombre de contacto",
			labelPosition: "top",
			placeholder: "Nombre de contacto",
			invalidMessage: required_text,
			tooltip: "El nombre del contacto es " + "#value#",
			css: "uppercase-input",
			required: true,
		};
		const contact_1_phone = {
			view: "text",
			id: "contact_1_phone",
			name: "contact_1_phone",
			label: "Teléfono",
			labelPosition: "top",
			placeholder: "Ingrese numero de 10 digitos",
			invalidMessage: required_text,
			tooltip: "El numero de teléfono es " + "#value#",
			required: true,
			pattern: phone_mask
		};
		const contact_1_position = {
			view: "server_autocomplete",
			id: "contact_1_position",
			name: "contact_1_position",
			label: "Posición",
			placeholder: select_text,
			url: master_url + "api/crm/position/select_list/",
		};

		const contact_1_email = {
			view: "text",
			id: "contact_1_email",
			name: "contact_1_email",
			label: "Correo electrónico",
			labelPosition: "top",
			invalidMessage: "Se requiere un correo electrónico válido",
			placeholder: "Correo electrónico",
			required: true,
		};

		const contact_2_name = {
			view: "text",
			name: "contact_2_name",
			label: "Nombre de contacto",
			labelPosition: "top",
			placeholder: "Nombre de contacto",
			invalidMessage: required_text,
			tooltip: "El nombre del contacto es " + "#value#",
			css: "uppercase-input",
		};
		const contact_2_phone = {
			view: "text",
			name: "contact_2_phone",
			label: "Teléfono",
			labelPosition: "top",
			placeholder: "Ingrese numero de 10 digitos",
			tooltip: "El numero de teléfono es " + "#value#",
			pattern: phone_mask,
			required: false,
			validate: function (val) {
				return true;
			}
		};
		const contact_2_position = {
			view: "server_autocomplete",
			id: "contact_2_position",
			name: "contact_2_position",
			label: "Posición",
			placeholder: select_text,
			url: master_url + "api/crm/position/select_list/",
		};
		const contact_2_email = {
			view: "text",
			name: "contact_2_email",
			label: "Correo electrónico",
			labelPosition: "top",
			invalidMessage: "Se requiere un correo electrónico válido",
			placeholder: "Correo electrónico",
		};

		const contact_3_name = {
			view: "text",
			name: "contact_3_name",
			label: "Nombre de contacto",
			labelPosition: "top",
			placeholder: "Nombre de contacto",
			invalidMessage: required_text,
			tooltip: "El nombre del contacto es " + "#value#",
			css: "uppercase-input",
		};
		const contact_3_phone = {
			view: "text",
			name: "contact_3_phone",
			label: "Teléfono",
			labelPosition: "top",
			placeholder: "Ingrese numero de 10 digitos",
			invalidMessage: required_text,
			tooltip: "El numero de teléfono es " + "#value#",
			pattern: phone_mask,
			required: false,
			validate: function (val) {
				return true;
			}
		};
		const contact_3_position = {
			view: "server_autocomplete",
			id: "contact_3_position",
			name: "contact_3_position",
			label: "Posición",
			placeholder: select_text,
			url: master_url + "api/crm/position/select_list/",
		};
		const contact_3_email = {
			view: "text",
			name: "contact_3_email",
			label: "Correo electrónico",
			labelPosition: "top",
			invalidMessage: "Se requiere un correo electrónico válido",
			placeholder: "Correo electrónico",
		};

		const birthday_date = {
			view: "datepicker",
			name: "birthday_date",
			label: "Fecha de cumpleaños",
			labelPosition: "top",
			placeholder: select_text,
			format: dateFormat,
		};

		const facebook = {
			view: "text",
			name: "facebook",
			label: "Facebook",
			labelPosition: "top",
			placeholder: "Link de Facebook"
		};
		const linkedin = {
			view: "text",
			name: "linkedin",
			label: "LinkedIn",
			labelPosition: "top",
			placeholder: "Link de LinkedIn"
		};
		const important_date = {
			view: "datepicker",
			name: "important_date",
			id: "input_important_date",
			label: "Fecha importante",
			labelPosition: "top",
			placeholder: select_text,
			format: dateFormat
		};
		const important_date_reason = {
			view: "text",
			name: "important_date_reason",
			label: "Motivo de fecha importante",
			labelPosition: "top",
			placeholder: "Motivo de fecha importante"
		};
		const sales_potential = {
			view: "combo",
			name: "sales_potential",
			localId: "sales_potential:combo",
			label: "Potencial de venta",
			labelPosition: "top",
			placeholder: select_text,
			suggest: {url: master_url + "api/crm/lead/choice_field/sales_potential/"}
		};
		const tentative_closing_date = {
			view: "datepicker",
			name: "tentative_closing_date",
			id: "input_tentative_closing_date",
			label: "Fecha tentativa de cierre",
			labelPosition: "top",
			placeholder: select_text,
			format: dateFormat
		};
		const adviser = {
			view: "server_autocomplete",
			id: "adviser",
			name: "adviser",
			label: "Asesor",
			placeholder: select_text,
			url: master_url + "api/cat/user/select_list/?por_jerarquia=1&activo=1",
			required: true
		};
		const sales_team = {
			view: "text",
			id: "sales_team",
			labelPosition: "top",
			name: "sales_team__display_webix",
			label: "Equipo de venta",
			hidden: true
		};
		const comments = {
			view: "textarea",
			name: "comments",
			label: "Comentarios",
			labelPosition: "top",
			placeholder: "Comentarios",
			invalidMessage: required_text,
			height: 150,
			required: true,
		};
		const probability_closure = {
			view: "server_autocomplete",
			id: "input_probability_closure",
			name: "probability_closure",
			label: "Probabilidad de cierre",
			placeholder: "Probabilidad de cierre",
			url: master_url + "api/crm/lead/choice_field/probability_closure/",
			required: true
		};
		const proveedor_estrategico = {
			view: "server_autocomplete",
			id: "input_proveedor_estrategico",
			name: "proveedor_estrategico",
			label: "Proveedor estratégico",
			placeholder: "Proveedor estratégico",
			url: master_url + "api/cxp/proveedor/select_list/",
			required: false
		};
		const contact_1_phone_extension = {
			view: "text",
			name: "contact_1_phone_extension",
			label: "Extensión de teléfono",
			labelPosition: "top",
			placeholder: "Extensión de teléfono",
			invalidMessage: required_text,
			tooltip: "Extensión de teléfono" + "#value#",
			pattern: phone_mask,
			required: false,
			validate: function (val) {
				return true;
			}
		};
		const contact_2_phone_extension = {
			view: "text",
			name: "contact_2_phone_extension",
			label: "Extensión de teléfono",
			labelPosition: "top",
			placeholder: "Extensión de teléfono",
			invalidMessage: required_text,
			tooltip: "Extensión de teléfono" + "#value#",
			pattern: phone_mask,
			required: false,
			validate: function (val) {
				return true;
			}
		};
		const contact_3_phone_extension = {
			view: "text",
			name: "contact_3_phone_extension",
			label: "Extensión de teléfono",
			labelPosition: "top",
			placeholder: "Extensión de teléfono",
			invalidMessage: required_text,
			tooltip: "Extensión de teléfono" + "#value#",
			pattern: phone_mask,
			required: false,
			validate: function (val) {
				return true;
			}
		};

		const sponsor = {
			view: "server_autocomplete",
			id: "input_sponsor",
			name: "sponsor",
			label: "Sponsor",
			placeholder: "Sponsor",
			url: master_url + "api/crm/user/select_list/?activo=1",
			required: false
		};

		const company_information = {
			template: "Información de compañía", type: "section"
		};

		const company_address = {
			template: "Dirección", type: "section"
		};

		const first_contact = {
			template: "Contacto principal", type: "section"
		};

		const second_contact = {
			template: "Segundo contacto", type: "section"
		};

		const third_contact = {
			template: "Tercer contacto", type: "section"
		};

		const social = {
			template: "Social", type: "section"
		};

		const general = {
			template: "General", type: "section"
		};

		const responsive_view = {
			margin: 10,
			rows: [
				company_information,
				is_project,
				contact_type,
				project,
				customer,
				company,
				rfc,
				business_name,
				line_business,
				industry,
				company_address,
				address,
				city,
				first_contact,
				contact_1_name,
				contact_1_phone,
				contact_1_phone_extension,
				contact_1_position,
				contact_1_email,
				second_contact,
				contact_2_name,
				contact_2_phone,
				contact_2_phone_extension,
				contact_2_position,
				contact_2_email,
				third_contact,
				contact_3_name,
				contact_3_phone,
				contact_3_phone_extension,
				contact_3_position,
				contact_3_email,
				social,
				birthday_date,
				facebook,
				linkedin,
				general,
				important_date,
				important_date_reason,
				sales_potential,
				tentative_closing_date,
				adviser,
				probability_closure,
				sales_team,
				proveedor_estrategico,
				sponsor
			]
		};
		const standard_view = {
			margin: 10,
			rows: [
				company_information,
				{
					cols: [
						{
							margin: 10,
							rows: [
								is_project,
								contact_type,
								customer,
								business_name,
								industry,
							]
						},
						{
							margin: 10,
							rows: [
								{},
								project,
								company,
								line_business,
								rfc,
							]
						}

					]
				},
				company_address,
				{
					cols: [
						{
							margin: 10,
							rows: [
								address,
							]
						},
						{
							margin: 10,
							rows: [
								city,
							]
						}

					]
				},
				first_contact,
				{
					cols: [
						{
							margin: 10,
							rows: [
								contact_1_name,
								contact_1_phone_extension,
								contact_1_position,
							]
						},
						{
							margin: 10,
							rows: [
								contact_1_phone,
								contact_1_email
							]
						}
					]
				},
				second_contact,
				{
					cols: [
						{
							margin: 10,
							rows: [
								contact_2_name,
								contact_2_phone_extension,
								contact_2_position,
							]
						},
						{
							margin: 10,
							rows: [
								contact_2_phone,
								contact_2_email
							]
						}
					]
				},
				third_contact,
				{
					cols: [
						{
							margin: 10,
							rows: [
								contact_3_name,
								contact_3_phone_extension,
								contact_3_position,

							]
						},
						{
							margin: 10,
							rows: [
								contact_3_phone,
								contact_3_email
							]
						}
					]
				},
				social,
				{
					cols: [
						{
							margin: 10,
							rows: [
								birthday_date,
								facebook,
							]
						},
						{
							margin: 10,
							rows: [
								linkedin,
							]
						}
					]
				},
				general,
				{
					cols: [
						{
							margin: 10,
							rows: [
								important_date,
								sales_potential,
								adviser,
								proveedor_estrategico,
							]
						},
						{
							margin: 10,
							rows: [
								important_date_reason,
								tentative_closing_date,
								sales_team,
								probability_closure,
								sponsor,
							]
						}
					]
				},
			],

		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section,
				comments
			]
		};
	}

	init() {
	}
}
