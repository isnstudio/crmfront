import {JetView} from "webix-jet";
import Download_csv from "../../../../helpers/download_csv";

export default class LeadListActionsView extends JetView {
	config() {
		const csv_leads = {
			padding: 20,
			rows: [
				{
					height: 40,
					view: "button",
					value: "Descargar lista de leads",
					click: function () {
						const url = "api/crm/lead/export_csv/";
						const download_csv = new Download_csv();
						download_csv.download_csv(url);
					}
				},
				{}
			]
		};

		return {
			view: "accordionitem",
			template: "Acciones",
			header: "Acciones",
			type: "header",
			id: "actions_header",
			collapsed: true,
			body: csv_leads,
		};

	}
}