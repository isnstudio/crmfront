import {JetView} from "webix-jet";
import "webix/server_autocomplete";

export default class FilterListView extends JetView {
	config() {
		const select_text = "Clic para seleccionar";

		const form = {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			scroll: "y",
			rows: [
				{
					view: "text",
					name: "search",
					localId: "q",
					id: "q",
					label: "Consulta",
					labelPosition: "top",
					placeholder: "Consulta"
				},
				{
					view: "combo",
					name: "sales_potential",
					localId: "sales_potential:combo",
					id: "sales_potential_id",
					label: "Potencial de venta",
					labelPosition: "top",
					placeholder: select_text,
					suggest: {url: master_url + "api/crm/lead/choice_field/sales_potential/"},
				},
				{
					view: "combo",
					name: "status",
					localId: "status:combo",
					id: "status_id",
					label: "Estatus",
					labelPosition: "top",
					placeholder: select_text,
					suggest: {url: master_url + "api/crm/lead/choice_field/status/"},
				},
				{
					view: "multicombo",
					name: "is_project",
					localId: "is_project:combo",
					id: "is_project",
					label: "Tipo",
					labelPosition: "top",
					placeholder: select_text,
					options: [
						{ id: "0", value: "Leads" },
						{ id: "1", value: "Proyectos" }
					],
					value: "0,1"
				},
				{
					id: "sales_team",
					view: "server_autocomplete",
					label: "Sales team",
					labelPosition: "top",
					name: "sales_team_id",
					placeholder: "Sales Team",
					url: master_url + "api/crm/sales_team/select_list/",
					select_text: select_text,
				},
				{
					view: "daterangepicker",
					stringResult: true,
					calendarCount: 1,
					label: "Rango fecha cierre",
					name: "fecha_cierre",
					id: "fecha_cierre",
					labelPosition: "top",
					placeholder: "Clic para seleccionar"
				},
				{
					id: "vendedor",
					view: "server_autocomplete",
					label: "Asesor comercial",
					labelPosition: "top",
					name: "adviser_id",
					placeholder: "Asesor comercial",
					url: master_url + "api/crm/user/select_list/?activo=1&es_vendedor=true",
					select_text: select_text,
				},
				{
					id: "input_sponsor",
					view: "server_autocomplete",
					name: "sponsor_id",
					label: "Sponsor",
					placeholder: "Sponsor",
					url: master_url + "api/crm/user/select_list/?activo=1",
					required: false
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon",
					icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						let querystring = "";
						const values = this.$$("filterform").getValues();
						if (values.is_project == 3)
							delete values.is_project;
						if (Object.keys(values).length !== 0) {
							for (let k in values) {
								if (values[k]) {
									querystring += k + "==" + values[k] + ";";
								}
							}
						}

						let fecha_cierre = this.$$("fecha_cierre").getValue();
						if (fecha_cierre["start"] !== null && querystring) {
							querystring += ";fecha_cierre_range==" + fecha_cierre["start"] + ",";
							if (fecha_cierre["end"] !== null) {
								querystring += fecha_cierre["end"];
							} else {
								querystring += fecha_cierre["start"];
							}
						}

						let table = this._parent.Table.$$("data");
						let current_querystring = this._parent.getParam("filter");
						if (querystring != current_querystring) {
							table.clearAll();
						}

						this._parent.setParam("page", 0, false);
						this._parent.setParam("start", 0, false);
						this._parent.setParam("filter", querystring, true);

					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						let table = this._parent.Table.$$("data");
						this._parent.setParam("page", 0, false);
						this._parent.setParam("start", 0, false);
						this._parent.setParam("filter", "", true);
						this.$$("filter_form").clear();
						table.clearAll();
					}
				}
			]
		};
		return {
			view: "accordionitem",
			id: "form_header",
			template: "Filtros",
			header: "Filtros",
			type: "header",
			collapsed: false,
			body: form,
		};

	}

	init() {

	}
}