import {JetView} from "webix-jet";

export default class UnitsDetailView extends JetView {
	config() {
		const details = {
			rows: [
				{
					view: "property",
					id: "detail_info",
					height: 260,
					nameWidth: 175,
					disabled: true,
					elements: [
						{label: "Proyecto", type: "text", id: "project"},
						{label: "Giro", type: "text", id: "line_business__display_webix"},
						{label: "Industria", type: "text", id: "industry__display_webix"},
						{label: "Equipo", type: "text", id: "sales_team__display_webix"},
						{label: "Teléfono", type: "text", id: "contact_1_phone"},
						{label: "Comentarios", type: "text", id: "comments"},
						{label: "Probabilidad de cierre", type: "text", id: "get_probability_closure_display"},
						{label: "Proveedor estratégico", type: "text", id: "proveedor_estrategico__display_webix"},
						{ label: "Sponsor", type: "text", id: "sponsor__display_webix" }
					]
				},
				{
					localId: "detail_components",
					padding: 20,
					rows: [
						{
							view: "timeline",
							localId: "traces_timeline",
							borderless: true,
							autoheight: true,
							hidden: true,
							on: {
								onAfterRender: function() {
									 const items = this.$view.querySelectorAll('.webix_timeline_item');
									 items.forEach(item => {
										 const valueDiv = item.querySelector('.webix_timeline_value');
										 if (valueDiv && valueDiv.textContent.trim() === "Cotización") {
											 item.classList.add('clickable-item');
										 }
									 });
								},
								onItemClick: (id) => {
                                    const timeline = this.$$("traces_timeline");
                                    const item = timeline.getItem(id);

                                    if (item.log_type === 5) { // Cotizacion
                                        const lead_id = item.lead_id;
                                        this.show("/home/crm.quotation?page=0&start=0&filter=lead_id%3D%3D"+ lead_id);
                                    }
								}
							}
						}
					]
				},
			]
		};

		if (this.app.Mobile)
			return details;

		return {
			view: "accordionitem",
			template: "Detalle",
			header: "Detalle",
			type: "header",
			id: "detalle_header",
			collapsed: true,
			hidden: true,
			body: details
		};
	}

	ready() {
		this.hideContents();
	}

	hideContents() {
		if (!this.app.Mobile) {
			this.$$("detalle_header").hide();
			this.$$("detail_components").hide();
			this.$$("traces_timeline").hide();
			this.$$("detail_info").hide();
		}
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		if (!this.app.Mobile) {
			this.$$("detalle_header").show();
			this.$$("detail_components").show();
			this.$$("detail_info").show();
			this.$$("traces_timeline").hide();
		}
		this.RefreshDetails(id);
	}

	showDetail(id) {
		if (id && id.constructor === Object && "id" in id) {
			id = id["id"];
		}
		this.lead_id = id;
		if (!this.lead_id)
			this.lead_id = this.getParam("id");
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				if (!child_id.includes("$timeline")) {
					child_ids.push(child_id);
				}
			}
			for (let childIdsKey in child_ids) {
				content.removeView(child_ids[childIdsKey]);
			}
		}
		if (!isNaN(id) && id !== "") {
			if (this.getRoot()) {
				this.showContents(id);
			}
		}
		if (!isNaN(this.lead_id) && this.lead_id !== "" && this.app.Mobile) {
			if (this.getRoot()) {
				this.showContents(this.lead_id);
			}
		}
	}

	urlChange() {
		this.showDetail();
	}

	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(master_url + "api/crm/lead/" + id + "/wdetails/");
			const json_response = JSON.parse(xhr.response);

			if ("actions" in json_response && "pk" in json_response) {
				const content = this.$$("detail_components");
				if (content) {
					for (const action_key in json_response["actions"]) {
						const action = json_response["actions"][action_key];
						content.addView({
							view: "button",
							label: action["label"],
							height: 40,
							click: () => {
								this.show("/home/crm.leads.actions." + action["action"] + "?id=" + json_response["pk"]);
							}
						}, 0);
					}
				}
			}
			if ("pk" in json_response) {
				const sets = this.$$("detail_info");
				if (sets) {
					sets.setValues(json_response);
				}
			}
			if ("traces" in json_response) {
				const traces = this.$$("traces_timeline");
				if (traces) {
					const trace_data = json_response["traces"];
					if (trace_data.length > 0) {
						traces.clearAll();
						traces.data_setter(trace_data);
						traces.show();
					}
				}
			}
		}
	}
}

