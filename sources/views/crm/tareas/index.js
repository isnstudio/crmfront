import {JetView} from "webix-jet";

import TableView from "./table";
import ListView from "./responsive/table";
import "webix/breadcrumb";
import FilterListView from "./sidebar/filters";
import UnitsDetailView from "./sidebar/details";
import Tables from "../../../helpers/tables";
export const TASK_TYPE_LEAD = "1";
export const TASK_TYPE_WARM = "2";
export const TASK_TYPE_QUOTATION = "3";

export default class tasksView extends JetView {
	config() {
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Panel de control de tareas", link: "/home/crm.tareas"},
			],
		};

		this.Tables =  new Tables(this.app);
		this.Table = new (this.app.Mobile ? ListView : TableView)(this.app);
		this.Filters = new FilterListView(this.app);
		if (!this.app.Mobile)
			this.UnitsDetailView = new UnitsDetailView(this.app);

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
				{
					view: "icon",
					icon: "mdi mdi-plus",
					localId: "addButton",
					css: "right_icon",
					click: () => {
						this.show("../crm.tareas.actions.add");
					}
				}
			]
		};

		if (this.app.Mobile)
			return {
				view: "accordion",
				rows: [
					this.Filters,
					search,
					this.Table,
					{$subview: true, popup: true}
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.Table,
					]
				},
				{
					view: "accordion",
					width: 384,
					rows: [
						this.Filters,
						this.UnitsDetailView,
					]
				},
			]
		};
	}

	ready(view) {
		const datagrid = view.$scope.Table.$$("data");
		this.Tables.pagerParams(view, datagrid);


	}

	urlChange() {
		const url = master_url + "api/crm/task/wlist/";
		const table = this.Table.$$("data");
		const filters = this.Filters;
		this.Tables.dataLoading(table, url, this, filters);
	}
}
