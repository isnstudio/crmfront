import {JetView,} from "webix-jet";
import "webix/server_autocomplete";
import "webix/datepicker_iso";

export default class FormFieldsView extends JetView {
	config() {
		const dateFormat = webix.Date.dateToStr("%Y-%m-%d");
		const select_text = "Clic para seleccionar";
		const required_text = "Requerido";
		const phone_mask = {mask: "###-###-####", allow: /[0-9]/g};

		const tipo = {
			view: "server_autocomplete",
			name: "task_type",
			localId: "tipo_actividad:combo",
			id: "tipo_actividad_id",
			label: "Tipo de tarea",
			labelPosition: "top",
			placeholder: select_text,
			url: master_url + "api/crm/task/choice_field/task_type/",
			required: true
		};

		const lead = {
			view: "server_autocomplete",
			name: "lead",
			localId: "lead:combo",
			id: "lead_id",
			label: "Lead",
			labelPosition: "top",
			placeholder: select_text,
			url: master_url + "api/crm/lead/select_list/?create_task_or_warm=1",
			hidden: true
		};

		const cotizacion_venta = {
			view: "server_autocomplete",
			name: "cotizacion_venta",
			localId: "cotizacion_venta:combo",
			id: "cotizacion_venta_id",
			label: "Cotización de venta",
			labelPosition: "top",
			placeholder: select_text,
			url: master_url + "api/vnt/cotizacion-venta/select_list/?create_task_or_warm=1&filter=estatus==1,2",
			hidden: true
		};

		const warm = {
			view: "server_autocomplete",
			name: "warm",
			localId: "warm:combo",
			id: "warm_id",
			label: "Warm",
			labelPosition: "top",
			placeholder: select_text,
			url: master_url + "api/crm/warm/select_list/?create_task_or_warm=1",
			hidden: true
		};

		const activity = {
			id: "activity_id",
			name: "activity",
			view: "server_autocomplete",
			label: "Actividad",
			url: master_url + "api/crm/activity/select_list/",
			required: true,
			disabled: true
		};

		const start_date = {
			view: "datepicker_iso",
			name: "start_date",
			id: "start_date",
			label: "Fecha y hora (Inicio)",
			labelPosition: "top",
			placeholder: select_text,
			timepicker: true,
			required: true,
			format: dateFormat
		};

		const date = {
			view: "datepicker_iso",
			name: "date",
			id: "date",
			label: "Fecha y hora (Fin)",
			labelPosition: "top",
			placeholder: select_text,
			timepicker: true,
			required: true,
			format: dateFormat
		};

		const reminder_time = {
			view: "combo",
			name: "reminder_time",
			localId: "reminder_time:combo",
			id: "reminder_time",
			label: "Notificación",
			labelPosition: "top",
			placeholder: select_text,
			invalidMessage: required_text,
			suggest: { url: master_url + "api/crm/task/choice_field/reminder_time/" },
			required: true,
		};

		const responsable = {
			id: "responsable_id",
			name: "responsable",
			view: "server_autocomplete",
			label: "Responsable (En caso de que no sea el Encargado de la Actividad)",
			url: master_url + "api/crm/user/select_list/?activo=1",
		};

		const description = {
			view: "textarea",
			name: "description",
			id: "description",
			label: "Comentarios",
			labelPosition: "top",
			placeholder: "Comentarios",
			invalidMessage: required_text,
			height: 75
		};

		const left_side = {
			margin: 10,
			rows: [
				tipo,
				start_date,
				activity,
				reminder_time,
			]
		};
		const right_side = {
			margin: 10,
			rows: [
				lead,
				cotizacion_venta,
				warm,
				date,
				responsable,
			]
		};

		const standard_view = {
			margin: 10,
			cols: [
				left_side,
				right_side
			]
		};

		const responsive_view = {
			margin: 10,
			rows: [
				activity,
				date,
				reminder_time,
				responsable,
			]
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section,
				description
			]
		};
	}

	init() {
	}
}
