import {JetView} from "webix-jet";
import FormFieldsView from "../fields";
import "webix/breadcrumb";
import SaveService from "../../../../services/save";
import { TASK_TYPE_LEAD, TASK_TYPE_WARM, TASK_TYPE_QUOTATION } from "../index";

export default class TaskAddView extends JetView {
	config() {
		const that = this;
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Tareas", link: "/home/crm.tareas"},
				{value: "Agregar", link: "/home/crm.tareas.actions.add"},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Agregar",
					height: 40,
					css: "webix_primary",
					id: "save",
					click: function() {
						var button = this;
						that.create(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						window.history.back();
					}
				},
			]
		};

		this.Fields = new FormFieldsView(this.app);

		this.form = {
			view: "form",
			localId: "form",
			id: "form",
			paddingX: 20,
			rows: [
				this.Fields,
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						this.form,
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							this.form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}



	ready() {
		const that = this;
		let input_activity = this.Fields.$$("activity_id")
		let input_tipo_actividad = this.Fields.$$("tipo_actividad_id")

		let input_lead = that.Fields.$$("lead_id")
		let input_warm = that.Fields.$$("warm_id")
		let input_cotizacion_venta = that.Fields.$$("cotizacion_venta_id")

		let input_responsable = that.Fields.$$("responsable_id")

		input_tipo_actividad.attachEvent("onChange", function (value) {
			if (value == TASK_TYPE_LEAD){
				input_lead.show()
				input_cotizacion_venta.hide()
				input_warm.hide()
			}
			else if (value == TASK_TYPE_WARM) {
				input_lead.hide()
				input_cotizacion_venta.hide()
				input_warm.show()
			}
			else {
				input_lead.hide()
				input_cotizacion_venta.show()
				input_warm.hide()
			}
			input_activity.setValue("")
			input_activity.disable()
		});

		input_lead.attachEvent("onChange", function (value) {
			input_activity.enable()
		});
		input_warm.attachEvent("onChange", function (value) {
			input_activity.enable()
		});
		input_cotizacion_venta.attachEvent("onChange", function (value) {
			input_activity.enable()
		});

		input_activity.attachEvent("onChange", function (value) {
			const xhr = webix.ajax().sync().get(master_url + "api/crm/activity/" + value);
			const activity = JSON.parse(xhr.response);
			let responsable;
			if (activity.generic){
				let tipo_actividad = input_tipo_actividad.getValue()
				if (tipo_actividad == TASK_TYPE_LEAD) {
					webix.ajax().get(master_url + "api/crm/lead/" + input_lead.getValue()).then(function (response) {
						responsable = response.json().adviser
						input_responsable.define({ "value": responsable })
						input_responsable.refresh()
					});
				}
				else if (tipo_actividad == TASK_TYPE_WARM){
					webix.ajax().get(master_url + "api/crm/warm/" + input_warm.getValue()).then(function (response) {
						responsable = response.json().lead__adviser
						input_responsable.define({ "value": responsable })
						input_responsable.refresh()
					});
				}
				else {
					webix.ajax().get(master_url + "api/vnt/cotizacion-venta/" + input_cotizacion_venta.getValue()).then(function (response) {
						responsable = response.json().vendedor
						input_responsable.define({ "value": responsable })
						input_responsable.refresh()
					});
				}
			}
		});
	}

	create(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/task/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Tareas guardada",
			path: "/home/crm.tareas/",
			button: button,
			context: that,
			method: "post"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}
