import {JetView} from "webix-jet";
import FormFieldsView from "../fields";
import "webix/breadcrumb";
import SaveService from "../../../../services/save";
import { TASK_TYPE_LEAD, TASK_TYPE_WARM, TASK_TYPE_QUOTATION } from "../index";


export default class TaskEditView extends JetView {
	config() {
		const that = this;
		this.task_id = this.getParam("id");
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Tareas", link: "/home/crm.tareas"},
				{ value: "Editar comentario" },
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Confirmar edición",
					height: 40,
					css: "webix_primary",
					id: "save",
					click: function() {
						var button = this;
						that.create(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						window.history.back();
					}
				},
			]
		};

		this.Fields = new FormFieldsView(this.app);

		this.form = {
			view: "form",
			localId: "form",
			id: "form",
			paddingX: 20,
			rows: [
				this.Fields,
			]
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					rows: [
						search,
						buttons,
						this.form,
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							this.form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}



	ready() {
		const that = this;
		webix.ajax().get(master_url + "api/crm/task/" + this.task_id).then(function (response) {
			const json_response = response.json();
			that.$$("form").setValues(json_response, true);

			if (json_response.task_type == TASK_TYPE_LEAD){
				that.Fields.$$("lead_id").show()
				that.Fields.$$("cotizacion_venta_id").hide()
				that.Fields.$$("warm_id").hide()
			}
			else if (json_response.task_type == TASK_TYPE_WARM) {
				that.Fields.$$("lead_id").hide()
				that.Fields.$$("cotizacion_venta_id").hide()
				that.Fields.$$("warm_id").show()
			}
			else {
				that.Fields.$$("lead_id").hide()
				that.Fields.$$("cotizacion_venta_id").show()
				that.Fields.$$("warm_id").hide()
			}
		});

		const form = this.$$("form");
		for (var element in form.elements) {
			form.elements[element].disable();
		}
		that.Fields.$$("description").enable()
	}

	create(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/task/" + that.task_id + "/edit/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Comentario actualizado",
			path: "/home/crm.tareas/",
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}
