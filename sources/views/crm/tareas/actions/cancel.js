import { JetView } from "webix-jet";
import "webix/breadcrumb";
import SaveService from "../../../../services/save";

export default class TaskCancelView extends JetView {
	config() {
		const required_text = "Requerido";
		this.tarea_id = this.getParam("id");
		this.task_id = this.getParam("task_id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{ value: "Tareas", link: "/home/crm.tareas?id=" + this.tarea_id },
				{
					value: "#" + this.tarea_id + " - Cancelar",
					link: "/home/crm.tareas?id=" + this.tarea_id + "&task_id=" + this.task_id
				},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const cancel_motive = {
			view: "textarea",
			name: "cancel_motive",
			label: "Comentarios",
			labelPosition: "top",
			placeholder: "Comentarios",
			invalidMessage: required_text,
			height: 150,
			required: true,
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Estás seguro de cancelar la tarea?",
					type: "label",
					borderless: true,
					css: "question"
				},
				cancel_motive
			],
			rules: {
				cancel_motive: webix.rules.isNotEmpty,
			}
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			padding: 20,
			rows: [
				{
					view: "button",
					value: "Guardar",
					css: "webix_primary",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.action(button);
					}
				},
				{
					view: "button",
					value: "Cancelar",
					height: 40,
					click: () => {
						this.show("/home/crm.tareas?id=" + this.tarea_id + "&task_id=" + this.task_id);
					}
				},
			]
		};


		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}

	action(button) {
		const that = this;
		const request_options = {
			url: master_url + "api/crm/task/" + that.tarea_id + "/cancel/",
			get_values: that.$$("form").getValues(),
			validate: that.$$("form").validate(),
			message: "Tarea cancelada",
			path: "/home/crm.tareas?id=" + that.tarea_id + "&task_id=" + that.task_id,
			button: button,
			context: that,
			method: "put"
		};
		this.SaveService = new SaveService(this.app);
		this.SaveService.save(request_options);
	}
}
