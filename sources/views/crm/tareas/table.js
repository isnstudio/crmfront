import {JetView} from "webix-jet";
import PagerView from "../../common/pager";
import Tables from "../../../helpers/tables";

export default class TableView extends JetView {
	config() {
		this.Tables = new Tables(this.app);
		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "id",
					header: "Folio",
					sort: "string",
					width: 80,
				},
				{
					id: "activity__display_webix",
					header: "Actividad",
					width: 160,
				},
				{
					id: "date",
					header: {text: "Fecha", css: "right"},
					width: 175,
					css: "right"
				},
				{
					id: "owner__display_webix",
					header: { text: "Owner", css: "right" },
					css: "right",
					width: 175
				},
				{
					id: "responsable__display_webix",
					header: { text: "Asignada a (Responsable)", css: "right" },
					css: "right",
					minWidth: 205
				},
				{
					id: "get_task_type_display",
					header: "Tipo",
					width: 100
				},
				{
					id: "lead__display_webix",
					header: { text: "Lead", css: "right"},
					css: "right",
					width: 175
				},
				{
					id: "warm__display_webix",
					header: { text: "Warm", css: "right" },
					width: 200,
					css: "right"
				},
				{
					id: "cotizacion_venta__display_webix",
					header: { text: "Cotización venta" },
					css: "upper",
					adjust: true,
					minWidth: 300,
				},
				{
					id: "get_status_display",
					header: { text: "Estatus", css: "right" },
					sort: "string",
					css: "right",
					fillspace: true,
					minWidth: 135
				},
			],
			on: {
				onAfterRender() {
					this.$scope.Tables.afterRender(this, true);
				},
				onAfterSelect: (id) => {
					let record = $$("grid").getItem(id);
					this._parent.UnitsDetailView.showDetail(id, record.folio);
					this._parent.UnitsDetailView.$$("detail_info").show();
					$$("form_header").collapse();

					this.setParam("id", id, true);
				},
			}
		};


		return {
			rows: [
				{
					view: "scrollview",
					scroll: "y",
					body: {
						rows: [
							datatable,
						]
					}
				},
				PagerView
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}


	urlChange() {
		const details = [
			this._parent.UnitsDetailView
		];
		const datatable = this.$$("data");
		this.Tables.hideContents(this, datatable, details);
	}
}