import {JetView} from "webix-jet";
import "webix/server_autocomplete";

export default class FilterListView extends JetView {
	config() {
		const select_text = "Clic para seleccionar";

		const form = {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			scroll: "y",
			rows: [
				{
					view: "server_autocomplete",
					name: "activity_id",
					localId: "tipo_actividad:combo",
					id: "tipo_actividad_id",
					label: "Tipo de actividad",
					labelPosition: "top",
					placeholder: select_text,
					url: master_url + "api/crm/activity/select_list/",
				},
				{
					id: "vendedor",
					view: "server_autocomplete",
					label: "Asesor comercial (Owner)",
					labelPosition: "top",
					name: "owner_id",
					placeholder: "Asesor comercial",
					url: master_url + "api/crm/user/select_list/?activo=1&es_vendedor=true",
					select_text: select_text,
				},
				{
					id: "responsable_id",
					view: "server_autocomplete",
					name: "responsable_id",
					label: "Asignada a (Responsable)",
					labelPosition: "top",
					placeholder: select_text,
					url: master_url + "api/crm/user/select_list/?activo=1",
					select_text: select_text,
				},
				{
					id: "sales_team_id",
					view: "server_autocomplete",
					name: "sales_team_id",
					label: "Sales Team",
					labelPosition: "top",
					placeholder: select_text,
					url: master_url + "api/crm/sales_team/select_list/?activo=1&es_vendedor=true",
					select_text: select_text,
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon",
					icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						let querystring = "";
						const values = this.$$("filterform").getValues();
						if (values.is_project == 3)
							delete values.is_project;
						if (Object.keys(values).length !== 0) {
							for (let k in values) {
								if (values[k]) {
									querystring += k + "==" + values[k] + ";";
								}
							}
						}

						let table = this._parent.Table.$$("data");
						let current_querystring = this._parent.getParam("filter");
						if (querystring != current_querystring) {
							table.clearAll();
						}

						this._parent.setParam("page", 0, false);
						this._parent.setParam("start", 0, false);
						this._parent.setParam("filter", querystring, true);

					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						let table = this._parent.Table.$$("data");
						this._parent.setParam("page", 0, false);
						this._parent.setParam("start", 0, false);
						this._parent.setParam("filter", "", true);
						this.$$("filter_form").clear();
						table.clearAll();
					}
				}
			]
		};
		return {
			view: "accordionitem",
			id: "form_header",
			template: "Filtros",
			header: "Filtros",
			type: "header",
			collapsed: false,
			body: form,
		};

	}

	init() {

	}
}