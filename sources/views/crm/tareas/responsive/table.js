import {JetView} from "webix-jet";
import PagerView from "../../../common/pager";

export default class ListView extends JetView {
	config() {
		return {
			rows: [
				{
					view: "list",
					localId: "data",
					css: "multi-line-box",
					select: true,
					pager: "pager",
					type: {
						height: "auto",
						template: obj =>
							`
								<span style="float:right; text-transform: uppercase"><b>${obj.id}</b></span>
								<span style="font-weight: 500;"><b>Actividad</b>: ${obj.activity__display_webix}</span>
								<br>
								<span style="font-weight: 500;"><b>Fecha</b>: ${obj.date}</span>
								<br>
								<span style="font-weight: 500;"><b>Owner</b>:${obj.owner__display_webix}</span>
								<br>
								<span style="font-weight: 500;"><b>Responsable</b>: ${obj.responsable__display_webix}</span>
								<br>
								<span style="font-weight: 500;"><b>Tipo</b>: ${obj.get_task_type_display}</span>
								<br>
								<span style="font-weight: 500;"><b>Lead</b>: ${obj.lead__display_webix}</span>
								<br>
								<span style="font-weight: 500;"><b>Warm</b>: ${obj.warm__display_webix}</span>
								<br>
								<span style="font-weight: 500;"><b>Estatus</b>: ${obj.get_status_display}</span>
							`
					},
					on: {
						onItemClick: (id) => {
							this.show("/home/crm.tareas/crm.tareas.popups.details?id=" + id);
						}
					}
				},
				PagerView
			]
		};
	}
}