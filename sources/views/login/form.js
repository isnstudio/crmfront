import {JetView} from "webix-jet";

export default class TablesView extends JetView {
	config() {
		const form = {
			view: "form",
			localId: "form",
			css: {
				"background-color": "white !important",
			},
			borderless: true,
			autowidth: true,
			maxWidth: 400,
			rows: [
				{
					cols: [
						{
							template: "<img src='/data/images/logo.png' alt='logo'>",
							height: 50,
							css: {"width": "80% !important"}
						},
						{
							css: {
								"font-size": "16px"
							},
							template: "<br><b>CRM</b>",
							height: 50
						},
					]
				},
				{
					align: "left",
					view: "text",
					label: "Usuario",
					name: "username",
					id: "username",
					height: 48,
					labelWidth: 90
				},
				{
					view: "text",
					type: "password",
					label: "Contraseña",
					name: "password",
					id: "password",
					height: 48,
					labelWidth: 90
				},
				{
					view: "button",
					css: "webix_primary",
					label: "Login",
					value: "submit",
					height: 50,
					hotkey: "enter",
					click: () => {
						if (this.$$("form").validate()) {
							const form_values = this.$$("form").getValues();
							const authService = this.app.getService("auth");
							const username = form_values.username;
							const password = form_values.password;
							authService.login(username, password).then(() => {
								if (this.getUrl()[0].page === "login") {
									this.show("/home/crm.leads");
								} else {
									this.app.refresh();
								}
							},
							() => this.IncorrectLogin());
						}
					},
				},
			],
			rules: {
				username: webix.rules.isNotEmpty,
				password: webix.rules.isNotEmpty
			}
		};

		if (this.app.Mobile)
			return {
				rows: [
					{},
					form,
					{},
				]
			};
		return {
			rows: [
				{},
				{
					cols: [
						{},
						form,
						{}
					]
				},
				{},
			]
		};
	}

	ready() {
		if (this.app.Mobile) {
			const form = this.$$("form");
			const username = this.$$("username");
			const password = this.$$("password");
			username.define("labelPosition", "top");
			username.define("height", 65);
			username.refresh();
			password.define("labelPosition", "top");
			password.define("height", 65);
			password.refresh();
			form.resize();
		}
	}

	IncorrectLogin() {
		webix.message({
			text: "Error al iniciar sesión, verifique sus credenciales",
			type: "error",
			expire: -1,
			id: "messageLogin"
		});
	}
}
