export const data = new webix.DataCollection({
	scheme: {
		$change: (obj) => {
			if (obj.start) obj.start = new Date(obj.start);
		},
	},
	data: [
		{
			id: "1",
			team: "LIFE & CARE",
			status: "BAJO",
			sale: 110000,
			goal: 200000,
			fc: 90000,
			tibios: 20000
		},
		{
			id: "2",
			team: "SAFETY",
			status: "QUE NO PARE",
			sale: 50000,
			goal: 40000,
			fc: 50000,
			tibios: 10000
		},
		{
			id: "3",
			team: "MRO",
			status: "BAJO",
			sale: 20000,
			goal: 40000,
			fc: 20000,
			tibios: 10000
		},
		{
			id: "4",
			team: "LOGISTICA",
			status: "BAJO",
			sale: 30000,
			goal: 50000,
			fc: 20000,
			tibios: 10000
		},
	],
});
  