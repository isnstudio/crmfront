export const data = new webix.DataCollection({
	scheme: {
		$change: (obj) => {
			if (obj.start) obj.start = new Date(obj.start);
		},
	},
	data: [
		{
			id: 1,
			total_sale: 210000,
			total_goal: 320000,
			total_fc: 180000,
			tibios: 50000
		}
	],
});
