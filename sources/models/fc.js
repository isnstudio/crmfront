export const data = new webix.DataCollection({
	scheme: {
		$change: (obj) => {
			if (obj.start) obj.start = new Date(obj.start);
		},
	},
	data: [
		{
			id: "1",
			fc: 90000,
			tibios: 200000,
			seller: "Jhon Doe",
			team: "LOGISTICA",
			date: "20/20/2021",
			acciones: [
				{"view": "button", "value": "edit", "label": "Editar"},
			]
		},
		{
			id: "2",
			fc: 90000,
			tibios: 200000,
			seller: "Jhon Sanchez",
			team: "MRO",
			date: "20/20/2021",
			acciones: [
				{"view": "button", "value": "edit", "label": "Editar"},
			]
		},
		{
			id: "3",
			fc: 90000,
			tibios: 200000,
			seller: "John Smith",
			team: "SAFETY",
			date: "20/20/2021",
			acciones: [
				{"view": "button", "value": "edit", "label": "Editar"},
			]
		}
	],
});
  