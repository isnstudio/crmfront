export function getTipoPagos() {
	return tipo_pagos;
}

const tipo_pagos = [
	{id: "$empty", value: "--", $empty: true},
	{id: "2", value: "Efectivo"},
	{id: "3", value: "Transferencia"},
	{id: "4", value: "Cheque"},
];
