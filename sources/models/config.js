export const data = new webix.DataCollection({
	scheme: {
		$change: (obj) => {
			if (obj.start) obj.start = new Date(obj.start);
		},
	},
	data: [
		{
			id: "1",
			date: "20/12/2021",
			ejercicio: "2021",
			total_mont: 100000,
			created_by: "John Smith",
			acciones: [
				{"view": "button", "value": "edit", "label": "Editar"},
				{"view": "button", "value": "details", "label": "Detalles"},
			]
		}
	],
});
  