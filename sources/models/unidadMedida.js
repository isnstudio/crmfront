export function getUnidadMedida() {
	return unidad_medida;
}

const unidad_medida = [
	{id: "$empty", value: "--", $empty: true},
	{id: "1", value: "Ganadería"},
	{id: "2", value: "Software"},
	{id: "3", value: "Minería"},
	{id: "4", value: "Pesca"},
	{id: "5", value: "Bienes raíces"},
	{id: "6", value: "Construcción"},
	{id: "7", value: "Transporte aéreo"},
	{id: "8", value: "Turismo"},
	{id: "9", value: "Telecomunicaciones"},
	{id: "10", value: "Metalurgia"},
	{id: "11", value: "Cinematográfica"},
	{id: "12", value: "Editorial"},
	{id: "13", value: "Mercados mayoristas"},
	{id: "14", value: "Productor agrícola"},
	{id: "15", value: "Empresa de diseño"},
	{id: "16", value: "Electricidad"},
	{id: "17", value: "Agua potable"},
	{id: "18", value: "Cobranzas"},
	{id: "19", value: "Vigilancia"},
	{id: "20", value: "Derecho"},
];
