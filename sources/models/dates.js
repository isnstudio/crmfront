export function getDates() {
	return dates;
}

const dates = [
	{id: "$empty", value: "--", $empty: true},
	{id: "1", value: "2020"},
	{id: "2", value: "2021"},
	{id: "3", value: "2022"},
	{id: "4", value: "2023"},
	{id: "5", value: "2024"},
	{id: "6", value: "2025"},
	{id: "7", value: "2026"},
	{id: "8", value: "2027"},
	{id: "9", value: "2029"},
	{id: "10", value: "2030"},
];
