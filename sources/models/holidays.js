export const data = new webix.DataCollection({
	data: [
		{
			id: 1,
			date: "01/01/2021",
			description: "Año nuevo",
		},
		{
			id: 2,
			date: "05/02/2021",
			description: "Constitución Mexicana",
		},
		{
			id: 3,
			date: "01/05/2021",
			description: "Día del trabajo",
		},
	],
});
