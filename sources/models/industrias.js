export function getIndustrias() {
	return industrias;
}

const industrias = [
	{id: "$empty", value: "--", $empty: true},
	{id: "1", value: "Siderurgia"},
	{id: "2", value: "Metalurgia"},
	{id: "3", value: "Cemento"},
	{id: "4", value: "Química"},
	{id: "5", value: "Petroquímica"},
	{id: "6", value: "Automotores"},
	{id: "7", value: "Naviera"},
	{id: "8", value: "Ferrocarriles"},
	{id: "9", value: "Armamento"},
	{id: "10", value: "Textiles"},
	{id: "11", value: "Papel"},
	{id: "12", value: "Aeronáutica"},
	{id: "13", value: "Minería"},
	{id: "14", value: "Alimentos"},
	{id: "15", value: "Textil"},
];
