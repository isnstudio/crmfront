export const data = new webix.DataCollection({
	scheme: {
		$change: (obj) => {
			if (obj.start) obj.start = new Date(obj.start);
		}
	},
	data: [
		{
			"almacen": "",
            "costo_mxp": "",
            "costo_usd": "",
            "existencia": "",
            "reserva": "",
            "disponibilidad": "",
		},
	]
});

