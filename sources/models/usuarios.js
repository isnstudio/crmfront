export function getUsuarios() {
	return usuarios;
}

const usuarios = [
	{id: "$empty", value: "--", $empty: true},
	{id: "1", value: "Gabriela Diego"},
	{id: "2", value: "Sonia Chaves"},
	{id: "3", value: "Oliver Manzano"},
	{id: "4", value: "Abraham Gonzalo"},
	{id: "5", value: "Petra Velez"},
	{id: "6", value: "Alicia Pedraza"},
	{id: "7", value: "Azucena Miguel"},
	{id: "8", value: "Aday Mansilla"},
	{id: "9", value: "Javier Gamero"},
	{id: "10", value: "Matias Vargas"},
];
