export function getPotenciales() {
	return potenciales;
}

const potenciales = [
	{id: "$empty", value: "--", $empty: true},
	{id: "1", value: "0 - 10k"},
	{id: "2", value: "10k - 20k"},
	{id: "3", value: "20k - 30k"},
	{id: "4", value: "30k - 40k"},
	{id: "5", value: "40k - 50k"},
	{id: "6", value: "50k - 60k"},
	{id: "7", value: "60k - 70k"},
	{id: "8", value: "70k - 80k"},
	{id: "9", value: "80k - 90k"},
	{id: "10", value: "90k - 100k"},
];
