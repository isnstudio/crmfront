export function getTiposContacto() {
	return tipos_contacto;
}

const tipos_contacto = [
	{id: "$empty", value: "--", $empty: true},
	{id: "1", value: "Tipo de contacto 1"},
	{id: "2", value: "Tipo de contacto 2"},
	{id: "3", value: "Tipo de contacto 3"},
];
