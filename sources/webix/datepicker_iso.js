webix.protoUI({
	name: "datepicker_iso",
	$cssName: "datepicker",
	$init: function (config) {
		config.height = 60;
		config.type = "calendar";
		config.stringResult = true;
		webix.Date.startOnMonday = true;
		if (config.timepicker === true) {
			webix.i18n.parseFormat = "%Y-%m-%d %H:%i";
			webix.i18n.setLocale();
			config.format = "%Y-%m-%d %H:%i";
		} else {
			webix.i18n.parseFormat = "%Y-%m-%d";
			webix.i18n.setLocale();
			config.format = "%Y-%m-%d";
		}
		if(config.onlyDay === 0 || config.onlyDay === 1) {
			config.suggest = {
				type:"calendar",
				body:{
					blockDates:function(date){
						return date.getDay() !== config.onlyDay;
					}
				}
			};
		}
	},
	defaults: {
		labelPosition: "top",
		height: 60
	}
}, webix.ui.datepicker);