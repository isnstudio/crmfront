webix.protoUI({
	name: "multi_server_autocomplete",
	$cssName: "multiselect",
	$init: function (config) {
		config.height = 60;
		if (typeof (config.url) === "undefined") {
			alert("A server autocomplete component was defined without the url property");
			return;
		}
		if (typeof (config.id) === "undefined") {
			alert("A server autocomplete component was defined without the id property");
			return;
		}
		if (typeof (config.name) === "undefined") {
			alert("A server autocomplete component was defined without the name property");
			return;
		}
		this.attachEvent("onChange", function (newValue, oldValue, source) {
			if (newValue !== "") {
				const element_id = this.data.id;
				const element_name = this.data.name;
				const options = this.getList();
				const found = options.find(function (obj) {
					return obj.id == newValue;
				});
				if (found.length == 0 && typeof newValue !== "object") {
					var values = this.$scope._parent.$$("form").getValues();
					var var_repr = {id: values[element_name], value: values[element_name + "__display_webix"]};
					$$(element_id).setValue(var_repr);
				}
			}
			var values = this.getValue().split(this.config.separator);
			if (values.length > 10) {
				webix.message("Maximo 10 valores");
				this.blockEvent();
				this.data.value = oldValue;
				this.unblockEvent();
			}
		});
	},
	url_setter: function (value) {
		var sugg = {
			template(obj, type) {
				return obj.value;
			},
			keyPressTimeout: 500,
			body: {
				url: value,
				dataFeed: function (svalue) {
					this.clearAll();
					let url = value;
					url += svalue ? "?param=" + svalue : "";
					this.load(url);
				},
				template(obj, type) {
					return obj.value;
				}
			}
		};
		$$(this.data.id).define("suggest", sugg);
		$$(this.data.id).refresh();
		return value;
	},
	defaults: {
		labelPosition: "top",
		height: 60
	}
}, webix.ui.multicombo);