webix.protoUI({
	name: "server_autocomplete",
	$cssName: "combo",
	$init: function (config) {
		config.height = 60;
		config.clear = "replace";
		if (typeof (config.url) === "undefined") {
			alert("A server autocomplete component was defined without the url property");
			return;
		}
		if (typeof (config.id) === "undefined") {
			alert("A server autocomplete component was defined without the id property");
			return;
		}
		if (typeof (config.name) === "undefined") {
			alert("A server autocomplete component was defined without the name property");
			return;
		}

		this.attachEvent("onChange", function (newValue) {
			if (newValue !== "") {
				const element_id = this.data.id;
				const element_name = this.data.name;
				const options = this.getList();
				const found = options.find(function (obj) {
					return obj.id === newValue;
				});
				if (found.length === 0 && typeof newValue !== "object" && !this.data.windowForm) {
					let values;
					if(!this.$scope){
						if( $$("form_wizard"))
							values = $$("form_wizard").getValues();
						else values = $$("form").getValues();
					}
					else if(this.$scope._parent.form_view) {
						values = $$("form") ? $$("form").getValues() : this.$scope._parent.form_view._root;
					}
					else {
						let filter_form = this.$scope.$$("filter_form");
						if(filter_form){
							values = filter_form.getValues();
						} else {
							values = this.$scope._parent.$$("form") ?
								this.$scope._parent.$$("form").getValues() :
								this.$scope.$$("form").getValues();
						}
					}
					if (values[element_name + "__display_webix"]) {
						var var_repr = {id: values[element_name], value: values[element_name + "__display_webix"]};
						$$(element_id).setValue(var_repr);
					} else {
						console.log("No existe display webix en " + element_name);
					}
				}
			}
		});
	},
	url_setter: function (value) {
		var sugg = {
			template(obj, type) {
				return obj.value;
			},
			keyPressTimeout: 500,
			body: {
				url: value,
				dataFeed: function (svalue) {
					this.clearAll();
					let url = value;
					let data_url = new URL(url);
					if (data_url.search)
						url += svalue ? "&param=" + svalue : "";
					else url += svalue ? "?param=" + svalue : "";
					this.load(url);
				},
				template(obj, type) {
					return obj.value;
				}
			}
		};
		$$(this.data.id).define("suggest", sugg);
		$$(this.data.id).refresh();
		return value;
	},
	defaults: {
		labelPosition: "top",
		height: 60
	}
}, webix.ui.combo);