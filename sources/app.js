import "./styles/app.css";
import {JetApp} from "webix-jet";
import AuthService from "./services/auth";

export default class safranApp extends JetApp {
	constructor(config) {
		super(
			webix.extend(
				{
					id: "MSC",
					version: 1.0,
					start: "/login",
					debug: !PRODUCTION,
				},
				config,
				true
			)
		);
		// this.master_url = "http://142.93.202.60/";
		this.Mobile = !!(webix.env.mobile || window.innerWidth < 955 || screen.width < 1366);
		master_url = MASTER_URL;
		this.Size = webix.env.mobile ? {databar: -1} : {databar: 265};

		/* error tracking */
		this.attachEvent("app:error:resolve", function (name, error) {
			console.log(error);
		});
	}

	Init(node, api) {
		const rights = {
			"leads": "leads",
			"quotation": "quotation",
			"sale":	"sale",
			"sale_accumulated": "sale_accumulated",
			"tibios": "tibios",
			"salesfunnel": "salesfunnel",
			"pronostico": "pronostico",
			"tac_4": "tac_4",
			"xselling": "xselling",
			"holidays": "holidays",
			"budgets": "budgets",
			"profile": "profile",
			"salesteam": "salesteam",
			"login": "login",
			"tareas": "tareas",
		};
		this.setService("auth", new AuthService(rights));
		this.render(node);
	}
}

export {safranApp};