import Errors from "../helpers/errors";

export default class SaveService {
	save(request_options) {
		if (request_options.validate) {
			if(request_options.button) {
				webix.extend(request_options.context.$$("save"), webix.ProgressBar);
				request_options.button.disable();
			}
			const ajax = request_options.method === "post" ?
				webix.ajax().headers({
					"Content-type":"application/json"
				}).post(request_options.url, request_options.get_values) :
				webix.ajax().headers({
					"Content-type":"application/json"
				}).put(request_options.url, request_options.get_values);
			ajax.then(function (response) {
				webix.message(request_options.message);
				const json_response = response.json();
				if (request_options.path)
					if (json_response && json_response.id)
						request_options.context.show(request_options.path + json_response.id);
					else
						request_options.context.show(request_options.path);
				else request_options.context.refresh();
			}).fail(function (res) {
				const error = new Errors();
				error.show_error(res, request_options.context);
			}).finally(function () {
				if(request_options.button) {
					request_options.button.hideProgress();
					request_options.button.enable();
				}
				return true;
			});
		} else {
			webix.message({
				text: "Faltan campos por llenar",
				type: "error",
				expire: -1,
				id: "messageLead"
			});
		}
	}
}