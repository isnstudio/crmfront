export default class AuthService {
	constructor(rights, user, reset) {
		this._reset = reset;
		this._rights = rights;

		this.setRights(user);
	}

	setRights(user) {
		this.rights = user;
		this._allowed = {};
		if (this.rights) {
			this.rights.forEach(a => this._allowed[a] = true);
		} else if(!this.rights) {
			this.rights = localStorage.getItem("Rights");
			let rights = this.rights ? this.rights.split(",") : [""];
			rights.forEach(a => this._allowed[a] = true);
		}
		return this.rights;
	}

	getRights() {
		const res = [];
		for (var name in this._rights)
			res.push({id: this._rights[name], name});
		return res;
	}

	getUser() {
		return this.rights;
	}

	login(username, password) {
		return webix.ajax().post(master_url + "api-token-auth/", {username, password})
			.then(res => {
				res = res.json();
				if (res && res.token) {
					localStorage.setItem("Token", res.token);
					webix.attachEvent("onBeforeAjax", function (mode, url, params, x, headers) {
						headers["Authorization"] = "Token " + res.token;

					});
				}
				localStorage.setItem("Rights", res.rights);
				if (res && res.rights) {
					this.setRights(res.rights);
				}
			});
	}

	register(name, email, pass) {
		return webix.ajax().post("/auth/register", {name, email, pass})
			.then(res => {
				res = res.json();
				if (res && res.user)
					this.setUser(res.user);

				return this._reset();
			})
			.then(() => this.getUser());
	}

	logout() {
		webix.attachEvent("onBeforeAjax", function (mode, url, params, x, headers) {
			headers["Authorization"] = "";
		});
		localStorage.setItem("Token", "");
		localStorage.setItem("Rights", "");
		window.location.replace("/#!/login");
	}

	hasAccess(right) {
		const code = this._rights[right];
		if (!code)
			throw "Unknow access right name: " + right;

		return !!this._allowed[code];
	}

	guardAccess(right) {
		if (!this.hasRight(right)) throw "Access denied";
	}
}