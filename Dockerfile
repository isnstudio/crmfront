# Use nginx base image
FROM nginx:1.20

# Replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Update apt and install dependencies in one layer
RUN apt-get update --fix-missing && \
    apt-get install -y curl build-essential libssl-dev rsync && \
    rm -rf /var/lib/apt/lists/*

# Create the NVM directory and install NVM and Node in one step to optimize caching
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 14.17.0

RUN mkdir -p $NVM_DIR && \
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash && \
    source $NVM_DIR/nvm.sh && \
    nvm install $NODE_VERSION && \
    nvm alias default $NODE_VERSION && \
    nvm use default

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Create necessary directories in one layer to minimize the number of layers
RUN mkdir -p /code /webapp/node_modules /webapp/data /webapp/codebase

# Set the working directory
WORKDIR /code

# Copy only package.json and package-lock.json first to leverage Docker layer caching

# Install dependencies
ARG NPM_TOKEN 
ARG MASTER_URL
ARG SOCKET_URL

COPY package.json package.json
# COPY package-lock.json package-lock.json
COPY .npmrc .npmrc 

RUN npm install
RUN rm -f .npmrc

# Copy the rest of the application files after npm install
COPY . /code/

# Build the application
RUN npm run build

# Rsync the necessary files to /webapp/
RUN rsync index.html /webapp/index.html && \
    rsync -avu node_modules/@xbs /webapp/node_modules/ && \
    rsync -avu data/ /webapp/data/ && \
    rsync -avu codebase/ /webapp/codebase/

# Remove the default nginx configuration and copy the custom one
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d
